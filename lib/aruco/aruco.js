/*
Copyright (c) 2011 Juan Mellado
Copyright (c) 2019 Stefan Rothe

simple image analysis library by Juan Mellado
enhanced by Stefan Rothe

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
(function () {
  const ARUCO_4X4_100_DICT = [
    [[181, 50], [235, 72], [76, 173], [18, 215]],
    [[15, 154], [101, 71], [89, 240], [226, 166]],
    [[51, 45], [222, 17], [180, 204], [136, 123]],
    [[153, 70], [193, 60], [98, 153], [60, 131]],
    [[84, 158], [161, 211], [121, 42], [203, 133]],
    [[121, 205], [216, 183], [179, 158], [237, 27]],
    [[158, 46], [135, 93], [116, 121], [186, 225]],
    [[196, 242], [35, 234], [79, 35], [87, 196]],
    [[254, 218], [173, 239], [91, 127], [247, 181]],
    [[207, 86], [101, 252], [106, 243], [63, 166]],
    [[249, 145], [248, 142], [137, 159], [113, 31]],
    [[17, 167], [211, 18], [229, 136], [72, 203]],
    [[14, 183], [55, 86], [237, 112], [106, 236]],
    [[42, 15], [29, 21], [240, 84], [168, 184]],
    [[36, 177], [58, 66], [141, 36], [66, 92]],
    [[38, 62], [47, 81], [124, 100], [138, 244]],
    [[70, 101], [22, 240], [166, 98], [15, 104]],
    [[102, 0], [12, 192], [0, 102], [3, 48]],
    [[108, 94], [41, 245], [122, 54], [175, 148]],
    [[118, 175], [159, 211], [245, 110], [203, 249]],
    [[134, 139], [21, 75], [209, 97], [210, 168]],
    [[176, 43], [155, 9], [212, 13], [144, 217]],
    [[204, 213], [48, 254], [171, 51], [127, 12]],
    [[221, 130], [193, 206], [65, 187], [115, 131]],
    [[254, 71], [157, 252], [226, 127], [63, 185]],
    [[148, 113], [178, 104], [142, 41], [22, 77]],
    [[172, 228], [10, 126], [39, 53], [126, 80]],
    [[165, 84], [104, 120], [42, 165], [30, 22]],
    [[33, 35], [91, 0], [196, 132], [0, 218]],
    [[52, 111], [155, 113], [246, 44], [142, 217]],
    [[68, 21], [48, 208], [168, 34], [11, 12]],
    [[87, 178], [231, 194], [77, 234], [67, 231]],
    [[158, 207], [149, 127], [243, 121], [254, 169]],
    [[240, 203], [153, 171], [211, 15], [213, 153]],
    [[8, 174], [3, 23], [117, 16], [232, 192]],
    [[9, 41], [82, 5], [148, 144], [160, 74]],
    [[24, 117], [178, 52], [174, 24], [44, 77]],
    [[4, 255], [51, 115], [255, 32], [206, 204]],
    [[13, 246], [99, 118], [111, 176], [110, 198]],
    [[28, 90], [161, 101], [90, 56], [166, 133]],
    [[23, 24], [228, 65], [24, 232], [130, 39]],
    [[42, 40], [14, 5], [20, 84], [160, 112]],
    [[50, 140], [140, 19], [49, 76], [200, 49]],
    [[56, 178], [171, 6], [77, 28], [96, 213]],
    [[36, 232], [10, 99], [23, 36], [198, 80]],
    [[46, 235], [31, 103], [215, 116], [230, 248]],
    [[45, 63], [123, 85], [252, 180], [170, 222]],
    [[75, 100], [70, 180], [38, 210], [45, 98]],
    [[80, 46], [131, 145], [116, 10], [137, 193]],
    [[80, 19], [177, 128], [200, 10], [1, 141]],
    [[81, 148], [224, 146], [41, 138], [73, 7]],
    [[85, 104], [194, 225], [22, 170], [135, 67]],
    [[93, 65], [208, 228], [130, 186], [39, 11]],
    [[95, 151], [245, 214], [233, 250], [107, 175]],
    [[104, 1], [24, 132], [128, 22], [33, 24]],
    [[104, 103], [27, 180], [230, 22], [45, 216]],
    [[97, 36], [74, 144], [36, 134], [9, 82]],
    [[97, 233], [90, 163], [151, 134], [197, 90]],
    [[107, 18], [109, 132], [72, 214], [33, 182]],
    [[111, 229], [94, 246], [167, 246], [111, 122]],
    [[103, 223], [125, 243], [251, 230], [207, 190]],
    [[126, 27], [189, 197], [216, 126], [163, 189]],
    [[128, 160], [2, 10], [5, 1], [80, 64]],
    [[131, 68], [68, 56], [34, 193], [28, 34]],
    [[139, 162], [71, 14], [69, 209], [112, 226]],
    [[147, 122], [231, 41], [94, 201], [148, 231]],
    [[132, 108], [2, 121], [54, 33], [158, 64]],
    [[133, 42], [67, 73], [84, 161], [146, 194]],
    [[133, 156], [96, 91], [57, 161], [218, 6]],
    [[156, 137], [144, 79], [145, 57], [242, 9]],
    [[159, 161], [214, 78], [133, 249], [114, 107]],
    [[187, 124], [238, 61], [62, 221], [188, 119]],
    [[188, 4], [136, 92], [32, 61], [58, 17]],
    [[182, 91], [189, 105], [218, 109], [150, 189]],
    [[191, 200], [204, 111], [19, 253], [246, 51]],
    [[183, 171], [223, 75], [213, 237], [210, 251]],
    [[202, 31], [53, 157], [248, 83], [185, 172]],
    [[201, 98], [67, 172], [70, 147], [53, 194]],
    [[217, 88], [224, 173], [26, 155], [181, 7]],
    [[211, 213], [244, 186], [171, 203], [93, 47]],
    [[204, 152], [32, 207], [25, 51], [243, 4]],
    [[199, 160], [70, 202], [5, 227], [83, 98]],
    [[197, 55], [115, 216], [236, 163], [27, 206]],
    [[233, 93], [120, 189], [186, 151], [189, 30]],
    [[249, 37], [218, 156], [164, 159], [57, 91]],
    [[251, 187], [255, 143], [221, 223], [241, 255]],
    [[238, 42], [15, 205], [84, 119], [179, 240]],
    [[247, 77], [220, 249], [178, 239], [159, 59]],
    [[53, 117], [250, 112], [174, 172], [14, 95]],
    [[138, 173], [22, 31], [181, 81], [248, 104]],
    [[118, 23], [189, 208], [232, 110], [11, 189]],
    [[10, 207], [21, 55], [243, 80], [236, 168]],
    [[6, 75], [21, 97], [210, 96], [134, 168]],
    [[45, 193], [88, 102], [131, 180], [102, 26]],
    [[73, 216], [96, 167], [27, 146], [229, 6]],
    [[67, 244], [102, 178], [47, 194], [77, 102]],
    [[79, 54], [103, 212], [108, 242], [43, 230]],
    [[79, 211], [117, 230], [203, 242], [103, 174]],
    [[105, 228], [74, 182], [39, 150], [109, 82]],
    [[112, 199], [153, 178], [227, 14], [77, 153]]
  ]

  function createByteArray (size) {
    return new Uint8ClampedArray(new ArrayBuffer(size))
  }

  function checkImage (imageSrc, imageDst) {
    if (imageDst && imageSrc.width === imageDst.width && imageSrc.height === imageDst.height) {
      return imageDst
    }
    return {
      width: imageSrc.width,
      height: imageSrc.height,
      data: createByteArray(imageSrc.width * imageSrc.height)
    }
  }

  function grayscale (imageSrc, imageDst) {
    imageDst = checkImage(imageSrc, imageDst)
    const src = imageSrc.data
    const dst = imageDst.data
    const len = src.length
    let j = 0
    for (let i = 0; i < len; i += 4) {
      dst[j++] = (src[i] * 77 + src[i + 1] * 150 + src[i + 2] * 29) >> 8
    }
    return imageDst
  }

  const AT_TAB = createByteArray(768)

  function setAdaptiveThreshold (threshold) {
    for (let i = 0; i < 768; ++i) {
      AT_TAB[i] = (i - 255 <= -threshold) ? 255 : 0
    }
  }

  function adaptiveThreshold (imageSrc, imageDst, kernelSize) {
    imageDst = checkImage(imageSrc, imageDst)
    const src = imageSrc.data
    const dst = imageDst.data
    const len = src.length
    stackBoxBlur(imageSrc, imageDst, kernelSize)
    for (let i = 0; i < len; ++i) {
      dst[i] = AT_TAB[src[i] - dst[i] + 255]
    }
    return imageDst
  }

  function threshold (imageSrc, imageDst, threshold) {
    const src = imageSrc.data
    const dst = imageDst.data
    const len = src.length
    const tab = createByteArray(255)
    let i
    for (i = 0; i < 256; ++i) {
      tab[i] = i <= threshold ? 0 : 255
    }
    for (i = 0; i < len; ++i) {
      dst[i] = tab[src[i]]
    }
    imageDst.width = imageSrc.width
    imageDst.height = imageSrc.height
    return imageDst
  }

  function otsu (imageSrc) {
    const src = imageSrc.data
    const len = src.length
    const hist = new Array(256)
    let threshold = 0
    let sum = 0
    let sumB = 0
    let wB = 0
    let wF = 0
    let max = 0
    let mu
    let between
    let i
    hist.fill(0)
    for (i = 0; i < len; ++i) {
      hist[src[i]]++
    }
    for (i = 0; i < 256; ++i) {
      sum += hist[i] * i
    }
    for (i = 0; i < 256; ++i) {
      wB += hist[i]
      if (wB !== 0) {
        wF = len - wB
        if (wF === 0) {
          break
        }
        sumB += hist[i] * i
        mu = (sumB / wB) - ((sum - sumB) / wF)
        between = wB * wF * mu * mu
        if (between > max) {
          max = between
          threshold = i
        }
      }
    }
    return threshold
  }

  const STACK_BOX_BLUR_MULT = [1, 171, 205, 293, 57, 373, 79, 137, 241, 27, 391, 357, 41, 19, 283, 265]
  const STACK_BOX_BLUR_SHIFT = [0, 9, 10, 11, 9, 12, 10, 11, 12, 9, 13, 13, 10, 9, 13, 13]

  function stackBoxBlur (imageSrc, imageDst, kernelSize) {
    const src = imageSrc.data
    const dst = imageDst.data
    const height = imageSrc.height
    const width = imageSrc.width
    const heightMinus1 = height - 1
    const widthMinus1 = width - 1
    const size = kernelSize + kernelSize + 1
    const radius = kernelSize + 1
    const mult = STACK_BOX_BLUR_MULT[kernelSize]
    const shift = STACK_BOX_BLUR_SHIFT[kernelSize]
    let stack
    let stackStart
    let color
    let sum
    let pos
    let start
    let p
    let x
    let y
    let i
    stack = stackStart = { color: 0, next: null }
    for (i = 1; i < size; ++i) {
      stack = stack.next = { color: 0, next: null }
    }
    stack.next = stackStart
    pos = 0
    for (y = 0; y < height; ++y) {
      start = pos
      color = src[pos]
      sum = radius * color
      stack = stackStart
      for (i = 0; i < radius; ++i) {
        stack.color = color
        stack = stack.next
      }
      for (i = 1; i < radius; ++i) {
        stack.color = src[pos + i]
        sum += stack.color
        stack = stack.next
      }
      stack = stackStart
      for (x = 0; x < width; ++x) {
        dst[pos++] = (sum * mult) >>> shift
        p = x + radius
        p = start + (p < widthMinus1 ? p : widthMinus1)
        sum -= stack.color - src[p]
        stack.color = src[p]
        stack = stack.next
      }
    }
    for (x = 0; x < width; ++x) {
      pos = x
      start = pos + width
      color = dst[pos]
      sum = radius * color
      stack = stackStart
      for (i = 0; i < radius; ++i) {
        stack.color = color
        stack = stack.next
      }
      for (i = 1; i < radius; ++i) {
        stack.color = dst[start]
        sum += stack.color
        stack = stack.next
        start += width
      }
      stack = stackStart
      for (y = 0; y < height; ++y) {
        dst[pos] = (sum * mult) >>> shift
        p = y + radius
        p = x + ((p < heightMinus1 ? p : heightMinus1) * width)
        sum -= stack.color - dst[p]
        stack.color = dst[p]
        stack = stack.next
        pos += width
      }
    }
    return imageDst
  }

  function binaryBorder (imageSrc, dst) {
    const src = imageSrc.data
    const height = imageSrc.height
    const width = imageSrc.width
    let posSrc = 0
    let posDst = 0
    let i
    let j
    for (j = -2; j < width; ++j) {
      dst[posDst++] = 0
    }
    for (i = 0; i < height; ++i) {
      dst[posDst++] = 0
      for (j = 0; j < width; ++j) {
        dst[posDst++] = (src[posSrc++] === 0 ? 0 : 1)
      }
      dst[posDst++] = 0
    }
    for (j = -2; j < width; ++j) {
      dst[posDst++] = 0
    }
    return dst
  }

  const NEIGHBORHOOD_X = [1, 1, 0, -1, -1, -1, 0, 1]
  const NEIGHBORHOOD_Y = [0, -1, -1, -1, 0, 1, 1, 1]

  function neighborhoodDeltas (width) {
    const len = NEIGHBORHOOD_X.length
    const deltas = new Array(len)
    for (let i = 0; i < len; ++i) {
      deltas[i] = NEIGHBORHOOD_X[i] + NEIGHBORHOOD_Y[i] * width
    }
    return deltas.concat(deltas)
  }

  function borderFollowing (src, pos, nbd, x, y, hole, deltas) {
    const contour = []
    let pos1
    let pos3
    let pos4
    let s
    let sEnd
    contour.hole = hole
    s = sEnd = hole ? 0 : 4
    do {
      s = (s - 1) & 7
      pos1 = pos + deltas[s]
      if (src[pos1] !== 0) break
    } while (s !== sEnd)
    if (s === sEnd) {
      src[pos] = -nbd
      contour.push({ x: x, y: y })
    } else {
      pos3 = pos
      while (true) {
        sEnd = s
        do {
          pos4 = pos3 + deltas[++s]
        } while (src[pos4] === 0)
        s &= 7
        if (((s - 1) >>> 0) < (sEnd >>> 0)) {
          src[pos3] = -nbd
        } else if (src[pos3] === 1) {
          src[pos3] = nbd
        }
        contour.push({ x: x, y: y })
        x += NEIGHBORHOOD_X[s]
        y += NEIGHBORHOOD_Y[s]
        if (pos4 === pos && pos3 === pos1) break
        pos3 = pos4
        s = (s + 4) & 7
      }
    }
    return contour
  }

  function findContours (imageSrc, binary, contours) {
    const width = imageSrc.width
    const height = imageSrc.height
    const src = binaryBorder(imageSrc, binary)
    const deltas = neighborhoodDeltas(width + 2)
    let ci = 0
    let pixel
    let nbd
    let outer
    let hole
    let x
    let y
    let pos = width + 3
    nbd = 1
    for (y = 0; y < height; ++y, pos += 2) {
      for (x = 0; x < width; ++x, ++pos) {
        pixel = src[pos]
        if (pixel === 0) continue
        outer = pixel === 1 && src[pos - 1] === 0
        hole = !outer && pixel >= 1 && src[pos + 1] === 0
        if (outer || hole) {
          ++nbd
          contours[ci++] = borderFollowing(src, pos, nbd, x, y, hole, deltas)
        }
      }
    }
    contours.length = ci
  }

  function approxPolyDP (contour, epsilon) {
    let slice = { startIndex: 0, endIndex: 0 }
    let rightSlice = { startIndex: 0, endIndex: 0 }
    let poly = []
    let stack = []
    let len = contour.length
    let pt
    let startPt
    let endPt
    let dist
    let maxDist
    let leEps
    let dx
    let dy
    let i
    let j
    let k
    epsilon *= epsilon
    k = 0
    for (i = 0; i < 3; ++i) {
      maxDist = 0
      k = (k + rightSlice.startIndex) % len
      startPt = contour[k]
      if (++k === len) { k = 0 }
      for (j = 1; j < len; ++j) {
        pt = contour[k]
        if (++k === len) { k = 0 }
        dx = pt.x - startPt.x
        dy = pt.y - startPt.y
        dist = dx * dx + dy * dy
        if (dist > maxDist) {
          maxDist = dist
          rightSlice.startIndex = j
        }
      }
    }
    if (maxDist <= epsilon) {
      poly.push({ x: startPt.x, y: startPt.y })
    } else {
      slice.startIndex = k
      slice.endIndex = (rightSlice.startIndex += slice.startIndex)
      rightSlice.startIndex -= rightSlice.startIndex >= len ? len : 0
      rightSlice.endIndex = slice.startIndex
      if (rightSlice.endIndex < rightSlice.startIndex) {
        rightSlice.endIndex += len
      }
      stack.push({ startIndex: rightSlice.startIndex, endIndex: rightSlice.endIndex })
      stack.push({ startIndex: slice.startIndex, endIndex: slice.endIndex })
    }
    while (stack.length !== 0) {
      slice = stack.pop()
      endPt = contour[slice.endIndex % len]
      startPt = contour[k = slice.startIndex % len]
      if (++k === len) { k = 0 }
      if (slice.endIndex <= slice.startIndex + 1) {
        leEps = true
      } else {
        maxDist = 0
        dx = endPt.x - startPt.x
        dy = endPt.y - startPt.y
        for (i = slice.startIndex + 1; i < slice.endIndex; ++i) {
          pt = contour[k]
          if (++k === len) { k = 0 }
          dist = Math.abs((pt.y - startPt.y) * dx - (pt.x - startPt.x) * dy)
          if (dist > maxDist) {
            maxDist = dist
            rightSlice.startIndex = i
          }
        }
        leEps = maxDist * maxDist <= epsilon * (dx * dx + dy * dy)
      }
      if (leEps) {
        poly.push({ x: startPt.x, y: startPt.y })
      } else {
        rightSlice.endIndex = slice.endIndex
        slice.endIndex = rightSlice.startIndex
        stack.push({ startIndex: rightSlice.startIndex, endIndex: rightSlice.endIndex })
        stack.push({ startIndex: slice.startIndex, endIndex: slice.endIndex })
      }
    }
    return poly
  }

  function square2quad (src) {
    const sq = new Array(9)
    let px, py, dx1, dx2, dy1, dy2, den
    px = src[0].x - src[1].x + src[2].x - src[3].x
    py = src[0].y - src[1].y + src[2].y - src[3].y
    if (px === 0 && py === 0) {
      sq[0] = src[1].x - src[0].x
      sq[1] = src[2].x - src[1].x
      sq[2] = src[0].x
      sq[3] = src[1].y - src[0].y
      sq[4] = src[2].y - src[1].y
      sq[5] = src[0].y
      sq[6] = 0
      sq[7] = 0
      sq[8] = 1
    } else {
      dx1 = src[1].x - src[2].x
      dx2 = src[3].x - src[2].x
      dy1 = src[1].y - src[2].y
      dy2 = src[3].y - src[2].y
      den = dx1 * dy2 - dx2 * dy1
      sq[6] = (px * dy2 - dx2 * py) / den
      sq[7] = (dx1 * py - px * dy1) / den
      sq[8] = 1
      sq[0] = src[1].x - src[0].x + sq[6] * src[1].x
      sq[1] = src[3].x - src[0].x + sq[7] * src[3].x
      sq[2] = src[0].x
      sq[3] = src[1].y - src[0].y + sq[6] * src[1].y
      sq[4] = src[3].y - src[0].y + sq[7] * src[3].y
      sq[5] = src[0].y
    }
    return sq
  }

  function getPerspectiveTransform (src, size) {
    const rq = square2quad(src)
    rq[0] /= size
    rq[1] /= size
    rq[3] /= size
    rq[4] /= size
    rq[6] /= size
    rq[7] /= size
    return rq
  }

  function warp (imageSrc, imageDst, contour, warpSize) {
    const src = imageSrc.data
    const dst = imageDst.data
    const width = imageSrc.width
    const height = imageSrc.height
    let pos = 0
    let sx1, sx2, dx1, dx2, sy1, sy2, dy1, dy2, p1, p2, p3, p4, m, r, s, t, u, v, w, x, y, i, j
    m = getPerspectiveTransform(contour, warpSize - 1)
    r = m[8]
    s = m[2]
    t = m[5]
    for (i = 0; i < warpSize; ++i) {
      r += m[7]
      s += m[1]
      t += m[4]
      u = r
      v = s
      w = t
      for (j = 0; j < warpSize; ++j) {
        u += m[6]
        v += m[0]
        w += m[3]
        x = v / u
        y = w / u
        sx1 = x >>> 0
        sx2 = (sx1 === width - 1) ? sx1 : sx1 + 1
        dx1 = x - sx1
        dx2 = 1.0 - dx1
        sy1 = y >>> 0
        sy2 = (sy1 === height - 1) ? sy1 : sy1 + 1
        dy1 = y - sy1
        dy2 = 1.0 - dy1
        p1 = p2 = sy1 * width
        p3 = p4 = sy2 * width
        dst[pos++] =
          (dy2 * (dx2 * src[p1 + sx1] + dx1 * src[p2 + sx2]) +
           dy1 * (dx2 * src[p3 + sx1] + dx1 * src[p4 + sx2])) & 0xff
      }
    }
    imageDst.width = warpSize
    imageDst.height = warpSize
    return imageDst
  }

  function isContourConvex (contour) {
    let orientation = 0
    let convex = true
    const len = contour.length
    let i = 0
    let j = 0
    let curPt, prevPt, dxdy0, dydx0, dx0, dy0, dx, dy
    prevPt = contour[len - 1]
    curPt = contour[0]
    dx0 = curPt.x - prevPt.x
    dy0 = curPt.y - prevPt.y
    for (; i < len; ++i) {
      if (++j === len) { j = 0 }
      prevPt = curPt
      curPt = contour[j]
      dx = curPt.x - prevPt.x
      dy = curPt.y - prevPt.y
      dxdy0 = dx * dy0
      dydx0 = dy * dx0
      orientation |= dydx0 > dxdy0 ? 1 : (dydx0 < dxdy0 ? 2 : 3)
      if (orientation === 3) {
        convex = false
        break
      }
      dx0 = dx
      dy0 = dy
    }
    return convex
  }

  function minEdgeLength (poly) {
    const len = poly.length
    let i = 0
    let j = len - 1
    let min = Infinity
    let d
    let dx
    let dy
    for (; i < len; j = i++) {
      dx = poly[i].x - poly[j].x
      dy = poly[i].y - poly[j].y
      d = dx * dx + dy * dy
      if (d < min) min = d
    }
    return Math.sqrt(min)
  }

  function findCandidates (contours, minSize, epsilon, minLength) {
    const candidates = []
    const len = contours.length
    let contour
    let poly
    this.polys = []
    for (let i = 0; i < len; ++i) {
      contour = contours[i]
      if (contour.length >= minSize) {
        poly = approxPolyDP(contour, contour.length * epsilon)
        this.polys.push(poly)
        if ((poly.length === 4) && (isContourConvex(poly))) {
          if (minEdgeLength(poly) >= minLength) {
            candidates.push(poly)
          }
        }
      }
    }
    return candidates
  }

  function clockwiseCorners (candidates) {
    const len = candidates.length
    let dx1, dx2, dy1, dy2, swap
    for (let i = 0; i < len; ++i) {
      dx1 = candidates[i][1].x - candidates[i][0].x
      dy1 = candidates[i][1].y - candidates[i][0].y
      dx2 = candidates[i][2].x - candidates[i][0].x
      dy2 = candidates[i][2].y - candidates[i][0].y
      if ((dx1 * dy2 - dy1 * dx2) < 0) {
        swap = candidates[i][1]
        candidates[i][1] = candidates[i][3]
        candidates[i][3] = swap
      }
    }
    return candidates
  }

  function perimeterSquare (poly) {
    const len = poly.length
    let i = 0
    let j = len - 1
    let p = 0.0
    let dx
    let dy
    for (; i < len; j = i++) {
      dx = poly[i].x - poly[j].x
      dy = poly[i].y - poly[j].y
      p += dx * dx + dy * dy
    }
    return p
  }

  function notTooNear (candidates, minDist) {
    const notTooNear = []
    const len = candidates.length
    let dist, dx, dy, i, j, k
    for (i = 0; i < len; ++i) {
      for (j = i + 1; j < len; ++j) {
        dist = 0
        for (k = 0; k < 4; ++k) {
          dx = candidates[i][k].x - candidates[j][k].x
          dy = candidates[i][k].y - candidates[j][k].y
          dist += dx * dx + dy * dy
        }
        if ((dist / 4) < (minDist * minDist)) {
          if (perimeterSquare(candidates[i]) < perimeterSquare(candidates[j])) {
            candidates[i].tooNear = true
          } else {
            candidates[j].tooNear = true
          }
        }
      }
    }
    for (i = 0; i < len; ++i) {
      if (!candidates[i].tooNear) {
        notTooNear.push(candidates[i])
      }
    }
    return notTooNear
  }

  function rotate (src, rotation) {
    const len = src.length
    const dst = new Array(len)
    for (let i = 0; i < len; ++i) {
      dst[(rotation + i) % len] = src[i]
    }
    return dst
  }

  const HAMMING_POP_COUNT_TABLE_2 = [
    0, 1, 1, 1, 1, 2, 2, 2, 1, 2, 2, 2, 1, 2, 2, 2, 1, 2, 2, 2, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3,
    1, 2, 2, 2, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3, 1, 2, 2, 2, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3,
    1, 2, 2, 2, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3, 3, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4,
    2, 3, 3, 3, 3, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4, 2, 3, 3, 3, 3, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4,
    1, 2, 2, 2, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3, 3, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4,
    2, 3, 3, 3, 3, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4, 2, 3, 3, 3, 3, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4,
    1, 2, 2, 2, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3, 2, 3, 3, 3, 3, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4,
    2, 3, 3, 3, 3, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4, 2, 3, 3, 3, 3, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4
  ]

  function normHamming (a, b) {
    const len = a.length
    let result = 0
    for (let i = 0; i < len; ++i) {
      result += HAMMING_POP_COUNT_TABLE_2[a[i] ^ b[i]]
    }
    return result
  }

  function createMarker (id, corners) {
    const x0 = (corners[0].x + corners[1].x) / 2
    const y0 = (corners[0].y + corners[1].y) / 2
    const x1 = x0 + (corners[1].y - corners[0].y)
    const y1 = y0 + (corners[0].x - corners[1].x)
    const dx = x1 - x0
    const dy = y1 - y0
    const d = Math.sqrt(dx * dx + dy * dy)
    const angle = -Math.atan2(dy / d, dx / d) * 180 / Math.PI
    return {
      id: id,
      corners: corners,
      north: [x0, y0, x1, y1],
      angle: angle
    }
  }

  function identify (marker, corners, maxCorrelationRate) {
    let candidate
    let rotation
    let minHamming
    let currentHamming
    let r
    for (let i = 0; i < ARUCO_4X4_100_DICT.length; ++i) {
      minHamming = 17
      candidate = ARUCO_4X4_100_DICT[i]
      rotation = -1
      for (r = 0; r < 4; ++r) {
        currentHamming = normHamming(candidate[r], marker)
        if (currentHamming < minHamming) {
          minHamming = currentHamming
          rotation = r
        }
      }
      if (minHamming <= maxCorrelationRate) {
        return createMarker(i, rotate(corners, rotation))
      }
    }
  }

  function isBitSet (imageSrc, x, y, side, minZero) {
    const src = imageSrc.data
    const span = imageSrc.width - side
    let pos = (x + imageSrc.width * y) * side
    let sum = 0
    for (y = 0; y < side; ++y) {
      for (x = 0; x < side; ++x) {
        if (src[pos++] !== 0) ++sum
      }
      pos += span
    }
    return sum > minZero
  }

  function checkMarker (imageSrc, candidate) {
    const side = (imageSrc.width / 6) >>> 0
    const minZero = (side * side) >> 1
    let x
    let y
    for (x = 0; x < 6; ++x) {
      if (isBitSet(imageSrc, x, 0, side, minZero)) return null
      if (isBitSet(imageSrc, x, 5, side, minZero)) return null
    }
    for (y = 1; y < 5; ++y) {
      if (isBitSet(imageSrc, 0, y, side, minZero)) return null
      if (isBitSet(imageSrc, 5, y, side, minZero)) return null
    }
    const bytes = [0, 0]
    let pos = 0
    for (y = 1; y < 5; ++y) {
      for (x = 1; x < 5; ++x) {
        bytes[pos] <<= 1
        bytes[pos] |= isBitSet(imageSrc, x, y, side, minZero) ? 1 : 0
      }
      if (y === 2) ++pos
    }
    return identify(bytes, candidate, 1)
  }

  let AR = {}

  AR.Detector = function () {
    this.grey = null
    this.thres = null
    this.homography = {
      width: 0,
      height: 0,
      data: []
    }
    this.binary = []
    this.contours = []
    this.polys = []
    this.candidates = []
    this.blurSize = 2
    this.warpSize = 49
    setAdaptiveThreshold(7)
  }

  AR.Detector.prototype.findMarkers = function (imageSrc, candidates, warpSize, mirrror) {
    const markers = []
    const len = candidates.length
    let candidate, marker, i
    for (i = 0; i < len; ++i) {
      candidate = candidates[i]
      warp(imageSrc, this.homography, candidate, warpSize)
      threshold(this.homography, this.homography, otsu(this.homography))
      marker = checkMarker(this.homography, candidate)
      if (marker) {
        markers.push(marker)
      }
    }
    return markers
  }

  const SIZE = 4

  AR.Detector.prototype.detect = function (image) {
    this.grey = grayscale(image, this.grey)
    this.thres = adaptiveThreshold(this.grey, this.thres, this.blurSize)
    findContours(this.thres, this.binary, this.contours)
    this.candidates = findCandidates(this.contours, SIZE * 2, 0.05, 10)
    this.candidates = clockwiseCorners(this.candidates)
    this.candidates = notTooNear(this.candidates, 10)
    return this.findMarkers(this.grey, this.candidates, this.warpSize)
  }

  window.AR = AR
})()
