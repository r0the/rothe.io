#!/usr/bin/env python3
import http.server
import os

class Handler(http.server.SimpleHTTPRequestHandler):

    def end_headers(self):
        self.send_header("Cache-Control", "no-cache, no-store, must-revalidate")
        self.send_header("Pragma", "no-cache")
        self.send_header("Expires", "0")
        self.send_header("Pragma", "no-cache")
        self.extensions_map.update({
            ".js": "application/javascript",
        })
        super().end_headers()
    def do_OPTIONS(self):
        self.do_HEAD()

def main():
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    httpd = http.server.HTTPServer(("localhost", 8000), Handler)
    httpd.serve_forever()

main()
