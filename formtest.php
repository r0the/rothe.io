<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="config/favicon.ico">
    <link rel="icon" type="image/svg+xml" href="config/favicon.svg" sizes="any">
    <link rel="apple-touch-icon" href="config/favicon-180.png">

    <link rel="stylesheet" href="lib/material-design-icons/css/mdi.css">
    <link rel="stylesheet" href="lib/prism/prism.css">
    <link rel="stylesheet" href="style/main.css">
    <link rel="stylesheet" href="config/main.css">
    <title>Webtechnologien - Formulartest</title>
  </head>
  <body>
    <nav class="navbar">
      <div class="navbar-brand">
        <a class="navbar-item is-hidden-desktop sidebar-button" href="#"><i class="mdi mdi-menu"></i></a>
        <span class="navbar-item">Webtechnologien - Formulartest</span>
      </div>
    </nav>
    <main class="container content">
      <div class="box">
        <h1>Formulartest</h1>
        <hr>
        <p>
          Die folgenden Formulardaten sind an den Server geschickt worden:
        </p>
        <table class="table">
          <tr><th>Feldname</th><th>Wert</th></tr>
            <?php
              foreach ($_POST as $key => $value){
                echo "<tr><td>{$key}</td><td>{$value}</td></tr>";
              }
            ?>
        </table>
        <button class="button is-link" id="back">Zurück</button>
      </div>
    </main>
    <script src="src/formtest.js"></script>
  </body>
</html>
