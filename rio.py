import json
import re

class Entry:
    def __init__(self, params, book):
        self.page = book.expand_path(params["page"] + ".md")
        self.entries = []
        self.book = book
        if "items" in params:
            for item in params["items"]:
                self.entries.append(Entry(item, book))


class Book(Entry):
    def __init__(self, params):
        self.base = params["base"] if "base" in params else None
        self.title = params["title"]
        self.cover = self.expand_path(params["cover"]) if "cover" in params else None
        Entry.__init__(self, params, self)

    def expand_path(self, path):
        if path[0] == '/':
            return path[1:]
        elif self.base:
            return self.base + "/" + path
        else:
            return path


def load_book(path):
    return Book(load_json(path))


def load_books():
    config = load_json("config/config.json")
    bookLists = config["books"]
    result = []
    for bookList in bookLists:
        list = load_json(bookList)
        for entry in list:
            result.append(list[entry])
    return result


def load_json(path):
    with open(path) as json_file:
        return json.load(json_file)


def save_json(path, data):
    with open(path, mode="w") as json_file:
        json.dump(data, json_file, ensure_ascii=False, indent=2)
        json_file.write("\n")
