const TITLE = {
  author: 'Autor',
  source: 'Quelle',
  licence: 'Lizenz',
  remix: 'Remix',
};

const ImageDetail = {
  props: {
    info: { type: Object },
    name: { type: String },
  },
  computed: {
    content() {
      if (!this.info) return '';
      const url =
        this.info[`${this.name}_url`] || this.info[`${this.name}_uri`];
      const name = this.info[`${this.name}_name`] || this.info[this.name];
      if (name) {
        if (url) return `<a href="${url}" target="_blank">${name}</a>`;
        return name;
      }
      return 'unbekannt';
    },
    title() {
      return TITLE[this.name];
    },
    visible() {
      return (
        this.info && (this.info[`${this.name}_name`] || this.info[this.name])
      );
    },
  },
  template: `<div class="control">
      <div v-if="visible" class="tags has-addons">
        <span class="tag">{{ title }}</span>
        <span class="tag is-success is-light" v-html="content"></span>
      </div>
    </div>`,
};

export default {
  components: {
    ImageDetail,
  },
  data() {
    return {
      info: null,
      src: '#',
      visible: false,
    };
  },
  computed: {
    classes() {
      const result = ['modal'];
      if (this.visible) result.push('is-active');
      return result;
    },
  },
  template: `
    <div :class="classes" id="modal" tabindex="-1" role="dialog">
      <div class="modal-background"></div>
      <div class="modal-card">
        <div class="modal-card-head">
          <h5 class="modal-card-title">Bildinformation</h5>
          <button class="delete" @click="visible = false"></button>
        </div>
        <div class="modal-card-body">
          <p><img alt="Bild" class="img-fluid" id="image-info-image" :src="src"></p>
          <div class="field is-grouped is-grouped-multiline">
            <ImageDetail :info="info" name="author"/>
            <ImageDetail :info="info" name="source"/>
            <ImageDetail :info="info" name="licence"/>
            <ImageDetail :info="info" name="remix"/>
          </div>
        </div>
        <div class="modal-card-foot">
          <button type="button" class="button is-secondary" @click="visible = false">Schliessen</button>
        </div>
      </div>
    </div>
  `,
};
