import MarkdownParser from './markdown-parser.js';

export default function () {
  const md = MarkdownParser();
  const ESCAPE_HTML = /</g;
  function escapeHtmlAttr(attr) {
    const el = document.createElement('span');
    el.setAttribute('a', attr);
    const text = el.outerHTML;
    return text.substr(9, text.length - 18);
  }

  function escapeHTML(code) {
    return code.replace(ESCAPE_HTML, '&lt;');
  }

  function renderCode(token, env) {
    let language;
    let file;
    const backtick = token.markup[0] === '`';
    if (typeof token.info === 'string') {
      const params = token.info.trim().split(' ');
      if (params[0].length > 0) language = params[0];
      file = env.normalizeURI(params[1]);
    }
    let tag = 'v-code';
    let code = token.content || '';
    if (backtick) code = escapeHTML(code);
    if (language === 'scratch') {
      tag = 'v-scratch';
    }
    let result = '<' + tag + ' language="' + language + '"';
    if (token.content.length === 0) {
      result += ' src="' + file + '"';
    }
    if (!env.card && !env.slides && backtick) result += ' toolbar';
    if (!backtick) result += ' def';
    result += '>' + code + '</' + tag + '>';
    return result;
  }

  function vueComponent(tokens, idx) {
    if (tokens[idx].type !== 'paragraph_open') return false;
    const inline = tokens[idx + 1];
    if (!inline || inline.type !== 'inline') return false;
    const children = inline.children;
    return (
      children.length > 0 &&
      children[0].type === 'html_inline' &&
      children[0].content.startsWith('<v-')
    );
  }

  md.decorator = {
    renderImageInfo(env) {
      if (!env.imgInfo) return '';
      const result = '<v-image-tag src="' + env.imgInfo + '"/>';
      env.imgInfo = null;
      return result;
    },
  };

  md.renderColumnOpen = function (env) {
    let result = '<div class="' + env.columns + '">';
    if (env.card) result += '<div class="card">';
    env.cardImgTop = false;
    return result;
  };

  md.renderColumnClose = function (env) {
    let result = '</div>';
    if (!env.card || env.cardContent) {
      result = md.decorator.renderImageInfo(env) + result;
    }
    if (env.cardContent) result += '</div>';
    delete env.cardContent;
    if (env.card) result += '</div>';

    return result;
  };

  const COL_MAP = {
    1: 'is-12',
    2: 'is-6',
    3: 'is-4',
    4: 'is-3',
    6: 'is-2',
    12: 'is-1',
  };

  function isMenuList(tokens, idx) {
    if (tokens.length < idx + 4) return false;
    const inline = tokens[idx + 3];
    return (
      inline.type === 'inline' &&
      inline.children &&
      inline.children[0].type === 'link_open'
    );
  }

  const renderer = {
    blockquote_open(tokens, idx, options) {
      tokens[idx].attrPush(['class', 'blockquote alert alert-secondary']);
      return md.renderer.renderToken(tokens, idx, options);
    },

    bullet_list_open(tokens, idx, options) {
      const token = tokens[idx];
      const listClass = isMenuList(tokens, idx) ? 'menu-list' : 'ul-dash';
      token.attrPush(['class', listClass]);
      return md.renderer.renderToken(tokens, idx, options);
    },

    code_block(tokens, idx, options, env) {
      return renderCode(tokens[idx], env);
    },

    container_box_close(tokens, idx, options, env) {
      if (!env.boxSlot) return '';
      const tag = env.boxTag;
      delete env.boxSlot;
      delete env.boxType;
      delete env.boxTag;
      return '</template></' + tag + '>';
    },

    container_box_open(tokens, idx, options, env) {
      console.warn('::: box sollte nicht mehr verwendet werden.');
      if (env.boxSlot) return '';
      const token = tokens[idx];
      env.boxType = token.info.substr(4).trim();
      env.boxTag = 'v-box';
      env.boxSlot = 1;
      if (env.boxType === 'exercise') {
        env.boxTag = 'v-exercise';
        console.warn('Benutze ::: exercise ohne box.');
      }
      return (
        '<' + env.boxTag + ' type="' + env.boxType + '"><template v-slot:slot1>'
      );
    },

    container_columns_open(tokens, idx, options, env) {
      env.columns = 'column';
      const params = tokens[idx].info.trim().split(' ');
      const colCount = params[params.length - 1];
      if (colCount) env.columns += ' ' + COL_MAP[colCount];
      env.card = params.length > 0 && params[0] === 'cards';
      return '<div class="columns is-multiline">' + md.renderColumnOpen(env);
    },

    container_columns_close(tokens, idx, options, env) {
      const result = md.renderColumnClose(env) + '</div>';
      delete env.columns;
      delete env.card;
      delete env.cardContent;
      return result;
    },

    container_details_open(tokens, idx) {
      const info = md.parseInfo(tokens, idx);
      return (
        '<v-collapsible class="box has-background-blue-light" title="' +
        info[1] +
        '">'
      );
    },

    container_details_close() {
      return '</v-collapsible>';
    },

    container_exercise_open(tokens, idx, options, env) {
      if (env.exerciseSlot) return '';
      const info = md.parseInfo(tokens, idx);
      if (info[1]) {
        console.warn(
          'Der Titel der Aufgabe sollte im innern der :::-Umgebung angegeben werden.',
        );
      }
      env.exerciseSlot = 1;
      let result = '<v-exercise icon="' + info[0] + '" title="' + info[1] + '"';
      if (env.showSolutions) result += ' showSolution';
      result += '><template v-slot:slot1>';
      return result;
    },

    container_exercise_close(tokens, idx, options, env) {
      if (!env.exerciseSlot) return '';
      delete env.exerciseSlot;
      return '</template></v-exercise>';
    },

    container_info_open(tokens, idx) {
      const info = md.parseInfo(tokens, idx);
      let result = '<div class="box has-background-blue-light">';
      if (info[1]) {
        result += '<h4>';
        result += '<v-icon name="' + info[0] + '"/> ';
        result += info[1] + '</h4>';
      }
      return result;
    },

    container_info_close() {
      return '</div>';
    },

    container_warning_open(tokens, idx) {
      const info = md.parseInfo(tokens, idx);
      let result = '<div class="box has-background-yellow-light">';
      if (info[1]) result += '<h4>' + info[1] + '</h4>';
      return result;
    },

    container_warning_close() {
      return '</div>';
    },

    em_open(tokens, idx, options) {
      if (tokens[idx].markup === '_')
        tokens[idx].attrPush(['class', 'is-gui-element']);
      return md.renderer.renderToken(tokens, idx, options);
    },

    fence(tokens, idx, options, env) {
      return renderCode(tokens[idx], env);
    },

    figcaption_close(tokens, idx, options, env) {
      let result = '';
      if (!env.card) result += md.decorator.renderImageInfo(env);
      result += '</figcaption>';
      return result;
    },

    heading_open(tokens, idx, options, env) {
      const token = tokens[idx];
      let result = '';
      if (env.card && !env.cardContent) {
        result += '<div class="card-content">';
        env.cardContent = true;
      }
      result += '<' + token.tag + '>';
      return result;
    },

    hr(tokens, idx, options, env) {
      if (env.columns) {
        return md.renderColumnClose(env) + md.renderColumnOpen(env);
      }
      if (env.exerciseSlot) {
        ++env.exerciseSlot;
        return '</template><template v-slot:slot' + env.exerciseSlot + '>';
      }
      if (env.boxSlot) {
        ++env.boxSlot;
        return '</template><template v-slot:slot' + env.boxSlot + '>';
      }
      return '<hr>';
    },

    icon(tokens, idx) {
      return '<v-icon name="' + tokens[idx].content + '"/>';
    },

    image(tokens, idx, options, env) {
      const token = tokens[idx];
      const src = env.normalizeURI(token.attrGet('src'));
      const title = token.attrGet('title');
      if (title)
        console.warn('Bildunterschrift zu ' + src + ' sollte in [] stehen.');
      let result = '<img alt=""';
      if (token.content.indexOf('©') !== -1) env.imgInfo = src;
      if (env.card && !env.cardContent) result += ' class="card-image"';
      result += ' src="' + src + '">';
      return result;
    },

    link_open(tokens, idx, options, env) {
      const link = env.normalizeURI(tokens[idx].attrGet('href'));
      return '<v-link to="' + link + '">';
    },

    link_close() {
      return '</v-link>';
    },

    math_block(tokens, idx) {
      return (
        '<v-math content="' +
        escapeHtmlAttr(tokens[idx].content) +
        '" display></v-math>'
      );
    },

    math_inline(tokens, idx) {
      return (
        '<v-math content="' +
        escapeHtmlAttr(tokens[idx].content) +
        '"></v-math>'
      );
    },

    paragraph_open(tokens, idx, options, env) {
      // detect vue component
      env.vue = vueComponent(tokens, idx);
      if (env.vue) return '';
      if (env.card && !env.cardContent) {
        env.cardContent = true;
        return '<div class="card-content">';
      }
      return '<p>';
    },

    paragraph_close(tokens, idx, options, env) {
      if (env.vue) {
        delete env.vue;
        return '';
      }
      return '</p>';
    },

    strong_open(tokens, idx, options) {
      if (tokens[idx].markup === '__')
        tokens[idx].attrPush(['class', 'is-gui-element']);
      return md.renderer.renderToken(tokens, idx, options);
    },

    sup(tokens, idx) {
      return '<sup>' + tokens[idx].content + '</sup>';
    },

    table_open() {
      return '<figure><table align="center" class="table is-narrow is-fullwidth">';
    },

    table_close() {
      return '</table></figure>';
    },

    text(tokens, idx, options, env) {
      let content = tokens[idx].content;
      if (env.imgInfo) content = content.replace('©', '');
      return content;
    },
  };

  Object.assign(md.renderer.rules, renderer);
  return md;
}
