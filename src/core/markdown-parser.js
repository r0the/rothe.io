import MarkdownItImplicitFigures from '../../lib/markdown-it/markdown-it-implicit-figures.js';

const KEYS = [
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'R',
  'S',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z',
  '§',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '0',
  '+',
  '-',
  '*',
  '<',
  ',',
  '.',
  '/',
  "'",
  '¨',
  '^',
  'ä',
  'ö',
  'ü',
  '$',
  'Esc',
  'F1',
  'F2',
  'F3',
  'F4',
  'F5',
  'F6',
  'F7',
  'F8',
  'F9',
  'F10',
  'F11',
  'F12',
  'Alt',
  'AltGr',
  'Ctrl',
  'Del',
  'End',
  'Home',
  'Ins',
  'PgDn',
  'PgUp',
  'Num',
];
const ICON_KEYS = {
  Backspace: 'mdi-backspace-outline',
  CapsLock: 'mdi-apple-keyboard-caps',
  Command: 'mdi-apple-keyboard-command',
  Control: 'mdi-apple-keyboard-control',
  Down: 'mdi-arrow-down-bold',
  '↓': 'mdi-arrow-down-bold',
  Enter: 'mdi-keyboard-return',
  Left: 'mdi-arrow-left-bold',
  '←': 'mdi-arrow-left-bold',
  Menu: 'mdi-menu',
  Option: 'mdi-apple-keyboard-option',
  Power: 'mdi-power',
  Right: 'mdi-arrow-right-bold',
  '→': 'mdi-arrow-right-bold',
  Shift: 'mdi-apple-keyboard-shift',
  Tab: 'mdi-keyboard-tab',
  Up: 'mdi-arrow-up-bold',
  '↑': 'mdi-arrow-up-bold',
  Windows: 'mdi-microsoft-windows',
};

const CIRCUMFLEX_CODE = 0x5e;
const COLON = ':';
const COLON_CODE = 0x3a;
const BRACKET_LEFT_CODE = 0x5b;
const ALPHANUM_REGEX = /(^\w+)/i;
const ICON_REGEX = /(^[a-z0-9-]+)/i;

function lookupKey(content) {
  if (content === 'Space') return { content: '', class: 'space' };
  if (KEYS.includes(content)) return { content: content };
  const icon = ICON_KEYS[content];
  if (icon) return { icon: icon };
  return null;
}

function pushToken(state, params) {
  const type = params.type;
  const tag = params.tag || type;
  const nesting = params.nesting || 0;
  const token = state.push(type, tag, nesting);
  if (params.content) token.content = params.content;
  if (params.markup) token.markup = params.markup;
  if (params.class) token.attrPush(['class', params.class]);
  return token;
}

function icon(state, silent) {
  let start = state.pos;
  if (state.src.charCodeAt(start) !== COLON_CODE) return false;
  ++start;
  if (silent) return false;
  const match = ICON_REGEX.exec(state.src.substring(start));
  if (!match) return false;
  const end = start + match[0].length;
  if (state.src.charCodeAt(end) !== COLON_CODE) return false;
  pushToken(state, {
    type: 'icon',
    nesting: 0,
    markup: COLON,
    class: 'icon',
    content: match[0],
  });
  state.pos = end + 1;
  return true;
}

function kbd(state, silent) {
  let start = state.pos;
  if (state.src.charCodeAt(start) !== BRACKET_LEFT_CODE) return false;
  ++start;
  if (silent) return false;
  const end = state.src.indexOf(']', start);
  if (end === -1) return false;
  const content = state.src.slice(start, end);
  const key = lookupKey(content);
  if (key) {
    pushToken(state, {
      type: 'kbd',
      nesting: 1,
      markup: '[',
      class: key.class,
    });
    if (key.content) {
      pushToken(state, { type: 'text', content: key.content });
    } else if (key.icon) {
      pushToken(state, { type: 'icon', nesting: 0, content: key.icon });
    }
    pushToken(state, { type: 'kbd', nesting: -1, markup: ']' });
    state.pos = end + 1;
    return true;
  }
  return false;
}

function sup(state, silent) {
  let start = state.pos;
  if (state.src.charCodeAt(start) !== CIRCUMFLEX_CODE) return false;
  ++start;
  if (silent) return false;
  const match = ALPHANUM_REGEX.exec(state.src.substring(start));
  if (!match) return false;
  const content = match[0];
  const len = content.length;
  pushToken(state, { type: 'sup', content: content, markup: '^' });
  state.pos = start + len;
  return true;
}

// source: https://github.com/runarberg/markdown-it-math
function scanDelims(state, start, delimLength) {
  let pos = start;
  let canOpen;
  let leftFlanking = true;
  let rightFlanking = true;
  const max = state.posMax;
  const isWhiteSpace = state.md.utils.isWhiteSpace;
  // treat beginning of the line as a whitespace
  const lastChar = start > 0 ? state.src.charCodeAt(start - 1) : 0x20;
  if (pos >= max) canOpen = false;
  pos += delimLength;
  const count = pos - start;
  // treat end of the line as a whitespace
  const nextChar = pos < max ? state.src.charCodeAt(pos) : 0x20;
  const isLastWhiteSpace = isWhiteSpace(lastChar);
  const isNextWhiteSpace = isWhiteSpace(nextChar);
  if (isNextWhiteSpace) leftFlanking = false;
  if (isLastWhiteSpace) rightFlanking = false;
  canOpen = leftFlanking;
  const canClose = rightFlanking;
  return {
    canOpen: canOpen,
    canClose: canClose,
    delims: count,
  };
}

// source: https://github.com/runarberg/markdown-it-math
function makeMathInline(open, close) {
  return function (state, silent) {
    let found, res, closeDelim;
    const max = state.posMax;
    const start = state.pos;
    const openDelim = state.src.slice(start, start + open.length);
    if (openDelim !== open) return false;
    if (silent) return false;
    res = scanDelims(state, start, openDelim.length);
    const startCount = res.delims;
    if (!res.canOpen) {
      state.pos += startCount;
      // Earlier we checked !silent, but this implementation does not need it
      state.pending += state.src.slice(start, state.pos);
      return true;
    }
    state.pos = start + open.length;
    while (state.pos < max) {
      closeDelim = state.src.slice(state.pos, state.pos + close.length);
      if (closeDelim === close) {
        res = scanDelims(state, state.pos, close.length);
        if (res.canClose) {
          found = true;
          break;
        }
      }
      state.md.inline.skipToken(state);
    }
    if (!found) {
      state.pos = start;
      return false;
    }
    state.posMax = state.pos;
    state.pos = start + close.length;
    const token = state.push('math_inline', 'math', 0);
    token.content = state.src.slice(state.pos, state.posMax);
    token.markup = open;
    state.pos = state.posMax + close.length;
    state.posMax = max;
    return true;
  };
}

// source: https://github.com/runarberg/markdown-it-math
function makeMathBlock(open, close) {
  return function (state, startLine, endLine, silent) {
    let params, nextLine, firstLine, lastLine, lastLinePos;
    let haveEndMarker = false;
    let pos = state.bMarks[startLine] + state.tShift[startLine];
    let max = state.eMarks[startLine];
    if (pos + open.length > max) return false;
    const openDelim = state.src.slice(pos, pos + open.length);
    if (openDelim !== open) return false;
    pos += open.length;
    firstLine = state.src.slice(pos, max);
    if (silent) return true;
    if (firstLine.trim().slice(-close.length) === close) {
      firstLine = firstLine.trim().slice(0, -close.length);
      haveEndMarker = true;
    }
    nextLine = startLine;
    for (;;) {
      if (haveEndMarker) break;
      nextLine++;
      if (nextLine >= endLine) {
        // unclosed block should be autoclosed by end of document.
        // also block seems to be autoclosed by end of parent
        break;
      }
      pos = state.bMarks[nextLine] + state.tShift[nextLine];
      max = state.eMarks[nextLine];
      if (pos < max && state.tShift[nextLine] < state.blkIndent) {
        // non-empty line with negative indent should stop the list:
        break;
      }
      if (state.src.slice(pos, max).trim().slice(-close.length) !== close) {
        continue;
      }
      if (state.tShift[nextLine] - state.blkIndent >= 4) {
        // closing block math should be indented less then 4 spaces
        continue;
      }
      lastLinePos = state.src.slice(0, max).lastIndexOf(close);
      lastLine = state.src.slice(pos, lastLinePos);
      pos += lastLine.length + close.length;
      // make sure tail has spaces only
      pos = state.skipSpaces(pos);
      if (pos < max) continue;
      // found!
      haveEndMarker = true;
    }

    // If math block has heading spaces, they should be removed from its inner block
    const len = state.tShift[startLine];
    state.line = nextLine + (haveEndMarker ? 1 : 0);
    const token = state.push('math_block', 'math', 0);
    token.block = true;
    token.content =
      (firstLine && firstLine.trim() ? firstLine + '\n' : '') +
      state.getLines(startLine + 1, nextLine, len, true) +
      (lastLine && lastLine.trim() ? lastLine : '');
    token.info = params;
    token.map = [startLine, state.line];
    token.markup = open;
    return true;
  };
}

function parseInfo(tokens, idx) {
  const info = tokens[idx].info.trim();
  const pos = info.indexOf(' ');
  if (pos === -1) {
    return [info, ''];
  } else {
    return [info.substr(0, pos), info.substr(pos + 1)];
  }
}

export default function () {
  const md = window.markdownit({ html: true });
  md.use(MarkdownItImplicitFigures, { figcaption: true });
  md.parseInfo = parseInfo;
  md.inline.ruler.after('emphasis', 'icon', icon);
  md.inline.ruler.after('link', 'kbd', kbd);
  md.inline.ruler.after('kbd', 'sup', sup);
  md.inline.ruler.before('escape', 'math_inline', makeMathInline('$', '$'));
  md.block.ruler.after('blockquote', 'math_block', makeMathBlock('$$', '$$'), {
    alt: ['paragraph', 'reference', 'blockquote', 'list'],
  });
  if (window.markdownitFootnote) md.use(window.markdownitFootnote);

  md.use(window.markdownitContainer, 'box', {
    validate(params) {
      return params.trim().match(/box/);
    },
  });

  md.use(window.markdownitContainer, 'details', {
    validate(params) {
      return params.trim().match(/details/);
    },
  });

  md.use(window.markdownitContainer, 'exercise', {
    validate(params) {
      return params.trim().match(/exercise|extra|task/);
    },
  });

  md.use(window.markdownitContainer, 'info', {
    validate(params) {
      return params.trim().match(/info|example|goal/);
    },
  });

  md.use(window.markdownitContainer, 'warning', {
    validate(params) {
      return params.trim().match(/warning/);
    },
  });

  md.use(window.markdownitContainer, 'columns', {
    validate(params) {
      return params.trim().match(/columns|cards/);
    },
  });

  return md;
}
