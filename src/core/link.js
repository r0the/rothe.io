/* global Vue */
const FILE_TYPES = ['.fls', '.docx', 'xlsx'];

function isDownload(url) {
  for (let i = 0; i < FILE_TYPES.length; ++i) {
    if (url && url.endsWith(FILE_TYPES[i])) {
      return true;
    }
  }
  return false;
}

export default {
  props: {
    to: { type: String, default: null },
  },
  computed: {
    local() {
      return this.to && this.to.startsWith('?');
    },
  },
  render() {
    if (isDownload(this.to)) {
      const attrs = { href: this.to, download: '' };
      return Vue.h('a', attrs, this.$slots.default());
    }
    if (this.local) {
      const RouterLink = Vue.resolveComponent('router-link');
      const attrs = { to: this.to };
      return Vue.h(RouterLink, attrs, this.$slots.default);
    } else {
      const attrs = { href: this.to, target: '_blank' };
      return Vue.h('a', attrs, this.$slots.default());
    }
  },
};
