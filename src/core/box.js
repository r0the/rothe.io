const BOX_TYPE = {
  info: {
    button: 'info',
    color: 'blue-light',
    text: 'Details',
  },
  warning: {
    button: 'warning',
    color: 'yellow-light',
    text: 'Details',
  },
};

export default {
  props: {
    type: { type: String, default: 'info' },
  },
  data() {
    return {
      showDetails: false,
    };
  },
  computed: {
    buttonClass() {
      return 'button is-' + this.typeInfo.button;
    },
    divClass() {
      return 'box has-background-' + this.typeInfo.color;
    },
    hasDetails() {
      return this.$slots.slot2;
    },
  },
  created() {
    this.typeInfo = BOX_TYPE[this.type] || BOX_TYPE.info;
  },
  methods: {
    toggleDetails() {
      this.showDetails = !this.showDetails;
    },
  },
  template: `
    <div :class="divClass">
      <slot name="slot1"></slot>
      <button v-if="hasDetails" :class="buttonClass" @click="toggleDetails">Details anzeigen</button>
      <div v-if="showDetails">
        <hr>
        <slot name="slot2"></slot>
      </div>
    </div>
  `,
};
