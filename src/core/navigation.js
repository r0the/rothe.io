import { createLinksNormalizer, loadJson, Storage } from '../common/service.js';

function copyNode(target, source) {
  target.title = source.title;
  target.page = '/' + source.page;
  target.icon = source.icon;
  if (source.items) {
    target.items = [];
    for (let i = 0; i < source.items.length; ++i) {
      const child = {};
      copyNode(child, source.items[i]);
      target.items.push(child);
    }
  }
}

function expandPath(base, page) {
  return base && page && page.charAt(0) !== '/' ? base + '/' + page : page;
}

function lastDescendant(node) {
  return node.items && node.items.length !== 0
    ? lastDescendant(node.items[node.items.length - 1])
    : node;
}

async function resolvePage(base, item) {
  if (item.include) {
    const i = await books.lookup(item.include);
    copyNode(item, i);
  }
  base = item.base || base;
  item.page = expandPath(base, item.page || 'index');
  const items = item.items;
  if (!items) return;
  if (items.length > 0) item.next = items[0];
  for (let i = 0; i < items.length; ++i) {
    const subItem = items[i];
    await resolvePage(base, subItem);
    if (i + 1 < items.length) lastDescendant(subItem).next = items[i + 1];
  }
}

function resolvePrev(node) {
  if (node.next) node.next.prev = node;
  if (node.items)
    node.items.forEach((subItem) => {
      resolvePrev(subItem);
    });
}

function _pageByUri(node, page) {
  if (node.page === page) return node;
  const items = node.items;
  if (!items) return;
  for (let i = 0; i < items.length; ++i) {
    const result = _pageByUri(items[i], page);
    if (result) return result;
  }
}

function _pageById(node, id) {
  if (node.id === id) return node;
  const items = node.items;
  if (!items) return;
  for (let i = 0; i < items.length; ++i) {
    const result = _pageById(items[i], id);
    if (result) return result;
  }
}

class Index {
  constructor(manager, name, config) {
    this.manager = manager;
    this.name = name;
    this.empty = name === 'none';
    if (this.empty) return;
    if (this.manager.type === 'book') {
      if (!config.id) console.warn('Buch ' + name + ' hat keine ID.');
      if (!config.icon) console.warn('Buch ' + name + ' hat kein Icon.');
    }
    if (!config.title) console.warn('Buch ' + name + ' hat keinen Titel.');
    Object.assign(this, config);
    this.cover = expandPath(this.base, this.cover);
    this.link = '?book=' + this.name;
  }

  async resolvePages() {
    await resolvePage(this.base, this);
    resolvePrev(this);
    if (!this.page) this.page = this.base + '/';
  }

  pageById(id) {
    return _pageById(this, id);
  }

  pageByUri(page) {
    return _pageByUri(this, page);
  }
}

class IndexManager {
  constructor(type) {
    this.type = type;
    this.map = {};
    this.empty = new Index(this, 'none', null);
  }

  async init(indexFiles) {
    const list = await Promise.all(indexFiles.map((file) => loadJson(file)));
    list.forEach((item) => {
      Object.assign(this.map, item);
    });
  }

  async lookup(name) {
    if (!name || !(name in this.map)) return this.empty;
    const item = this.map[name];
    if (typeof item !== 'string') return item;
    const json = await loadJson(item);
    const index = new Index(this, name, json);
    await index.resolvePages();
    this.map[name] = index;
    return index;
  }
}

class PageManager {
  open(page) {
    Storage.store('page', page);
    this.page = page;
    this.normalizeURI = createLinksNormalizer(this.page);
  }
}

export const books = new IndexManager('book');
export const courses = new IndexManager('course');
export const page = new PageManager();
