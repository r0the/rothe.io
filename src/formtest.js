(function () {
  const goBack = function () {
    window.history.back();
  };
  document.getElementById('back').addEventListener('click', goBack);
})();
