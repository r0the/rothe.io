function clamp(value) {
  const min = 0;
  const max = 255;
  return Math.min(Math.max(value, min), max);
}

export default class Color {
  constructor(r, g, b) {
    this.r = clamp(Math.floor(Number(r)));
    this.g = clamp(Math.floor(Number(g)));
    this.b = clamp(Math.floor(Number(b)));
  }

  hex() {
    const r = ('0' + Number(this.r).toString(16)).slice(-2);
    const g = ('0' + Number(this.g).toString(16)).slice(-2);
    const b = ('0' + Number(this.b).toString(16)).slice(-2);
    return '#' + r + g + b;
  }

  rgb() {
    return 'rgb(' + this.r + ', ' + this.g + ', ' + this.b + ')';
  }

  mix(other, weight) {
    const w = weight ? Number(weight) : 1;
    const r = (Number(this.r) + Number(other.r) * w) / (1 + w);
    const g = (Number(this.g) + Number(other.g) * w) / (1 + w);
    const b = (Number(this.b) + Number(other.b) * w) / (1 + w);
    return new Color(r, g, b);
  }
}
