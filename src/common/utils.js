export function toBinary(n, bits) {
  let result = '';
  for (let i = 0; i < bits; ++i) {
    if (n % 2) {
      result = '1' + result;
      --n;
    } else {
      result = '0' + result;
    }
    n /= 2;
  }
  return result;
}

export function byteToBinary(n) {
  return toBinary(n, 8);
}
