export default class Canvas {
  constructor() {
    if (arguments.length === 2) {
      this.el = document.createElement('canvas');
      this.el.width = arguments[0];
      this.el.height = arguments[1];
    } else if (typeof arguments[0] === 'string') {
      this.el = document.getElementById(arguments[0]);
    } else {
      this.el = arguments[0];
    }
    this.ctx = this.el.getContext('2d', { alpha: false });
    this.setFont('20pt Arial');
    this.isMirrored = false;
  }

  beginPath() {
    this.ctx.beginPath();
  }

  drawGreyscale(greyscale) {
    const imageData = this.ctx.createImageData(this.width(), this.height());
    const src = greyscale.data;
    const dst = imageData.data;
    let j;
    let color;
    for (let i = 0; i < src.length; ++i) {
      j = i << 2;
      color = src[i];
      dst[j] = color;
      dst[j + 1] = color;
      dst[j + 2] = color;
      dst[j + 3] = 255;
    }
    this.ctx.putImageData(imageData, 0, 0);
  }

  fastDrawImage(src) {
    if (src instanceof Canvas) src = src.el;
    const w = this.width();
    const h = this.height();
    this.ctx.drawImage(src, 0, 0, w, h, 0, 0, w, h);
  }

  drawImage(src, x, y, width, height) {
    if (src instanceof Canvas) src = src.el;
    this.ctx.drawImage(
      src,
      x || 0,
      y || 0,
      width || this.width(),
      height || this.height()
    );
  }

  drawPolygon(vertices) {
    this.ctx.moveTo(vertices[0].x, vertices[0].y);
    for (let i = 1; i < vertices.length; ++i) {
      this.ctx.lineTo(vertices[i].x, vertices[i].y);
    }
    this.ctx.lineTo(vertices[0].x, vertices[0].y);
  }

  drawText(text, x, y, color) {
    this.ctx.fillStyle = color;
    text.split('\n').forEach((line) => {
      this.ctx.fillText(line, x, y);
      y += 25;
    });
  }

  getImageData(x, y, width, height) {
    return this.ctx.getImageData(
      x || 0,
      y || 0,
      width || this.el.width,
      height || this.el.height
    );
  }

  fill(color) {
    this.ctx.beginPath();
    this.ctx.rect(0, 0, this.el.width, this.el.height);
    this.ctx.fillStyle = color;
    this.ctx.fill();
  }

  height() {
    return this.el.height;
  }

  lineTo(x, y) {
    this.ctx.lineTo(x, y);
  }

  mirror() {
    this.ctx.transform(-1, 0, 0, 1, this.width(), 0);
    this.isMirrored = !this.isMirrored;
  }

  moveTo(x, y) {
    this.ctx.moveTo(x, y);
  }

  setFont(font) {
    this.ctx.font = font;
  }

  setSize(width, height) {
    this.el.width = width;
    this.el.height = height;
  }

  stroke(color, width) {
    this.ctx.strokeStyle = color;
    this.ctx.lineWidth = width || 1;
    this.ctx.stroke();
    this.ctx.closePath();
  }

  width() {
    return this.el.width;
  }
}
