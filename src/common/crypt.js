import { Penta } from '../common/penta.js';

const polybiosLookup = {
  A: '11',
  B: '12',
  C: '13',
  D: '14',
  E: '15',
  F: '21',
  G: '22',
  H: '23',
  I: '24',
  K: '25',
  L: '31',
  M: '32',
  N: '33',
  O: '34',
  P: '35',
  Q: '41',
  R: '42',
  S: '43',
  T: '44',
  U: '45',
  W: '51',
  X: '52',
  Y: '53',
  Z: '54',
  ' ': '55',
};

export function rotChar(ch, offset) {
  const i = ch.charCodeAt(0) - 65;
  while (offset < 0) offset += 26;
  if (i >= 0 && i <= 26) {
    return String.fromCharCode(((i + offset) % 26) + 65);
  } else {
    return ch;
  }
}

export function caesar(text, key) {
  text = text.toUpperCase();
  let result = '';
  for (let i = 0; i < text.length; ++i) {
    result += rotChar(text.charAt(i), key);
  }
  return result;
}

function hash(text) {
  let hash = 0;
  if (text.length === 0) return hash;
  for (let i = 0; i < text.length; i++) {
    hash = (hash << 5) - hash + text.charCodeAt(i);
    hash = hash & hash;
  }

  return hash;
}

function getPolybiosKey(value) {
  for (const key in polybiosLookup) {
    if (polybiosLookup[key] === value) {
      return key;
    }
  }
  return '';
}

export function polybiosDecrypt(code) {
  let result = '';
  for (let i = 0; i < code.length; ++i) {
    result += getPolybiosKey(code[i]);
  }
  return result;
}

export function polybiosEncrypt(text) {
  text = text.toUpperCase();
  text = text.replace(/J/g, 'I');
  text = text.replace(/V/g, 'U');
  const result = [];
  for (let i = 0; i < text.length; ++i) {
    const ch = text.charAt(i);
    if (!(ch in polybiosLookup)) {
      // ignore (and drop) characters not included in Polybios table
      continue;
    }
    result.push(polybiosLookup[ch]);
  }
  return result;
}

const SANITIZE = [
  { find: /ä/g, replace: 'ae' },
  { find: /ö/g, replace: 'oe' },
  { find: /ü/g, replace: 'ue' },
  { find: /Ä/g, replace: 'Ae' },
  { find: /Ö/g, replace: 'Oe' },
  { find: /Ü/g, replace: 'Ue' },
];
// Sanitizes a German text by replacing Umlaute.
export function sanitize(text) {
  SANITIZE.forEach((rule) => {
    text = text.replace(rule.find, rule.replace);
  });
  return text;
}

export async function sha256(message) {
  const msgUint8 = new TextEncoder().encode(message);
  const buffer = await window.crypto.subtle.digest('SHA-256', msgUint8);
  const array = Array.from(new Uint8Array(buffer));
  return array.map((b) => b.toString(16).padStart(2, '0')).join('');
}

export function skytaleDecrypt(text, key) {
  let result = '';
  const lines = text.trim().split('\n');
  for (let i = 0; i < lines[0].length; ++i) {
    for (let j = 0; j < lines.length; ++j) {
      if (i < lines[j].length) {
        result += lines[j].charAt(i);
      }
    }
  }
  return result;
}

export function skytaleEncrypt(text, key) {
  let result = '';
  for (let i = 0; i < key; ++i) {
    for (let j = i; j < text.length; j += key) {
      result += text.charAt(j);
    }
    result += '\n';
  }
  return result;
}

export function substitutionDecrypt(text, key) {
  text = text.toUpperCase();
  let result = '';
  for (let i = 0; i < text.length; ++i) {
    const index = key.indexOf(text.charAt(i));
    if (index === -1) {
      result += text.charAt(i);
    } else {
      result += String.fromCharCode(index + 65);
    }
  }
  return result;
}

export function substitutionEncrypt(text, key) {
  text = text.toUpperCase();
  let result = '';
  for (let i = 0; i < text.length; ++i) {
    const code = text.charCodeAt(i) - 65;
    if (code >= 0 && code < 26) {
      result += key[text.charCodeAt(i) - 65];
    } else {
      result += text.charAt(i);
    }
  }
  return result;
}

function bitwiseXOR(a, b) {
  let result = '';
  for (let i = 0; i < a.length; ++i) {
    if (a[i] === b[i]) {
      result += '0';
    } else {
      result += '1';
    }
  }
  return result;
}

function buildBlocks(textBin, keyLen) {
  // build an array (blocks) of arrays (pentacode chars)
  const blocks = [];
  for (let i = 0; i < textBin.length; ++i) {
    if (i % keyLen === 0) {
      blocks.push([textBin[i]]);
    } else {
      blocks[blocks.length - 1].push(textBin[i]);
    }
  }
  return blocks;
}

function cbc(block, key, prevBlock, encryption) {
  const result = [];
  for (let i = 0; i < block.length; ++i) {
    if (encryption) {
      result.push(bitwiseXOR(bitwiseXOR(block[i], prevBlock[i]), key[i]));
    } else {
      result.push(bitwiseXOR(bitwiseXOR(block[i], key[i]), prevBlock[i]));
    }
  }
  return result;
}

function ecb(block, key) {
  const result = [];
  for (let i = 0; i < block.length; ++i) {
    result.push(bitwiseXOR(block[i], key[i]));
  }
  return result;
}

export function getRandomCharFromAlphabet() {
  const alphabet = Penta.getAlphabet();
  const ran = Math.floor(Math.random() * Math.floor(alphabet.length));
  return alphabet[ran];
}

export function xorBlockCipher(text, key, iv, mode, encryption) {
  if (key.length === 0) return '';
  const textBin = Penta.encode(text);
  const keyBin = Penta.encode(key);
  const ivBin = Penta.encode(iv);
  const blocks = buildBlocks(textBin, keyBin.length);
  const resultBlocks = [];
  let result = '';
  for (let i = 0; i < blocks.length; ++i) {
    if (mode === 'ecb') {
      resultBlocks.push(ecb(blocks[i], keyBin));
    } else if (mode === 'cbc') {
      if (i === 0) {
        // use iv on the first block
        resultBlocks.push(cbc(blocks[i], keyBin, ivBin, encryption));
      } else {
        if (encryption) {
          // use the previous output block for encryption
          resultBlocks.push(cbc(blocks[i], keyBin, resultBlocks[i - 1]));
        } else {
          // use the previous input block for decryption
          resultBlocks.push(cbc(blocks[i], keyBin, blocks[i - 1]));
        }
      }
    }
  }
  for (let i = 0; i < resultBlocks.length; ++i) {
    result += Penta.decode(resultBlocks[i]);
  }
  return result;
}
