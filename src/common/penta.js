const lookup = {
  ' ': '00000',
  A: '00001',
  B: '00010',
  C: '00011',
  D: '00100',
  E: '00101',
  F: '00110',
  G: '00111',
  H: '01000',
  I: '01001',
  J: '01010',
  K: '01011',
  L: '01100',
  M: '01101',
  N: '01110',
  O: '01111',
  P: '10000',
  Q: '10001',
  R: '10010',
  S: '10011',
  T: '10100',
  U: '10101',
  V: '10110',
  W: '10111',
  X: '11000',
  Y: '11001',
  Z: '11010',
  ',': '11011',
  '-': '11100',
  '.': '11101',
  '?': '11110',
  '@': '11111',
};

function getKey(value) {
  for (const key in lookup) {
    if (lookup[key] === value) {
      return key;
    }
  }
  return '';
}

export const Penta = {
  getAlphabet() {
    return Object.keys(lookup);
  },

  decode(code) {
    let result = '';
    for (let i = 0; i < code.length; ++i) {
      result += getKey(code[i]);
    }
    return result;
  },

  encode(text) {
    text = text.toUpperCase();
    const result = [];
    for (let i = 0; i < text.length; ++i) {
      const ch = text.charAt(i);
      if (!(ch in lookup)) {
        // ignore (and drop) characters not included in penta code
        continue;
      }
      result.push(lookup[ch]);
    }
    return result;
  },
};
