// Singleton API

export class Singleton {
  constructor(initFunction) {
    this.initFunction = initFunction;
    this.initPromise = null;
  }

  async lazyInit() {
    if (!this.initPromise) {
      this.initPromise = this.initFunction();
    }
    return this.initPromise;
  }

  async get() {
    return await this.lazyInit();
  }
}

// URL API

function base(page) {
  return page ? page.substr(0, page.lastIndexOf('/') + 1) : null;
}

export function baseUrl() {
  return document.location.origin + document.location.pathname;
}

const _parser = document.createElement('a');

function absolutePath(base, relative) {
  const stack = base.split('/');
  const parts = relative.split('/');
  stack.pop();
  parts.forEach((part) => {
    if (part === '..') {
      stack.pop();
    } else if (part !== '.') {
      stack.push(part);
    }
  });
  return stack.join('/');
}

export function createLinksNormalizer(page) {
  const pageBase = base(page);
  function normalizePath(path) {
    return path.startsWith('/') ? path.substr(1) : absolutePath(pageBase, path);
  }
  return (path) => {
    if (!path) return;
    _parser.href = path;
    if (
      _parser.hostname !== window.location.hostname ||
      _parser.port !== window.location.port
    )
      return path;
    if (path.startsWith('?')) {
      const search = new URLSearchParams(_parser.search);
      if (search.has('page'))
        search.set('page', normalizePath(search.get('page')));
      return '?' + search.toString();
    }
    const x = normalizePath(path);
    return x;
  };
}

// fetch API

export async function loadJson(url) {
  const response = await window.fetch(url);
  if (response.ok) {
    return await response.json();
  } else {
    throw new Error('Cannot load ' + url + ', status: ' + response.status);
  }
}

export function loadScript(url) {
  return new Promise(function (resolve, reject) {
    const script = document.createElement('script');
    document.body.appendChild(script);
    script.async = true;
    script.onload = resolve;
    script.onerror = reject;
    script.src = url;
  });
}

export async function loadText(url) {
  const response = await window.fetch(url);
  if (response.ok) {
    return await response.text();
  } else {
    throw new Error('Cannot load ' + url + ', status: ' + response.status);
  }
}

// Licence API

const LICENCES = new Singleton(async function () {
  return await loadJson('data/licence.json');
});

async function getLicence(name) {
  const licences = await LICENCES.get();
  return licences[name];
}

// Image API

async function lookupLicence(info) {
  const name = info ? info.licence : null;
  const licence = await getLicence(name);
  if (licence) {
    info.licence_name = licence.description;
    info.licence_url = licence.url;
  }
  return info;
}

export async function loadImageInfo(url) {
  if (!url) return;
  const j = url.lastIndexOf('.');
  const infoUrl = url.substring(0, j) + '.json';
  let json = null;
  json = await loadJson(infoUrl).catch(async function () {
    const i = url.lastIndexOf('/');
    const dir = url.substring(0, i);
    const infoFile = dir + '/images.json';
    json = await loadJson(infoFile).catch((err) => {
      console.warn('Kann keine Bildinformationen zu ' + url + ' finden.');
    });
  });
  return await lookupLicence(json);
}

// Storage API

export const Storage = {
  init(namespace) {
    this.namespace = namespace;
  },
  load(name) {
    if (this.namespace) name = this.namespace + '.' + name;
    return window.localStorage.getItem(name);
  },
  store(name, value) {
    if (this.namespace) name = this.namespace + '.' + name;
    if (value) {
      return window.localStorage.setItem(name, value);
    } else {
      return window.localStorage.removeItem(name);
    }
  },
};
