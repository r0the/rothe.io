import { byteToBinary } from '../common/utils.js';

const VueColourSlider = {
  props: {
    colour: { type: String },
    percent: { type: Boolean, default: false },
    modelValue: { type: Number, default: 0 },
    name: { type: String },
  },
  data() {
    return {
      value: this.modelValue,
    };
  },
  emits: ['update:modelValue'],
  computed: {
    displayValue: {
      get() {
        return this.value;
      },
      set(value) {
        this.value = Number.parseInt(value);
        this.$emit('update:modelValue', this.value);
      },
    },
    max: function () {
      return this.percent ? 100 : 255;
    },
  },
  template: `
    <div class="columns is-vcentered">
      <div class="column is-2">{{ name }}</div>
      <div class="column">
        <input type="range" :class="['slider', colour]" v-model="displayValue" :min="0" :max="max">
      </div>
    </div>
  `,
};

export default {
  props: {
    binary: { type: Boolean, default: false },
    percent: { type: Boolean, default: false },
    type: { type: String, default: 'rgb' },
    output: { type: String },
  },
  data: () => {
    return {
      c: 20,
      m: 20,
      y: 20,
      k: 20,
      r: 255,
      g: 220,
      b: 150,
      a: 255,
    };
  },
  components: {
    VueColourSlider,
  },
  computed: {
    bits: function () {
      const r = this.toByte(this.r);
      const g = this.toByte(this.g);
      const b = this.toByte(this.b);
      return byteToBinary(r) + ' ' + byteToBinary(g) + ' ' + byteToBinary(b);
    },
    tuple: function () {
      this.update();
      const r = this.toOutput(this.r);
      const g = this.toOutput(this.g);
      const b = this.toOutput(this.b);
      return '(' + r + ', ' + g + ', ' + b + ')';
    },
    styleBackground: function () {
      if (this.type !== 'rgba') return {};
      return {
        'border-radius': '6px',
        'background-image': 'url(images/wellington.jpg)',
      };
    },
    cssRGBA: function () {
      this.update();
      const r = this.toByte(this.r);
      const g = this.toByte(this.g);
      const b = this.toByte(this.b);
      const a = this.toByte(this.a) / 255;
      return 'rgb(' + r + ', ' + g + ', ' + b + ', ' + a + ')';
    },
  },
  methods: {
    toOutput: function (x) {
      return this.percent ? x / 100 : x;
    },
    toByte: function (x) {
      return this.percent ? x * 2.55 : x;
    },
    update: function () {
      if (this.type === 'cmyk') {
        const f = 0.0255 * (100 - this.k);
        this.r = Math.round((100 - this.c) * f);
        this.g = Math.round((100 - this.m) * f);
        this.b = Math.round((100 - this.y) * f);
      }
    },
  },
  template: `
    <div class="box box-colour" :style="styleBackground">
      <div class="box-colour-inner" :style="{ backgroundColor: cssRGBA }">
        <h4>{{ type.toUpperCase() }}-Farbwahl</h4>
        <div v-if="type.startsWith('rgb')">
          <VueColourSlider v-model="r" colour="is-danger" :percent="percent" name="Rot" ></VueColourSlider>
          <VueColourSlider v-model="g" colour="is-success" :percent="percent" name="Grün" ></VueColourSlider>
          <VueColourSlider v-model="b" colour="is-info" :percent="percent" name="Blau"></VueColourSlider>
          <VueColourSlider v-if="type === 'rgba'" v-model="a" colour="is-black" :percent="percent" name="Alpha"></VueColourSlider>
        </div>
        <div v-if="type === 'cmyk'">
          <VueColourSlider v-model="c" colour="is-cyan" percent name="Cyan"></VueColourSlider>
          <VueColourSlider v-model="m" colour="is-magenta" percent name="Magenta" ></VueColourSlider>
          <VueColourSlider v-model="y" colour="is-yellow" percent name="Gelb"></VueColourSlider>
          <VueColourSlider v-model="k" colour="is-black" percent name="Key"></VueColourSlider>
        </div>
        <div v-if="binary" class="columns is-vcentered">
          <div class="column is-2">Binär</div>
          <div class="column">
            <input class="input" v-model="bits" readonly>
          </div>
        </div>
        <div v-if="output === 'tuple'" class="columns is-vcentered">
          <div class="column is-2">Tupel</div>
          <div class="column">
            <input class="input" v-model="tuple" readonly>
          </div>
        </div>
        <div v-if="output === 'css'" class="columns is-vcentered">
          <div class="column is-2">CSS-Wert</div>
          <div class="column">
            <input class="input" v-model="cssRGBA" readonly>
          </div>
        </div>
      </div>
    </div>
  `,
};
