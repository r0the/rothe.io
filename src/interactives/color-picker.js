import Color from '../common/color.js';

export default {
  props: {
    modelValue: { type: Number, default: 0 },
  },
  emits: ['update:modelValue'],
  data() {
    return {
      max: 255,
      value: this.modelValue,
    };
  },
  methods: {
    update() {
      this.$emit('update:modelValue', this.value);
    },
  },
  template: `
    <div>
      <div style="display: flex">
      <input
          type="range"
          style="flex-grow: 2"
          v-model="value.r"
          class="red"
          id="red"
          :max="max"
          @input="update"
      /><label for="red" style="width: 2.5em; text-align: right">{{
          value.r
      }}</label>
      </div>
      <div style="display: flex">
      <input
          type="range"
          style="flex-grow: 2"
          v-model="value.g"
          class="green"
          id="green"
          :max="max"
          @input="update"
      /><label for="green" style="width: 2.5em; text-align: right">{{
          value.g
      }}</label>
      </div>
      <div style="display: flex">
      <input
          type="range"
          style="flex-grow: 2"
          v-model="value.b"
          class="blue"
          id="blue"
          :max="max"
          @input="update"
      /><label for="blue" style="width: 2.5em; text-align: right">{{
          value.b
      }}</label>
      </div>
      <div class="output" :style="'background-color:'+value.hex()"></div>

  </div>
  `,
};

//<style>
//
//.output {
//  width: 100%;
//  height: 20px;
//  border: 1px solid black;
//  margin-top: 12px;
//}
//
//</style>
