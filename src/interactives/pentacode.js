import { Penta } from '../common/penta.js';

export default {
  data: function () {
    return {
      text: '',
      key: 'D',
      pentacode: '',
    };
  },
  methods: {
    decode: function () {
      const trimmed = this.pentacode.trim().replace(/\n/g, ' ');
      this.text = Penta.decode(trimmed.split(' '));
    },
    encode: function () {
      const result = Penta.encode(this.text.trim());
      this.pentacode = result.join(' ');
    },
  },
  template: `
    <div class="box interactive">
      <h4>Pentacode</h4>
      <div class="field">
        <label class="label">Text</label>
        <textarea class="textarea" placeholder="Text" v-model="text"></textarea>
      </div>
      <div class="buttons">
        <button class="button is-primary" @click="encode"><v-icon name="mdi-arrow-down-thick"/> Codieren</button>
        <button class="button is-primary" @click="decode"><v-icon name="mdi-arrow-up-thick"/> Decodieren</button>
      </div>
      <div class="field">
        <label class="label">Pentacode</label>
        <textarea class="textarea" placeholder="" v-model="pentacode"></textarea>
      </div>
    </div>
  `,
};
