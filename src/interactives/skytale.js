import { skytaleDecrypt, skytaleEncrypt } from '../common/crypt.js';

export default {
  data() {
    return {
      crypttext: '',
      key: 2,
      plaintext: '',
    };
  },
  methods: {
    decrypt() {
      this.plaintext = skytaleDecrypt(this.crypttext, this.key);
    },
    encrypt() {
      this.crypttext = skytaleEncrypt(this.plaintext, this.key);
    },
  },
  template: `
    <div class="box interactive">
      <h4>Skytale</h4>
      <div class="field">
        <label class="label">Klartext</label>
        <textarea class="textarea" placeholder="Klartext" v-model="plaintext"></textarea>
      </div>
      <label class="label">Schlüssel</label>
      <v-number-picker v-model="key"/>
      <div class="buttons">
          <button class="button is-primary" @click="encrypt"><v-icon name="mdi-arrow-down-thick"/> Verschlüsseln</button>
          <button class="button is-primary" @click="decrypt"><v-icon name="mdi-arrow-up-thick"/> Entschlüsseln</button>
      </div>
      <div class="field">
        <label class="label">Geheimtext</label>
        <textarea class="textarea" placeholder="Geheimtext" v-model="crypttext"></textarea>
      </div>
    </div>
  `,
};
