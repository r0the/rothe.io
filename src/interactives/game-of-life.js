function createCell(alive) {
  return {
    alive: alive,
    colour: alive ? 'red' : 'white',
  };
}

function randomBoolean() {
  return Math.floor(Math.random() * 2) === 0;
}

function parseIntList(str) {
  const result = [];
  const items = str.split(',');
  for (let i = 0; i < items.length; ++i) {
    const n = parseInt(items[i]);
    if (n >= 0 && n < 9) {
      result.push(n);
    } else {
      return;
    }
  }
  return result;
}

export default {
  data() {
    return {
      birthRule: '3',
      birthRuleParsed: [3],
      birthRuleType: 'is-success',
      surviveRule: '2,3',
      surviveRuleParsed: [2, 3],
      surviveRuleType: 'is-success',
      size: 50,
      speed: 25,
      timer: null,
      torus: false,
      world: null,
    };
  },
  created() {
    this.reset();
  },
  computed: {
    playIcon() {
      return this.timer ? 'mdi-pause' : 'mdi-play';
    },
    delay() {
      return 1000 - (this.speed - 1) * 20;
    },
  },
  watch: {
    birthRule(newValue) {
      const result = parseIntList(newValue);
      this.birthRuleType = result ? 'is-success' : 'is-danger';
      if (result) {
        this.birthRuleParsed = result;
      }
    },
    surviveRule(newValue) {
      const result = parseIntList(newValue);
      this.surviveRuleType = result ? 'is-success' : 'is-danger';
      if (result) {
        this.surviveRuleParsed = result;
      }
    },
    speed(newValue) {
      if (this.timer) {
        window.clearInterval(this.timer);
        this.timer = window.setInterval(this.step, this.delay);
      }
    },
  },
  methods: {
    clear() {
      this.init(() => {
        return createCell(false);
      });
    },
    countNeighbours(x, y) {
      let count = 0;
      for (let yn = y - 1; yn <= y + 1; ++yn) {
        for (let xn = x - 1; xn <= x + 1; ++xn) {
          if ((xn !== x || yn !== y) && this.isAlive(xn, yn)) {
            ++count;
          }
        }
      }
      return count;
    },
    init(initCell) {
      this.world = [];
      for (let y = 0; y < this.size; ++y) {
        const row = [];
        for (let x = 0; x < this.size; ++x) {
          row.push(initCell());
        }
        this.world.push(row);
      }
    },
    nextState(x, y) {
      const neighbours = this.countNeighbours(x, y);
      if (this.isAlive(x, y)) {
        return this.surviveRuleParsed.indexOf(neighbours) !== -1;
      } else {
        return this.birthRuleParsed.indexOf(neighbours) !== -1;
      }
    },
    reset() {
      this.init(() => {
        return createCell(randomBoolean());
      });
    },
    isAlive(x, y) {
      if (this.torus) {
        if (x < 0) x += this.size;
        if (y < 0) y += this.size;
        x %= this.size;
        y %= this.size;
      }
      const row = this.world[y];
      return row && row[x] && row[x].alive;
    },
    step() {
      const newWorld = [];
      for (let y = 0; y < this.size; ++y) {
        const row = [];
        for (let x = 0; x < this.size; ++x) {
          row.push(createCell(this.nextState(x, y)));
        }
        newWorld.push(row);
      }
      this.world = newWorld;
    },
    toggleCell(x, y, cell) {
      this.world[y][x] = createCell(!cell.alive);
    },
    togglePlay() {
      if (this.timer) {
        window.clearInterval(this.timer);
        this.timer = null;
      } else {
        this.timer = window.setInterval(this.step, this.delay);
      }
    },
  },
  template: `
    <div class="box interactive">
      <h4>Game of Life</h4>
      <div class="columns">
        <div class="column is-9">
          <VueGrid @click="toggleCell" :cell-size="12" :data="world"></VueGrid>
        </div>
        <div class="column is-3">
          <div class="field">
            <button class="button" @click="togglePlay"><v-icon :name="playIcon"/></button>
          </div>
          <div class="field">
            <label class="label">Geschwindigkeit</label>
            <input class="slider" min="1" max="40" v-model="speed" type="range">
          </div>
          <div class="field">
            <button class="button" @click="clear">Löschen</button>
            <button class="button" @click="reset">Zufall</button>
            </div>
          <div class="field">
            <input class="switch is-default" id="torus" type="checkbox" v-model="torus">
            <label for="torus">Torus-Welt</label>
          </div>
          <div class="field" label="Überlebensregel" :type="surviveRuleType">
            <input class="input" v-model="surviveRule">
          </div>
          <div class="field" label="Geburtsregel" :type="birthRuleType">
            <input class="input" v-model="birthRule">
          </div>
        </div>
      </div>
    </div>
  `,
};
