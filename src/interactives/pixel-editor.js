/* global Vue */
function createPixel(bit) {
  return {
    colour: bit === 1 ? 'white' : 'black',
    bit: bit,
  };
}

function pixmapToBits(pixmap) {
  let result = '';
  for (let y = 0; y < pixmap.length; ++y) {
    const row = pixmap[y];
    for (let x = 0; x < row.length; ++x) {
      result += row[x].bit;
    }
    if (y < pixmap.length - 1) {
      // no newline at the end
      result += '\n';
    }
  }
  return result;
}

function bitsToPixmap(bits, width, height) {
  let pixmap = [];
  let index = 0;
  for (let y = 0; y < height; ++y) {
    const row = [];
    for (let x = 0; x < width; ++x) {
      row.push(createPixel(parseInt(bits.charAt(index))));
      index += 1;
    }
    pixmap.push(row);
    index += 1; // skip space between groups of 5 bits
  }
  return pixmap;
}

export default {
  props: {
    cellSize: { type: Number, default: 20 },
    width: { type: Number, default: 8 },
    height: { type: Number, default: 8 },
  },
  data() {
    return {
      bits: '',
      pixmap: [],
    };
  },
  mounted() {
    this.bits = '00000\n00000\n00000\n00000\n00000';
    this.pixmap = bitsToPixmap(this.bits, this.width, this.height);
  },
  methods: {
    toggleCell(x, y, cell) {
      this.pixmap[y][x] = createPixel(cell.bit === 1 ? 0 : 1);
      this.bits = pixmapToBits(this.pixmap);
    },
    updateValue(value) {
      this.bits = value;
      this.pixmap = bitsToPixmap(value, this.width, this.height);
    },
  },
  template: `
    <div class="box interactive">
      <h4>Pixel-Editor</h4>
      <div class="columns">
        <div class="column is-9">
          <div>
            <VueGrid @click="toggleCell" :cell-size="cellSize" :data="pixmap"></VueGrid>
          </div>
        </div>
        <div class="column is-3">
          <textarea @input="updateValue($event.target.value)" class="textarea" v-model="bits" style="font-family: monospace"></textarea>
        </div>
      </div>
    </div>
  `,
};
