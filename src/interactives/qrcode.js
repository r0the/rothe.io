import { loadScript, Singleton } from '../common/service.js';

const QRIOUS = new Singleton(async function () {
  await loadScript('lib/qrious/qrious.min.js');
  return window.QRious;
});

export default {
  props: {
    size: { type: Number, default: 200 },
  },
  data() {
    return {
      text: '',
    };
  },
  async created() {
    const QRious = await QRIOUS.get();
    this.qr = new QRious({
      element: this.$refs.canvas,
      size: this.size,
      value: 'https://rothe.io',
    });
  },
  methods: {
    download() {
      const link = document.createElement('a');
      link.href = this.qr.toDataURL();
      link.download = 'QR-Code.png';
      link.click();
    },
    update(val) {
      console.log(val);
    },
  },
  watch: {
    text(newText) {
      this.qr.value = newText;
    },
  },
  template: `
    <div class="box interactive">
      <h4>QR-Code</h4>
      <div class="field">
        <label class="label">Text</label>
        <input class="input" v-model="text">
      </div>
      <div class="field">
        <label class="label">Ausgabe</label>
        <canvas ref="canvas" width="400" height="400"></canvas>
        <button @click="download" class="button">Herunterladen</button>
      </div>
    </div>
  `,
};
