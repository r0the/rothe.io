import { getRandomCharFromAlphabet, xorBlockCipher } from '../common/crypt.js';

export default {
  data() {
    return {
      crypttext: '',
      encryption: true,
      iv: '',
      key: '',
      mode: 'ecb',
      plaintext: '',
    };
  },
  methods: {
    decrypt() {
      this.plaintext = xorBlockCipher(
        this.crypttext,
        this.key,
        this.iv,
        this.mode,
        !this.encryption,
      );
    },
    encrypt() {
      this.crypttext = xorBlockCipher(
        this.plaintext,
        this.key,
        this.iv,
        this.mode,
        this.encryption,
      );
    },
    randomIV() {
      this.iv = '';
      for (let i = 0; i < this.key.length; ++i) {
        this.iv += getRandomCharFromAlphabet();
      }
    },
  },
  template: `
    <div class="box interactive">
      <h4>XOR-Blockchiffre</h4>
      <div class="field">
        <label class="label">Klartext</label>
        <textarea class="textarea" placeholder="Klartext" v-model="plaintext"></textarea>
      </div>
      <div class="field">
        <label class="label">Schlüssel</label>
        <input class="input" type="text" placeholder="Schlüssel" v-model="key">
      </div>
      <div class="field is-grouped">
        <div class="control">
          <button class="button is-primary" @click="encrypt" :disabled="key == '' || (mode == 'cbc' && iv.length != key.length)"><v-icon name="mdi-arrow-down-thick"/> Verschlüsseln</button>
          <button class="button is-primary" @click="decrypt" :disabled="key == '' || (mode == 'cbc' && iv.length != key.length)"><v-icon name="mdi-arrow-up-thick"/> Entschlüsseln</button>
          <button class="button is-primary" @click="randomIV" :disabled="mode == 'ecb'">IV zufällig setzen</button>
        </div>
        <div class="select">
          <select v-model="mode">
            <option selected value="ecb">ECB</option>
            <option value="cbc">CBC</option>
          </select>
        </div>
      </div>
      <div class="field" v-if="mode == 'cbc'">
        <label class="label">IV</label>
        <input class="input" type="text" placeholder="Der IV muss gleich lange sein wie der Schlüssel" v-model="iv">
      </div>
      <div class="field">
        <label class="label">Geheimtext</label>
        <textarea class="textarea" placeholder="Geheimtext" v-model="crypttext"></textarea>
      </div>
    </div>
  `,
};
