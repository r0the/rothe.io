import { polybiosDecrypt, polybiosEncrypt } from '../common/crypt.js';

export default {
  data() {
    return {
      crypttext: '',
      plaintext: '',
    };
  },
  methods: {
    decrypt() {
      this.plaintext = polybiosDecrypt(this.crypttext.trim().split(' '));
    },
    encrypt() {
      const result = polybiosEncrypt(this.plaintext.trim());
      this.crypttext = result.join(' ');
    },
  },
  template: `
    <div class="box interactive">
      <h4>Polybios-Chiffre</h4>
      <div class="field">
        <label class="label">Klartext</label>
        <textarea class="textarea" placeholder="Klartext" v-model="plaintext"></textarea>
      </div>
      <div class="buttons">
          <button class="button is-primary" @click="encrypt"><v-icon name="mdi-arrow-down-thick"/> Verschlüsseln</button>
          <button class="button is-primary" @click="decrypt"><v-icon name="mdi-arrow-up-thick"/> Entschlüsseln</button>
      </div>
      <div class="field">
        <label class="label">Geheimtext</label>
        <textarea class="textarea" placeholder="Geheimtext" v-model="crypttext"></textarea>
      </div>
    </div>
  `,
};
