export default {
  data() {
    return {
      value: 0,
    };
  },
  template: `
    <div class="box interactive">
      <h4>Binär-Umrechner</h4>
      <div class="field">
        <label class="label">Dezimal</label>
        <v-input type="integer" :radix="10" v-model="value"/>
      </div>
      <div class="field">
        <label class="label">Binär</label>
        <v-input type="integer" :radix="2" v-model="value"/>
      </div>
      <div class="field">
        <label class="label">Hexadezimal</label>
        <v-input type="integer" :radix="16" v-model="value"/>
      </div>
    </div>
  `,
};
