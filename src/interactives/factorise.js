import Prime from '../common/prime.js';

export default {
  data() {
    return {
      min: 5000000,
      max: 10000000,
      a: 0,
      b: 0,
      m: 0,
      x: 0,
      y: 0,
      step: 0,
      time_mult: 0,
      time_fact: 0,
    };
  },
  methods: {
    choose() {
      this.step = 1;
      this.a = Prime.randomPrime(this.min, this.max);
      this.b = Prime.randomPrime(this.min, this.max);
    },
    multiply() {
      this.step = 2;
      const t1 = window.performance.now();
      this.m = this.a * this.b;
      const t2 = window.performance.now();
      this.time_mult = t2 - t1;
    },
    factorise() {
      this.step = 3;
      const t1 = window.performance.now();
      let n = 2;

      while (n < this.m) {
        const r = this.m / n;
        if (r % 1 === 0) {
          this.x = n;
          this.y = r;
          break;
        }
        if (n === 2) {
          n++;
        } else {
          n += 2;
        }
      }
      const t2 = window.performance.now();
      this.time_fact = t2 - t1;
    },
  },
  template: `
    <div class="box interactive">
      <div class="row">
        <button @click="choose" class="button">
          Zufällig zwei Primzahlen zwischen {{min.toLocaleString("de-CH")}} und {{max.toLocaleString("de-CH")}} wählen
        </button>
      </div>
      <div class="row" v-if="step > 0">
        1. Primzahl: {{ a.toLocaleString("de-CH") }}, 2. Primzahl: {{ b.toLocaleString("de-CH") }}
      </div>
      <div class="row" v-if="step > 0">
        <button @click="multiply" class="button">Diese Primzahlen multiplizieren</button>
      </div>
      <div class="row" v-if="step > 1">
        {{ a.toLocaleString("de-CH") }} x {{ b.toLocaleString("de-CH") }} = {{ m.toLocaleString("de-CH") }} <small v-if="time_mult > 1">({{ time_mult.toFixed(0) }} ms)</small><small v-else>(<1 ms)</small>
      </div>
      <div class="row" v-if="step > 1">
        <button @click="factorise" class="button">
          Das Ergebnis wieder in Primzahlen zerlegen
        </button>
      </div>
      <div class="row" v-if="step > 2">
        {{ m.toLocaleString("de-CH") }} = {{ x.toLocaleString("de-CH") }} x {{ y.toLocaleString("de-CH") }} <small>({{ time_fact.toFixed(0) }} ms)</small>
      </div>
    </div>
  `,
};
