import { rotChar } from '../common/crypt.js';
import { loadJson } from '../common/service.js';

const ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ';

function frequencyMap(text, alphaOnly) {
  text = text.toUpperCase();
  const result = {};
  const len = text.length;
  let count = 0;
  for (let i = 0; i < len; ++i) {
    const ch = text.charAt(i);

    if (!alphaOnly || ALPHA.indexOf(ch) !== -1) {
      ++count;
      if (ch in result) {
        result[ch] += 1;
      } else {
        result[ch] = 1;
      }
    }
  }
  for (const ch in result) result[ch] = (100 * result[ch]) / count;
  return result;
}

function toSortedList(map, order) {
  const list = [];
  if (order) {
    const keys = [];
    for (const ch in map) keys.push(ch);
    keys.sort();
    for (let i = 0; i < keys.length; ++i) list.push([keys[i], map[keys[i]]]);
  } else {
    for (const ch in map) list.push([ch, map[ch]]);
    list.sort(function (a, b) {
      return b[1] - a[1];
    });
  }

  return list;
}

export default {
  data() {
    return {
      key: 'A',
      chartConfig: {
        responsive: true,
        data: {
          labels: [],
          datasets: [
            {
              type: 'bar',
              backgroundColor: 'rgba(255, 0, 0, 0.6)',
              data: [],
            },
            {
              type: 'line',
              backgroundColor: 'rgba(0, 255, 0, 0.6)',
              data: [],
            },
          ],
        },
        options: {
          animation: false,
          plugins: {
            legend: {
              display: false,
            },
          },
        },
      },
      alphaOnly: true,
      alphabet: {},
      order: true,
      text: this.message,
    };
  },
  props: [
    'message'
  ],
  async mounted() {
    this.chart = new window.Chart(this.$refs.canvas, this.chartConfig);
    this.alphabet = await loadJson('./src/interactives/letter-frequency.json');
    this.update();
  },
  updated() {
    this.update();
  },
  methods: {
    update() {
    const map = frequencyMap(this.text, this.alphaOnly);
    const chars = [];
    const freq = [];
    const de_freq = [];

    // shift alphabet
    const shift = 'A'.charCodeAt(0) - this.key.charCodeAt(0);

    for (let i = 0; i < ALPHA.length; ++i) {
      const ch = ALPHA.charAt(i);
      const ch_shifted = rotChar(ch, shift);
      chars.push(ch);
      de_freq.push(this.alphabet.de[ch_shifted]);
      freq.push(map[ch]);
    }
    this.chartConfig.data.labels = chars;
    this.chartConfig.data.datasets[0].data = freq;
    this.chartConfig.data.datasets[1].data = de_freq;
    this.chart.update();
    }
  },
  computed: {
    countSpaceLabel() {
      return this.alphaOnly ? 'nur Buchstaben' : 'alle Zeichen';
    },
    orderLabel() {
      return 'Sortiere ' + (this.order ? 'alphabetisch' : 'nach Häufigkeit');
    },
  },
  template: `
    <div class="box interactive">
      <h4>Häufigkeitsanalyse</h4>
      <div class="field">
        <label class="label">Text</label>
        <textarea class="textarea" placeholder="Text" spellcheck="false" v-model="text"></textarea>
      </div>
      <v-letter-picker v-model="key"/>
      <canvas ref="canvas"></canvas>
    </div>
  `,
};
