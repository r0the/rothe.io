import { sanitize } from '../common/crypt.js';

export default {
  data() {
    return {
      output: '',
      input: '',
    };
  },
  methods: {
    process(remove) {
      const input = sanitize(this.input);
      remove = remove.toUpperCase() + remove.toLowerCase();
      let out = '';
      for (let i = 0; i < input.length; ++i) {
        const char = input.charAt(i);
        if (remove.indexOf(char) === -1) out += char;
      }
      this.output = out;
    },
  },
  template: `
    <div class="box interactive">
      <h4>Redundanz</h4>
      <div class="field">
        <label class="label">Originaltext</label>
          <textarea class="textarea" placeholder="Originaltext" v-model="input"></textarea>
      </div>
      <div class="buttons">
        <button class="button is-primary" @click="process('ENIS')">häufige entfernen</button>
        <button class="button is-primary" @click="process('TDHULCGMOBWFKZPVJYXQ')">seltene entfernen</button>
      </div>
      <div class="field">
        <label class="label">Ausgabe</label>
        <textarea class="textarea" placeholder="Ausgabe" v-model="output"></textarea>
      </div>
    </div>
  `,
};
