import { caesar } from '../common/crypt.js';

export default {
  data() {
    return {
      crypttext: '',
      key: 'D',
      plaintext: '',
    };
  },
  methods: {
    decrypt() {
      const shift = this.key.charCodeAt(0) - 'A'.charCodeAt(0);
      this.plaintext = caesar(this.crypttext, -shift);
    },
    encrypt() {
      const shift = this.key.charCodeAt(0) - 'A'.charCodeAt(0);
      this.crypttext = caesar(this.plaintext, shift);
    },
  },
  template: `
    <div class="box interactive">
      <h4>Caesar-Verschlüsselung</h4>
      <div class="field">
        <label class="label">Klartext</label>
        <textarea class="textarea" placeholder="Klartext" v-model="plaintext"></textarea>
      </div>
      <label class="label">Schlüssel</label>
      <v-letter-picker v-model="key"/>
      <div class="buttons">
          <button class="button is-primary" @click="encrypt"><Icon name="mdi-arrow-down-thick"/> Verschlüsseln</button>
          <button class="button is-primary" @click="decrypt"><Icon name="mdi-arrow-up-thick"/> Entschlüsseln</button>
      </div>
      <div class="field">
        <label class="label">Geheimtext</label>
        <textarea class="textarea" placeholder="Geheimtext" v-model="crypttext"></textarea>
      </div>
    </div>
  `,
};
