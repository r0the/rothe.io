const VueFlipCard = {
  props: {
    modelValue: { type: Boolean, default: false },
  },
  emits: ['update:modelValue'],
  data() {
    return {
      front: this.modelValue,
    };
  },
  methods: {
    flip() {
      this.front = !this.front;
      this.$emit('update:modelValue', this.front);
    },
  },
  template: `
    <div :class="{'vue-flip-card': true, 'is-flipped': !front }">
      <div @click="flip"><div class="card"><slot></slot></div><div class="digit">1</div></div>
      <div @click="flip"><div class="back"></div><div class="digit">0</div></div>
    </div>
  `,
};

export default {
  components: {
    VueFlipCard,
  },
  data() {
    return {
      items: [
        { number: 16, visible: false },
        { number: 8, visible: false },
        { number: 4, visible: false },
        { number: 2, visible: false },
        { number: 1, visible: false },
      ],
    };
  },
  methods: {
    lines(number) {
      return number < 8 ? Math.round(number / 2) : Math.round(number / 4);
    },
    dots(number) {
      number = number / this.lines(number);
      let result = '';
      for (let i = 0; i < number; ++i) {
        result += '⬤ ';
      }
      return result;
    },
    visibleCount() {
      let total = 0;
      for (let i = 0; i < this.items.length; ++i) {
        if (this.items[i].visible) total += this.items[i].number;
      }
      return total;
    },
  },
  template: `
    <div class="box interactive">
      <h4>Binäre Karten</h4>
      <div class="columns is-vcentered">
        <div class="column is-2 vue-binary-cards-total">{{ visibleCount() }}
        </div>
        <div v-for="item in items" class="column is-2"><VueFlipCard v-model="item.visible" v>
          <div v-for="i in lines(item.number)">{{ dots(item.number) }}
          </div>
        </VueFlipCard></div>
      </div>
    </div>
  `,
};
