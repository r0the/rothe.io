import { toBinary } from '../common/utils.js';

export default {
  data() {
    return {
      binary: '',
      bits: 4,
      number: 0,
    };
  },
  created() {
    this.randomNumber();
  },
  methods: {
    randomNumber() {
      this.number = Math.floor(Math.random() * Math.pow(2, this.bits));
      this.binary = '?'.repeat(this.bits);
    },
    showBinary() {
      this.binary = toBinary(this.number, this.bits);
    },
  },
  template: `<div class="box interactive">
    <div style="font-size: 100pt; text-align: center">
      {{ number }}
    </div>
    <div style="font-size: 50pt; text-align: center">
      {{ binary }}
    </div>
    <label class="label">Anzahl Bits</label>
    <v-number-picker v-model="bits" min="4" max="8"/>
    <div class="buttons">
      <button class="button is-primary" @click="randomNumber"><v-icon name="mdi-dice-5"/> neue  Zahl</button>
      <button class="button is-primary" @click="showBinary"><v-icon name="mdi-eye"/> Binärzahl anzeigen</button>
    </div>
  </div>`,
};
