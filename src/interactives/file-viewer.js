import { byteToBinary } from '../common/utils.js';

const MAX_FILE_SIZE = 200000;

export default {
  data() {
    return {
      fileName: '',
      fileType: '',
      view: 'ASCII',
      output: '',
    };
  },
  watch: {
    view(newView) {
      console.log(this.view);
      this.renderFile();
    },
  },
  methods: {
    loadFile(event) {
      const files = event.target.files;
      if (files.length !== 1) return;
      this.fileName = files[0].name;
      this.fileType = files[0].type;
      console.log(files[0]);
      if (files[0].size > MAX_FILE_SIZE) {
        this.output = 'Datei ist zu gross.';
        return;
      }
      const reader = new window.FileReader();
      reader.onload = this.loadedFile;
      reader.readAsArrayBuffer(files[0]);
    },
    loadedFile(event) {
      this.content = new DataView(event.target.result);
      this.renderFile();
    },
    renderByte(n) {
      switch (this.view) {
        case 'ASCII':
          return String.fromCharCode(n);
        case 'Dezimalzahlen':
          return n.toString() + ' ';
        case 'Bits':
          return byteToBinary(n) + ' ';
      }
    },
    renderFile() {
      let result = '';
      for (let i = 0; i < this.content.byteLength; ++i) {
        result += this.renderByte(this.content.getUint8(i));
      }
      this.output = result;
    },
  },
  template: `
    <div class="box interactive">
      <h4>Dateibetrachter</h4>
      <div class="field">
        <div class="file">
          <label class="file-label">
            <input @change="loadFile" class="file-input" type="file" name="resume">
            <span class="file-cta">
              <span class="file-icon">
                <v-icon name="mdi-upload"/>
              </span>
              <span class="file-label">
                Datei wählen…
              </span>
            </span>
          </label>
        </div>
      </div>
      <div class="field">
        <label class="label">Dateiname</label>
        <input class="input" type="text" v-model="fileName" readonly>
      </div>
      <div class="field">
        <label class="label">Internet Media Type</label>
        <input class="input" type="text" v-model="fileType" readonly>
      </div>
      <div class="field">
        <label class="label">Darstellungsart</label>
        <div class="select">
          <select v-model="view">
            <option>Bits</option>
            <option>Dezimalzahlen</option>
            <option>ASCII</option>
          </select>
        </div>
      </div>
      <div class="field">
        <label class="label">Dateiinhalt</label>
        <div class="control">
          <textarea class="textarea" v-model="output"></textarea>
        </div>
      </div>
    </div>
  `,
};
