export default {
  data() {
    return {
      query: '',
      result: '',
    };
  },
  methods: {
    async lookup() {
      const response = await window.fetch(
        'https://dns.google/resolve?name=' + this.query,
      );
      if (response.ok) {
        const result = await response.json();
        if (result.Status === 0 && result.Answer.length > 0) {
          this.result = result.Answer[0].data;
        } else {
          this.result = 'unbekannte Domain';
        }
      }
    },
  },
  template: `
    <div class="box interactive">
      <h4>DNS-Abfrage</h4>
      <div class="field">
        <label class="label">Domain Name</label>
        <input class="input" v-model="query">
      </div>
      <div class="buttons">
          <button class="button is-primary" @click="lookup">Abfrage</button>
      </div>
      <div class="field">
        <label class="label">Resultat</label>
        <input class="input" readonly v-model="result">
      </div>
    </div>`,
};
