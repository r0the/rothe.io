import { substitutionDecrypt, substitutionEncrypt } from '../common/crypt.js';

export default {
  data() {
    return {
      crypttext: '',
      key: 'WIXRYNQMAPLBHDZEJCSUTGVOKF',
      plaintext: '',
    };
  },
  methods: {
    decrypt() {
      this.plaintext = substitutionDecrypt(this.crypttext, this.key);
    },
    encrypt() {
      this.crypttext = substitutionEncrypt(this.plaintext, this.key);
    },
  },
  template: `
    <div class="box interactive">
      <h4>Substitutionschiffre</h4>
      <div class="field">
        <label class="label">Klartext</label>
          <textarea class="textarea" placeholder="Klartext" v-model="plaintext"></textarea>
      </div>
      <label class="label">Schlüssel</label>
      <VueSelectAlphabet v-model="key"/>
      <div class="buttons">
        <button class="button is-primary" @click="encrypt"><v-icon name="mdi-arrow-down-thick"/> Verschlüsseln</button>
        <button class="button is-primary" @click="decrypt"><v-icon name="mdi-arrow-up-thick"/> Entschlüsseln</button>
      </div>
      <div class="field">
        <label class="label">Geheimtext</label>
        <textarea class="textarea" placeholder="Geheimtext" v-model="crypttext"></textarea>
      </div>
    </div>
  `,
};
