import { loadJson, loadText } from '../common/service.js';
import Canvas from '../common/canvas.js';

export default {
  props: {
    src: { type: String },
  },
  data() {
    return {
      basePath: '',
      challenges: [],
      current: null,
      error: false,
      source: '',
      index: 0,
      code: '',
      points: 0,
    };
  },
  async created() {
    const json = await loadJson(this.src);
    this.basePath = json.basePath;
    this.challenges = json.challenges;
    this.current = this.challenges[0];
    this.load();
  },
  computed: {
    classes() {
      return (
        'box ' +
        (this.error ? 'has-background-red-light' : 'has-background-green-light')
      );
    },
  },
  mounted() {
    this.canvas = new Canvas(this.$refs.canvas);
    this.canvas.setFont('12pt Courier');
  },
  methods: {
    async load() {
      const data = await loadText(this.basePath + this.current.source);
      this.canvas.fill('white');
      this.canvas.drawText(data, 10, 25, 'black');
      this.source = data;
    },
    type(value) {
      this.code += value;
      this.check();
    },
    check() {
      if (this.code.length !== 4) return;
      this.error = this.code !== this.current.solution;
      if (this.error) {
        this.error = true;
        this.code = '';
        return;
      }

      this.points += this.current.points;
      ++this.index;
      this.current = this.challenges[this.index];
      this.load();
      this.code = '';
    },
  },
  template: `
    <div :class="classes">
      <p>Rätsel Nummer {{ index }}</p>
      <p>Total Punkte: {{ points }}</p>
      <div class="columns">
        <div class="column">
          <canvas ref="canvas" width="350" height="400" style="width: 350px; height: 400px;"></canvas>
        </div>
        <div class="column">
          <div class="field">
            <div class="control">
              <input v-model="code" @keyup="check" class="input is-large" type="text" placeholder="CODE">
            </div>
          </div>
          <div class="columns is-multiline is-mobile">
            <div class="column is-4"><button @click="type('7')" class="button is-success is-large">7</button></div>
            <div class="column is-4"><button @click="type('8')" class="button is-success is-large">8</button></div>
            <div class="column is-4"><button @click="type('9')" class="button is-success is-large">9</button></div>
            <div class="column is-4"><button @click="type('4')" class="button is-success is-large">4</button></div>
            <div class="column is-4"><button @click="type('5')" class="button is-success is-large">5</button></div>
            <div class="column is-4"><button @click="type('6')" class="button is-success is-large">6</button></div>
            <div class="column is-4"><button @click="type('1')" class="button is-success is-large">1</button></div>
            <div class="column is-4"><button @click="type('2')" class="button is-success is-large">2</button></div>
            <div class="column is-4"><button @click="type('3')" class="button is-success is-large">3</button></div>
            <div class="column is-4"></div>
            <div class="column is-4"><button @click="type('0')" class="button is-success is-large">0</button></div>
            <div class="column is-4"></div>
          </div>
        </div>
      </div>
    </div>
  `,
};
