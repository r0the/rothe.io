import { loadScript, Singleton } from '../common/service.js';

const LEVEL_MAP = {
  achievement: 0,
  notice: 1,
  warning: 2,
  insecure: 3,
};

const DICTIONARY = {
  instantly: 'sofort',
  forever: 'nie',
};

const PERIOD_DICTIONARY = [
  {
    singular: 'Nanosekunde',
    plural: 'Nanosekunden',
    seconds: 1e-9,
  },
  {
    singular: 'Mikrosekunde',
    plural: 'Mikrosekunden',
    seconds: 1e-6,
  },
  {
    singular: 'Millisekunde',
    plural: 'Millisekunden',
    abbreviations: ['ms', 'msec'],
    seconds: 1e-3,
  },
  {
    singular: 'second',
    plural: 'Sekunden',
    abbreviations: ['s', 'sec', 'secs'],
    seconds: 1,
  },
  {
    singular: 'Minute',
    plural: 'Minuten',
    abbreviations: ['m', 'min', 'mins'],
    seconds: 60,
  },
  {
    singular: 'Stunde',
    plural: 'Stunden',
    abbreviations: ['h'],
    seconds: 3600,
  },
  {
    singular: 'Tag',
    plural: 'Tagen',
    seconds: 86400,
  },
  {
    singular: 'Woche',
    plural: 'Wochen',
    seconds: 604800,
  },
  {
    singular: 'Monat',
    plural: 'Monaten',
    seconds: 2626560,
  },
  {
    singular: 'Jahr',
    plural: 'Jahren',
    seconds: 31557600,
  },
];

const NAMED_NUMBER_DICTIONARY = {
  Millionen: 6,
  Milliarden: 9,
  Billionen: 12,
  Billiarden: 15,
  Trillionen: 18,
  Trilliarden: 21,
  Quadrillionen: 24,
  Quadrilliarden: 27,
  Quintillionen: 30,
  Quintilliarden: 33,
  Sextillionen: 36,
  Sextilliarden: 39,
  Septillionen: 42,
  Septilliarden: 45,
  Oktillionen: 48,
  Oktilliarden: 51,
  Nonillionen: 54,
  Nonilliarden: 57,
  Dezillionen: 60,
  Dezilliarden: 63,
};

const CHECKER_DICTIONARY = {
  common: {
    name: 'Top {{ value }} häufigste Passwörter',
    message:
      'Dein Passwort wird sehr häufig verwendet. Es kann fast augenblicklich geknackt werden.',
  },
  xkcd: {
    name: 'xkcd',
    message: 'https://xkcd.com/936/',
  },
  jeff: {
    name: 'Es gibt immer eine Hintertür',
    message:
      "'The guy who made the software was called Jeff Jeffty Jeff. Born on the first of Jeff, nineteen-jeffty-jeff.'",
  },
  lengthVeryShort: {
    name: 'Sehr kurz',
    message:
      'Dein Passwort ist sehr kurz. Je länger ein Passwort ist, desto sicherer ist es.',
  },
  possiblyWord: {
    name: 'Wort oder Name',
    message:
      'Dein Passwort sieht wie ein Wort aus dem Wörterbuch oder ein Name aus. Ein Name mit persönlichem Bezug könnte einfach erraten werden. Ein Wort aus dem Wörterbuch kann sehr schnell geknackt werden.',
  },
  onlyNumbers: {
    name: 'Nur Zahlen',
    message:
      'Dein Passwort besteht nur aus Zahlen. Füge Buchstaben und Symbole hinzu, um es sicherer zu machen.',
  },
  wordAndNumber: {
    name: 'Wort und Zahl',
    message:
      'Dein Passwort besteht nur aus einem Wort und ein paar Ziffern. Dies ist eine häufige Kombination und kann sehr rasch geknackt werden.',
  },
  lengthShort: {
    name: 'Kurz',
    message:
      'Dein Passwort ist ziemlich kurz. Je länger ein Passwort ist, desto sicherer ist es.',
  },
  justLetters: {
    name: 'Nur Buchstaben',
    message:
      'Dein Passwort besteht nur aus Buchstaben. Füge Zahlen und Symbole hinzu, um es sicherer zu machen.',
  },
  noSymbols: {
    name: 'Keine Symbole',
    message:
      'Dein Passwort enthält nur Zahlen und Buchstaben. Füge ein Symbol hinzu, um es sicherer zu machen.',
  },
  telephone: {
    name: 'Telefonnummer oder Datum',
    message:
      'Dein Passwort könnte eine Telefonnummer oder ein Datum sein. Falls es einen persönlichen Bezug hat, kann es einfach zu erraten sein.',
  },
  repeatedPattern: {
    name: 'Wiederholendes Muster',
    message:
      'Wiederholende Zeichen oder Muster können dein Passwort vorhersagbar machen.',
  },
  nonStandardCharacters: {
    name: 'Spezialzeichen',
    message:
      'Dein Passwort enthält ein Spezialzeichen und ist dadurch sicherer.',
  },
  lengthLong: {
    name: 'Lang',
    message: 'Dein Passwort ist länger als sechzehn Zeichen.',
  },
};

const HSIMP = new Singleton(async function () {
  await loadScript('lib/hsimp/hsimp.min.js');
  const result = window.hsimp;
  result.setDictionary(DICTIONARY);
  result.setPeriodDictionary(PERIOD_DICTIONARY);
  result.setNamedNumberDictionary(NAMED_NUMBER_DICTIONARY);
  result.setCheckerDictionary(CHECKER_DICTIONARY);
  return result;
});

export default {
  data() {
    return {
      checks: [],
      level: 0,
      time: '',
    };
  },
  computed: {
    bgColor() {
      if (!this.time) return '#90CAF9';
      switch (this.level) {
        case 0:
          return '#A5D6A7';
        case 1:
          return '#A5D6A7';
        case 2:
          return '#FFE082';
        default:
          return '#EF9A9A';
      }
    },
  },
  async created() {
    const hsimp = await HSIMP.get();
    const options = {
      outputTime: (time, input) => {
        if (time && time !== 'sofort' && time !== 'nie') time = 'in ' + time;
        this.time = time;
      },
      outputChecks: (checks, input) => {
        let level = 0;
        checks.forEach((check) => {
          level = Math.max(level, LEVEL_MAP[check.level]);
        });
        this.level = level;
        this.checks = checks;
      },
    };
    hsimp(options, this.$refs.password);
  },
  template: `
    <div class="box" :style="{ backgroundColor: bgColor }">
      <h4>Wie sicher ist mein Passwort?</h4>
      <div class="field">
        <label class="label">Password</label>
        <input class="input" type="password" ref="password">
      </div>
      <div class="field">
        <label class="label">Analyse</label>
      </div>
      <p v-if="time">Ein Computer kann dein Passwort <strong>{{ time }}</strong> erraten.</p>
      <ul>
        <li v-for="check in checks"><strong>{{ check.name }}:</strong> {{ check.message }}</li>
      </ul>
    </div>
  `,
};
