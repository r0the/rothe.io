export default {
  props: {
    title: { type: String, default: 'Hash-Funktion' },
    type: { type: String, default: 'simple' },
  },
  data() {
    return {
      text: '',
      original: 0,
    };
  },
  computed: {
    hash() {
      return this.simpleHash(this.text);
    },
  },
  methods: {
    remember() {
      this.original = this.hash;
    },
    simpleHash(text) {
      let result = 0;
      for (let i = 0; i < text.length; ++i) {
        result += text.charCodeAt(i);
        result %= 256;
      }
      return result;
    },
  },
  template: `
    <div class="box interactive">
      <h4>{{ title }}</h4>
      <div class="field">
        <label class="label">Text</label>
        <div class="control">
          <textarea class="textarea" placeholder="Text" v-model="text"></textarea>
        </div>
      </div>
      <div class="field">
        <label class="label">Hashwert</label>
        <div class="control">
          <input class="input" type="text" readonly v-model="hash">
        </div>
      </div>
      <label class="label">Originaler Hashwert</label>
      <div class="field has-addons">
        <div class="control is-expanded">
          <input class="input" type="text" readonly v-model="original">
        </div>
        <div class="control">
          <button class="button" @click="remember">Hashwert merken</button>
        </div>
      </div>
    </div>
  `,
};
