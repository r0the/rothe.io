/* global Vue, VueRouter */

// core components
import App from './generated/app.js';
import Box from './core/box.js';
import Code from './generated/code.js';
import Collapsible from './generated/collapsible.js';
import Exercise from './generated/exercise.js';
import Icon from './generated/icon.js';
import ImageDialog from './core/image-dialog.js';
import ImageTag from './generated/image-tag.js';
import Link from './core/link.js';
import Math from './generated/math.js';
import Scratch from './generated/scratch.js';

// additional components
import Book from './generated/book.js';
import Cam from './components/cam.js';
import CardLink from './components/card-link.js';
import Circuit from './components/circuit.js';
import ColourTable from './components/colour-table.js';
import Grid from './components/grid.js';
import Input from './components/input.js';
import LetterPicker from './components/letter-picker.js';
import Neuron from './components/neuron.js';
import NumberPicker from './components/number-picker.js';
import RemoteIp from './components/remote-ip.js';
import SelectAlphabet from './components/select-alphabet.js';
import SemesterTable from './components/semester-table.js';
import Tab from './generated/tab.js';
import Tabs from './generated/tabs.js';
import Video from './generated/video.js';

import ColorPicker from './interactives/color-picker.js';

// interactives
import BinaryCards from './interactives/binary-cards.js';
import BinaryChallenge from './interactives/binary-challenge.js';
import BinaryConverter from './interactives/binary-converter.js';
import Caesar from './interactives/caesar.js';
import ColourPicker from './interactives/colour-picker.js';
import ColourDiffieHellman from './interactives/colour-diffie-hellman.js';
import DnsLookup from './interactives/dns-lookup.js';
import Factorise from './interactives/factorise.js';
import FileViewer from './interactives/file-viewer.js';
import FrequencyAnalysis from './interactives/frequency-analysis.js';
import GameOfLife from './interactives/game-of-life.js';
import Hash from './interactives/hash.js';
import PasswordCheck from './interactives/password-check.js';
import Pentacode from './interactives/pentacode.js';
import Polybios from './interactives/polybios.js';
import Redundancy from './interactives/redundancy.js';
import Qrcode from './interactives/qrcode.js';
import Skytale from './interactives/skytale.js';
import Substitution from './interactives/substitution.js';
import XorBlockCipher from './interactives/xor-block-cipher.js';

import VueCodeBreaker from './interactives/code-breaker.js';
import VuePixelEditor from './interactives/pixel-editor.js';

const app = Vue.createApp({
  el: '#app',
  config: {
    compilerOptions: {
      whitespace: 'preserve',
    },
  },
  components: {
    App,
    ImageDialog,
  },
  template: `
    <div class="app">
      <router-view></router-view>
      <ImageDialog ref="dialog"></ImageDialog>
    </div>
  `,
});

// core components
app.component('v-box', Box);
app.component('v-cam', Cam);
app.component('v-code', Code);
app.component('v-collapsible', Collapsible);
app.component('v-exercise', Exercise);
app.component('v-icon', Icon);
app.component('v-image-tag', ImageTag);
app.component('v-link', Link);
app.component('v-math', Math);
app.component('v-scratch', Scratch);

// additional components
app.component('v-book', Book);
app.component('v-card-link', CardLink);
app.component('v-circuit', Circuit);
app.component('v-input', Input);
app.component('v-letter-picker', LetterPicker);
app.component('v-number-picker', NumberPicker);
app.component('v-remote-ip', RemoteIp);
app.component('v-semester-table', SemesterTable);
app.component('v-tab', Tab);
app.component('v-tabs', Tabs);
app.component('v-video', Video);

// old names
app.component('VueSemesterTable', SemesterTable);
app.component('VueVideo', Video);

app.component('VueColourTable', ColourTable);
app.component('VueGrid', Grid);
app.component('VueNeuron', Neuron);
app.component('VueSelectAlphabet', SelectAlphabet);
app.component('VueColorPicker', ColorPicker);

// interactives
app.component('v-binary-cards', BinaryCards);
app.component('v-binary-challenge', BinaryChallenge);
app.component('v-binary-converter', BinaryConverter);
app.component('v-caesar', Caesar);
app.component('v-colour-diffie-hellman', ColourDiffieHellman);
app.component('v-colour-picker', ColourPicker);
app.component('v-dns-lookup', DnsLookup);
app.component('v-factorise', Factorise);
app.component('v-file-viewer', FileViewer);
app.component('v-frequency-analysis', FrequencyAnalysis);
app.component('v-game-of-life', GameOfLife);
app.component('v-hash', Hash);
app.component('v-password-check', PasswordCheck);
app.component('v-pentacode', Pentacode);
app.component('v-polybios', Polybios);
app.component('v-qrcode', Qrcode);
app.component('v-redundancy', Redundancy);
app.component('v-skytale', Skytale);
app.component('v-substitution', Substitution);
app.component('v-xor-block-cipher', XorBlockCipher);

app.component('VueCodeBreaker', VueCodeBreaker);
app.component('VuePixelEditor', VuePixelEditor);

const router = VueRouter.createRouter({
  routes: [{ path: '/:pathMatch(.*)*', component: App }],
  history: VueRouter.createWebHistory(),
  base: window.location.pathname,
});
app.use(router);
app.mount('#app');
