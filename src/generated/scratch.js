import { loadScript, Singleton } from '../common/service.js';

const OPTIONS = {
  languages: ['de', 'en'],
  style: 'scratch3',
};

const SCRATCH = new Singleton(async function () {
  await loadScript('lib/scratchblocks/scratchblocks.min.js');
  await loadScript('lib/scratchblocks/translation-de.js');
  return window.scratchblocks;
});

export default {
  template: `
  <div ref="code">
    <pre><slot></slot></pre>
  </div>
  `,
  async mounted() {
    const scratch = await SCRATCH.get();
    const el = this.$refs.code;
    const code = scratch.read(el, OPTIONS);
    const doc = scratch.parse(code, OPTIONS);
    const svg = scratch.render(doc, OPTIONS);
    scratch.replace(el, svg, doc, OPTIONS);
  },
};
