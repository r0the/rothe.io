import { loadText, Storage } from '../common/service.js';
import MarkdownRenderer from '../core/markdown-renderer.js';
import { page } from '../core/navigation.js';

const RuntimeTemplate = {
  props: {
    template: { type: String, default: null },
  },
  render() {
    if (!this.template) return Vue.h('div');
    return Vue.h(Vue.compile(this.template, { whitespace: 'preserve' }));
  },
};

export default {
  template: `
  <div>
    <div class="page-menu">
      <div v-if="permalink" class="dropdown is-right is-hoverable">
        <div class="dropdown-trigger">
          <a arial-haspopup="true" arial-controls="dropdown-menu"><v-icon name="mdi-share"/></a>
        </div>
        <div class="dropdown-menu" id="dropdown-menu">
          <div class="dropdown-content">
            <div class="dropdown-item" style="min-width: 320px">
              <div class="field">
                <label class="label">Permalink</label>
                <input id="permalink" class="input" readonly :value="permalink">
              </div>
              <button class="button" @click="copyPermalink">Kopieren</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <RuntimeTemplate :template="content"></RuntimeTemplate>
    <div v-if="pageNav" class="page-navigation">
      <v-link v-if="prevText!=null" :to="prevLink" class="is-pulled-left" ><v-icon name="mdi-arrow-left"/> {{ prevText }}</v-link>
      <v-link v-if="nextText!=null" :to="nextLink" class="is-pulled-right">{{ nextText }} <v-icon name="mdi-arrow-right"/></v-link>
    </div>
  </div>
  `,
  props: {
    page: { type: String, default: null },
    pageEntry: { type: Object, default: null },
    permalink: { type: String, default: null },
  },
  emits: ['loaded'],
  components: {
    RuntimeTemplate,
  },
  data() {
    return {
      base: null,
      content: null,
      loading: false,
      markdown: null,
      nextLink: null,
      nextText: null,
      pageNav: false,
      prevLink: null,
      prevText: null,
    };
  },
  watch: {
    page(newPage) {
      this.loadPage(newPage);
    },
    pageEntry(newPageEntry) {
      this.updateLinks(newPageEntry);
    },
  },
  mounted() {
    this.renderer = MarkdownRenderer();
    this.loadPage(this.page);
  },
  methods: {
    copyPermalink() {
      const element = document.getElementById('permalink');
      element.select();
      element.setSelectionRange(0, 99999);
      document.execCommand('copy');
    },
    updateLinks() {
      this.nextLink = null;
      this.nextText = null;
      this.prevLink = null;
      this.prevText = null;
      this.pageNav = false;
      if (!this.pageEntry) return;
      if (this.pageEntry.next) {
        this.nextLink = '?page=' + this.pageEntry.next.page;
        this.nextText = this.pageEntry.next.title;
      }
      if (this.pageEntry.prev) {
        this.prevLink = '?page=' + this.pageEntry.prev.page;
        this.prevText = this.pageEntry.prev.title;
      }
      this.pageNav = this.prevText != null || this.nextText != null;
    },
    async loadPage(page) {
      if (!page || this.loading) return;
      this.loading = true;
      try {
        let data;
        if (page.endsWith('/')) {
          try {
            data = await loadText(page + 'index.md');
          } catch (e) {
            data = await loadText(page + 'README.md');
          }
        } else {
          data = await loadText(page + '.md');
        }
        window.getSelection().removeAllRanges();
        window.scrollTo(0, 0);
        this.markdown = data;
        if (this.pageEntry) {
          const lock = this.pageEntry.lock;
          this.showSolutions = !lock || lock === Storage.load('key');
        } else {
          this.showSolutions = true;
        }
        this.$emit('loaded');
      } catch (e) {
        this.markdown = '';
        this.$emit('loaded', e);
      }
      this.loading = false;
      this.content = this.renderHTML(this.markdown);
    },
    renderHTML(markdown) {
      const html = this.renderer.render(markdown, {
        hiddenId: 0,
        showSolutions: this.showSolutions,
        normalizeURI: page.normalizeURI,
      });
      return '<div class="content" id="content">' + html + '</div>';
    },
  },
};
