export default {
  template: `
  <div>
    <h4 @click="toggle"><v-icon :name="iconName" /> {{ title }}</h4>
    <slot v-if="visible"></slot>
  </div>
  `,
  props: {
    title: { type: String, default: null },
  },
  data() {
    return {
      visible: false,
    };
  },
  methods: {
    toggle() {
      this.visible = !this.visible;
    },
  },
  computed: {
    iconName() {
      return `mdi-chevron-${this.visible ? 'down' : 'right'}`;
    },
  },
};
