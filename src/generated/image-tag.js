import { loadImageInfo } from '../common/service.js';

export default {
  template: `
  <p v-if="visible">
    <span @click="showInfo" class="tag vue-image-tag">
      Bild: {{ info.author_name || 'unbekannt' }}
    </span>
  </p>
  `,
  props: {
    src: { type: String, default: null },
  },
  data() {
    return {
      info: null,
      visible: false,
    };
  },
  watch: {
    src(newValue) {
      this.loadInfo(newValue);
    },
  },
  created() {
    this.loadInfo(this.src);
  },
  methods: {
    async loadInfo(src) {
      const json = await loadImageInfo(src);
      this.info = json;
      this.visible = json;
    },
    showInfo() {
      this.$root.$refs.dialog.src = this.src;
      this.$root.$refs.dialog.info = this.info;
      this.$root.$refs.dialog.visible = true;
    },
  },
};
