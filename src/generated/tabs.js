export default {
  template: `
  <div>
    <div class="tabs is-boxed">
      <ul>
        <li v-for="(tab, i) in tabs" :key="i" :class="tabClasses(i)">
          <a  @click="selectedTab = i">{{ tab }}</a>
        </li>
      </ul>
    </div>
    <section>
      <slot/>
    </section>
  </div>
  `,
  data() {
    return {
      selectedTab: 0,
      tabs: [],
    };
  },
  provide() {
    return {
      tabs: this.tabs,
      selectedTab: Vue.computed(() => this.selectedTab),
    };
  },
  methods: {
    tabClasses(index) {
      return this.selectedTab === index ? 'is-active' : '';
    },
  },
};
