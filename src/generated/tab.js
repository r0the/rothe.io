export default {
  template: `
  <section v-show="active">
    <slot></slot>
  </section>
  `,
  props: {
    label: { type: String },
  },
  inject: ['tabs', 'selectedTab'],
  created() {
    this.tabs.push(this.label);
  },
  computed: {
    active() {
      return this.tabs[this.selectedTab.value] === this.label;
    },
  },
};
