export default {
  template: `
  <div :class="['message', type]">
    <div class="message-header">
      <p>{{ title }}</p>
    </div>
    <div class="message-body">
      <slot></slot>
    </div>
  </div>
  `,
  props: {
    type: { type: String },
    title: { type: String },
  },
};
