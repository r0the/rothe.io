const VueSlider = {
  props: {
    classes: { type: String, default: '' },
    max: { type: Number, default: 100 },
    min: { type: Number, default: 0 },
    step: { type: Number, default: 1 },
    value: { type: Number, default: 50 },
  },
  data() {
    return { value: 50 };
  },
  methods: {
    valueChanged(event) {
      this.$emit('input', event.target.value);
    },
  },
  template: `<div class="control">
    <input :value="value" @input="valueChanged" class="slider" :class="classes" v-bind:max="max" :min="min" :step="step" type="range">
    <output>{{ value }}</output>
    </div>`,
};

export default {
  data() {
    return {
      x1: 0.5,
      x2: 0.5,
      x3: 0.5,
      w1: 0,
      w2: 0,
      w3: 0,
    };
  },
  components: {
    VueSlider,
  },
  computed: {
    output() {
      const value = this.x1 * this.w1 + this.x2 * this.w2 + this.x3 * this.w3;
      const output = 1 / (1 + Math.pow(Math.E, -value));
      return Math.round(output * 100) / 100;
    },
  },
  template: `<div>
    <div class="columns is-hidden-mobile">
      <div class="column is-4">Eingabe</div>
      <div class="column is-4">Gewichte</div>
      <div class="column is-4">Ausgabe</div>
    </div>
    <div class="columns is-vcentered">
      <div class="column is-4">
        <span class="is-hidden-tablet">Eingabe</span>
        <VueSlider v-model="x1" max="1" step="0.01" has-output="true"></VueSlider>
        <VueSlider v-model="x2" max="1" step="0.01" has-output="true"></VueSlider>
        <VueSlider v-model="x3" max="1" step="0.01" has-output="true"></VueSlider>
      </div>
      <div class="column is-4">
      <span class="is-hidden-tablet">Gewichte</span>
        <VueSlider v-model="w1" min="-2" max="2" step="0.01" has-output="true"></VueSlider>
        <VueSlider v-model="w2" min="-2" max="2" step="0.01" has-output="true"></VueSlider>
        <VueSlider v-model="w3" min="-2" max="2" step="0.01" has-output="true"></VueSlider>
      </div>
      <div class="column is-4">
      <span class="is-hidden-tablet">Ausgabe</span>
        <span class="tag is-dark" style="width: 4rem;">{{ output }}</span>
      </div>
    </div>
  </div>`,
};
