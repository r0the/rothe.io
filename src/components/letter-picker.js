import { rotChar } from '../common/crypt.js';

export default {
  props: {
    modelValue: { type: String, default: 'A' },
  },
  emits: ['update:modelValue'],
  data() {
    return { value: this.modelValue };
  },
  methods: {
    next() {
      this.value = rotChar(this.value, 1);
      this.$emit('update:modelValue', this.value);
    },
    prev() {
      this.value = rotChar(this.value, -1);
      this.$emit('update:modelValue', this.value);
    },
  },
  template: `
    <div class="field has-addons">
      <div class="control">
        <button class="button is-primary" @click="prev"><v-icon name="mdi-chevron-left"/></button>
      </div>
      <div class="control">
        <input class="input" type="text" readonly v-model="value">
      </div>
      <div class="control">
        <button class="button is-primary" @click="next"><v-icon name="mdi-chevron-right"/></button>
      </div>
    </div>
  `,
};
