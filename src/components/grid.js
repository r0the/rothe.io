export default {
  props: {
    cellSize: { type: Number, default: 24 },
    data: { type: Array, default: [] },
  },
  emits: ['click'],
  methods: {
    onclick(x, y, cell) {
      this.$emit('click', x, y, cell);
    },
    style(cell) {
      const size = `${this.cellSize}px`;
      const style = {
        width: size,
        height: size,
      };
      if (cell.colour) style.backgroundColor = cell.colour;
      return style;
    },
  },
  template: `
    <table class="vue-grid">
      <tr v-for="(row, y) in data">
        <td v-for="(cell, x) in row" :style="style(cell)" @click="onclick(x, y, cell)"></td>
      </tr>
    </table>
  `,
};
