export default {
  props: {
    alphabet: { type: String, default: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' },
    modelValue: { type: String, default: '' },
  },
  emits: ['update:modelValue'],
  data() {
    return {
      remaining: this.alphabet,
      value: this.modelValue,
    };
  },
  computed: {
    message() {
      return this.remaining.length > 0 ? `fehlend: ${this.remaining}` : '';
    },
  },
  watch: {
    value(newValue) {
      let chosen = '';
      let remaining = this.alphabet;
      newValue = newValue.toUpperCase();
      // retain only unique alphabet characters
      for (let i = 0; i < newValue.length; ++i) {
        const ch = newValue[i];
        const indexA = this.alphabet.indexOf(ch);
        const indexC = chosen.indexOf(ch);
        if (indexA !== -1 && indexC === -1) {
          chosen += ch;
          remaining = remaining.replace(ch, '');
        }
      }
      this.value = chosen;
      this.remaining = remaining;
      this.$emit('update:modelValue', this.value);
    },
  },
  methods: {
    randomFill() {
      // only generate a completely random alphabet when alphabet is already completely filled
      // otherwise use existing characters in chosen alphabet
      if (this.remaining.length === 0) {
        this.remaining = this.alphabet;
        this.value = '';
      }
      for (let i = this.remaining.length; i > 0; --i) {
        const ran = Math.floor(Math.random() * Math.floor(i));
        const ch = this.remaining.charAt(ran);
        this.remaining = this.remaining.replace(ch, '');
        this.value += ch;
      }
    },
  },
  template: `
    <div class="field">
      <div class="field has-addons">
        <div class="control">
          <input class="input" type="text" size="26" v-model="value">
        </div>
        <div class="control">
          <button class="button is-primary" @click="randomFill"><v-icon name="mdi-dice-5"/> zufällig</button>
        </div>
      </div>
      <p class="help is-danger">{{ message }}</p>
    </div>
  `,
};
