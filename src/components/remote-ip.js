import { loadJson } from '../common/service.js';

export default {
  data() {
    return {
      address: null,
    };
  },
  async created() {
    const json = await loadJson('ip.php');
    this.address = json.clientIpAddress;
  },
  template: '<span v-if="address">{{ address }}</span>',
};
