import { loadJson } from '../common/service.js';

export default {
  props: {
    name: { type: String, default: 'css-colours' },
  },
  data() {
    return {
      rows: [],
    };
  },
  async created() {
    this.rows = await loadJson(`data/${this.name}.json`);
  },
  methods: {
    styleFor(row) {
      return {
        background: row.css_name,
        width: '70%',
      };
    },
  },
  template: `
    <table>
      <template v-for="row in rows">
        <tr v-if="row.tk_name">
          <td><code>"{{ row.tk_name }}"</code></td>
          <td :style="styleFor(row)"></td>
        </tr>
      </template>
    </table>
  `,
};
