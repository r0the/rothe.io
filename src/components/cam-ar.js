/* global AR */
import Canvas from '../common/canvas.js';

const MD = navigator.mediaDevices;

export default {
  props: {
    height: { type: Number, default: 480 },
    width: { type: Number, default: 640 }
  },
  data () {
    return {
      devices: [],
      mode: 'orig',
      playing: false,
      mirror: false,
      selectedDevice: 0
    };
  },
  computed: {
    mirrorButton () { return this.mirror ? 'mirror-off' : 'mirror-on'; },
    playButton () { return this.playing ? 'pause' : 'play'; }
  },
  created () {
    if (MD == null) return;
    MD.enumerateDevices().then(devs => {
      devs.forEach(dev => {
        console.log(dev);
        if (dev.kind === 'videoinput') {
          this.devices.push({
            deviceId: dev.deviceId,
            label: dev.label || 'Kamera'
          });
        }
      });
    });
  },
  mounted () {
    this.canvas = new Canvas(this.$refs.canvas);
    this.canvas.setSize(this.width, this.height);
    this.detector = new AR.Detector();
    this.captureFrame = true;
    this.count = 0;
    this.time = 0;
  },
  methods: {
    tick () {
      const video = this.$refs.video;
      if (video.readyState === video.HAVE_ENOUGH_DATA) {
        if (!this.capture) {
          this.capture = new Canvas(video.videoWidth, video.videoHeight);
          this.output = new Canvas(video.videoWidth, video.videoHeight);
        }
        if (this.captureFrame) {
          this.capture.drawImage(video);
        } else {
          const imageData = this.capture.getImageData();
          const markers = this.detector.detect(imageData);
          if (markers.length > 0) this.$emit('input', markers);
          switch (this.mode) {
            case 'orig':
              this.output.drawImage(this.capture, 0, 0);
              break;
            case 'grey':
              this.output.drawGreyscale(this.detector.grey);
              break;
            case 'thres':
              this.output.drawGreyscale(this.detector.thres);
              break;
          }
          this.drawMarkers(markers);
          this.canvas.drawImage(this.output);
        }
        this.captureFrame = !this.captureFrame;
      }
      if (this.playing) window.requestAnimationFrame(this.tick);
    },
    drawCandidates (candidates) {
      this.output.beginPath();
      candidates.forEach(candidate => {
        this.output.drawPolygon(candidate);
      });
      this.output.stroke('green', 3);
    },
    drawMarkers (markers) {
      this.output.beginPath();
      markers.forEach(marker => {
        this.output.drawPolygon(marker.corners);
        this.output.moveTo(marker.north[0], marker.north[1]);
        this.output.lineTo(marker.north[2], marker.north[3]);
      });
      this.output.stroke('red', 3);
      markers.forEach(marker => {
        let x = 2 * marker.north[2] - marker.north[0];
        let y = 2 * marker.north[3] - marker.north[1];
        this.output.drawText(marker.id, x, y, 'red');
      });
    },
    toggleMirror () {
      this.mirror = !this.mirror;
      this.canvas.mirror();
    },
    togglePlay () {
      const video = this.$refs.video;
      if (this.playing) {
        video.srcObject.getTracks().forEach(track => { track.stop(); });
        this.capture = null;
      } else {
        let mediaConfig = { video: true };
        if (this.devices && this.devices.length > 0) {
          const id = this.devices[this.selectedDevice].deviceId;
          mediaConfig = { video: { deviceId: { exact: id } } };
        }
        MD.getUserMedia(mediaConfig).then(stream => {
          video.srcObject = stream;
          video.play();
          window.requestAnimationFrame(this.tick);
        }).catch(error => {
          console.err('Error while starting video');
          console.err(error);
        });
      }
      this.playing = !this.playing;
    }
  },
  template: `
    <div>
      <div>
        <video v-show="false" :width="width" :height="height" ref="video"></video>
        <canvas ref="canvas" :width="width" :height="height"></canvas>
      </div>
      <div class="field is-grouped">
        <div class="control"><div class="select"><select v-model="selectedDevice">
          <option v-for="(device, index) in devices" :value="index">{{ device.label }}</option>
        </select></div></div>
        <div class="control"><button class="button" @click="togglePlay"><v-icon :name="playButton"/></button></div>
        <div class="control"><div class="select"><select v-model="mode">
          <option value="orig">Original</option>
          <option value="grey">Graustufen</option>
          <option value="thres">Kanten</option>
        </select></div></div>
        <div class="control"><button class="button" @click="toggleMirror"><v-icon :name="mirrorButton"/></button></div>
      </div>
    </div>
  `
};
