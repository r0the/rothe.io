export default {
  props: {
    height: { type: Number, default: 360 },
    id: { type: String, default: null },
  },
  computed: {
    src() {
      return `https://circuitverse.org/simulator/embed/${this.id}`;
    },
  },
  template: `
    <figure class="circuit">
      <iframe width="640" :height="height" :src="src" frameborder="0" allow="" scrolling="no" allowfullscreen></iframe>
    </figure>
  `,
};
