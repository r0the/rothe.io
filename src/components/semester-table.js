import Message from '../generated/message.js';
import { page } from '../core/navigation.js';
import { loadJson } from '../common/service.js';

const DATE_FORMAT = { weekday: 'short', day: '2-digit', month: '2-digit' };
const HOLIDAY_ICONS = '🌷🌱🌻🍁🌲☀️🧳⚽';
const EXAM_ICONS = '❗';

function hasIcon(theme, icons) {
  return (
    typeof theme === 'string' &&
    theme.length > 0 &&
    icons.indexOf(theme.charAt(0)) !== -1
  );
}

export default {
  components: {
    Message,
  },
  props: {
    src: { type: String },
  },
  data() {
    return {
      error: null,
      rows: [],
      who: false,
    };
  },
  async created() {
    try {
      this.setData(await loadJson(page.normalizeURI(this.src)));
    } catch (error) {
      this.error = error;
    }
  },
  methods: {
    setData(json) {
      const rows = [];
      const date = new Date(json.start);
      const now = new Date().getTime();
      this.who = false;
      for (let i = 0; i < json.weeks.length; ++i) {
        const week = json.weeks[i];
        const diffDays = (date.getTime() - now) / (1000 * 3600 * 24);
        const current = diffDays > -1 && diffDays < 6;
        let classes = '';
        if (current) classes += ' has-background-blue-light';
        if (hasIcon(week.content, EXAM_ICONS))
          classes += ' has-background-red-light';
        if (hasIcon(week.theme, HOLIDAY_ICONS))
          classes += ' has-background-grey-light';
        if (week.who) this.who = true;
        rows.push({
          classes: classes,
          icon: week.icon || null,
          date: date.toLocaleDateString('de-CH', DATE_FORMAT),
          theme: week.theme,
          content: week.content,
          who: week.who,
          link: week.link,
          exam: typeof week.exam !== 'undefined',
          holiday: typeof week.holiday !== 'undefined',
        });
        date.setDate(date.getDate() + 7);
      }
      this.rows = rows;
    },
  },
  template: `
    <div>
      <Message v-if="error" title="Fehler beim Laden der JSON-Datei" type="is-danger">
        {{ error }}
      </Message>
      <table v-if="!error" class="table is-narrow">
        <thead>
          <tr>
            <th>Datum</th>
            <th>Thema</th>
            <th>Inhalt</th>
            <th v-if="who">Wer</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="row in rows" :class="row.classes">
              <td>{{ row.date }}</td>
              <td>{{ row.theme }}</td>
              <td v-if="row.link"><v-link :to="row.link">{{ row.content }}</v-link></td>
              <td v-if="!row.link">{{ row.content }}</td>
              <td v-if="who">{{ row.who }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  `,
};
