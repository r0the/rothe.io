const STATUS_TYPE_ICON = {
  'is-success': 'mdi mdi-check',
  'is-danger': 'mdi mdi-alert-circle',
  'is-info': 'mdi mdi-information',
  'is-warning': 'mdi mdi-alert',
};
const STATUS_TYPE_TEXT = {
  'is-success': 'has-text-success',
  'is-danger': 'has-text-danger',
  'is-info': 'has-text-info',
  'is-warning': 'has-text-warning',
};

export default {
  props: {
    type: { type: String, default: 'text' },
    radix: { type: Number, default: 10 },
    modelValue: [Number, String],
  },
  emits: ['update:modelValue'],
  data() {
    return {
      value: this.modelValue,
      statusType: '',
    };
  },
  watch: {
    modelValue() {
      this.value = this.modelValue;
      this.statusType = null;
    },
  },
  computed: {
    displayValue: {
      get() {
        switch (this.type) {
          case 'text':
            return this.displayText(this.value);
          case 'integer':
            return this.displayInteger(this.value);
        }
      },
      set(value) {
        this.statusType = null;
        this.value = value;
        switch (this.type) {
          case 'text':
            this.parseText(value);
            break;
          case 'integer':
            this.parseInteger(value);
            break;
        }
      },
    },
    statusTypeIcon() {
      return STATUS_TYPE_ICON[this.statusType];
    },
    statusTypeText() {
      return STATUS_TYPE_TEXT[this.statusType];
    },
  },
  methods: {
    displayInteger(value) {
      if (typeof value !== 'number' || isNaN(value)) return '';
      return value.toString(this.radix);
    },
    displayText(value) {
      return value;
    },
    parseInteger(value) {
      value = value.toUpperCase();
      const n = parseInt(value, this.radix);
      if (isNaN(n)) {
        this.statusType = 'is-danger';
      }
      this.value = n;
      this.$emit('update:modelValue', n);
    },
    parseText(value) {
      this.$emit(value);
    },
  },
  template: `
    <div class="control has-icons-right">
      <input :class="['input', statusType]" v-model="displayValue">
      <span :class="['icon', 'is-right', statusTypeText]"><i :class="statusTypeIcon"></i></span>
    </div>
  `,
};
