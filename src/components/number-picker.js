export default {
  props: {
    max: { type: Number, default: 255 },
    min: { type: Number, default: 1 },
    modelValue: { type: Number, default: 1 },
  },
  emits: ['update:modelValue'],
  data() {
    return { value: this.modelValue };
  },
  methods: {
    next() {
      if (this.value < this.max) ++this.value;
      this.$emit('update:modelValue', this.value);
    },
    prev() {
      if (this.value > this.min) --this.value;
      this.$emit('update:modelValue', this.value);
    },
  },
  template: `
    <div class="field has-addons">
      <div class="control">
        <button class="button is-primary" @click="prev"><v-icon name="mdi-chevron-left"/></button>
      </div>
      <div class="control">
        <input class="input" type="text" readonly v-model="value">
      </div>
      <div class="control">
        <button class="button is-primary" @click="next"><v-icon name="mdi-chevron-right"/></button>
      </div>
    </div>
  `,
};
