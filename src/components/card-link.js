export default {
  props: {
    cover: { type: String, default: null },
    icon: { type: String, default: null },
    subtitle: { type: String, default: null },
    to: { type: String, default: null },
    title: { type: String, default: null },
  },
  template: `
    <div>
      <div v-if="icon" class="card-content">
        <div class="media">
          <div class="media-left">
            <figure class="image is-64x64">
              <v-link :to="to"><v-icon fit :name="icon" color="#455A64"/></v-link>
            </figure>
          </div>
          <div class="media-content">
            <p class="title is-4"><v-link :to="to">{{ title }}</v-link></p>
            <p v-if="subtitle" class="subtitle is-6 has-text-grey">{{ subtitle }}</p>
          </div>
        </div>
      </div>
      <div v-else>
        <v-link class="card-image" :to="to"><img :src="cover"></v-link>
        <div class="card-content">
          <p class="title is-5"><v-link :to="to"><v-icon :name="icon"/>{{ title }}</v-link></p>
          <p v-if="subtitle" class="subtitle has-text-grey">{{ subtitle }}</p>
          <v-image-tag :src="cover"/>
        </div>
      </div>
    </div>
  `,
};
