/* global Vue */
/* eslint no-new: 0 */
import VueCam from './components/VueCam.js';
import Icon from './components/Icon.js';

Vue.component('v-cam', VueCam);
Vue.component('v-icon', Icon);

Vue.component('v-votes', {
  props: {
    votes: { type: Array, default: [] },
  },
  template: `<div><ul>
      <li v-for="vote in votes">{{ vote.id }}: {{ vote.vote }}</li>
    </ul></div>`,
});

new Vue({
  el: '#app',
  data: {
    votes: [],
    top: 'TOP',
    left: 'LEFT',
    right: 'RIGHT',
    bottom: 'BOTTOM',
  },
  created: function () {
    this.map = {};
  },
  methods: {
    angleToVote: function (angle) {
      if (angle < 40 && angle > -40) return this.left;
      if (angle > 50 && angle < 130) return this.top;
      if (angle > 140 || angle < -140) return this.right;
      if (angle < -50 || angle > -130) return this.bottom;
      return null;
    },
    reset: function () {
      this.votes = [];
      this.map = {};
    },
    detection: function (markers) {
      markers.forEach((marker) => {
        const id = marker.id;
        const vote = this.angleToVote(marker.angle);
        if (vote) this.map[id] = vote;
      });
      this.votes = [];
      for (const id in this.map) {
        this.votes.push({ id: id, vote: this.map[id] });
      }
    },
  },
});
