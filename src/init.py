import browser
import sys

add_result = None


def equals(get, expect):
    if type(expect) == float or type(get) == float:
        return abs(get - expect) < 1e-6
    return get == expect


def valuestr(value):
    if type(value) == str:
        return "\"" + value.replace("\\", "\\\\").replace("\"", "\\\"").replace("\n", "\\n") + "\""
    else:
        return str(value)


def check_value(vars, name, value):
    if not name in vars:
        add_result(False, "Variable '" + name + "' ist nicht vorhanden.")
    elif name in vars and equals(vars[name], value):
        add_result(True, "Variable '" + name + "' hat den Wert " + valuestr(value) + ".")
    else:
        add_result(False, "Variable '" + name + "' hat einen falschen Wert.")

def execute(code, add_result_js):
    global add_result
    add_result = add_result_js

    result = None
    env = {
        "check_value": check_value
    }
    try:
        exec(code, env)
        add_result(True, "Korrekter Python-Code")
    except NameError as ex:
        name = str(ex).split("'")[1]
        add_result(False, "Python: Die Variable '" + name + "' wird verwendet, ist aber nicht definiert.")
    except SyntaxError as ex:
        add_result(False, "Python: Syntaxfehler.")
    except Exception as ex:
        add_result(False, "Fehler: " + str(ex))

browser.window.execPython = execute
