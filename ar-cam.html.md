<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="config/favicon.ico">
    <link rel="icon" type="image/svg+xml" href="config/favicon.svg" sizes="any">
    <link rel="stylesheet" href="lib/material-design-icons/css/mdi.css">
    <link rel="stylesheet" href="style/main.css">
    <link rel="stylesheet" href="config/main.css">
    <style>
      html, body {
        height: 100%;
        overflow: hidden;
      }
      .top {
        position: absolute;
        top: 0;
        width: 100%;
        height: 5%;
      }
      .bottom {
        position: absolute;
        bottom: 0;
        width: 100%;
      }
      .left {
        display: flex;
        align-items: center;
        position: absolute;
        left: 0;
        height: 100%;
        width: 30%;
      }
      .right {
        text-align: right;
        display: flex;
        align-items: center;
        position: absolute;
        right: 0;
        height: 100%;
        width: 30%;
      }
      .answer-h {
        background-color: rgb(100%, 80%, 80%);
        width: 30%;
        padding: 1rem;
        font-size: 20pt;
        text-align: center;
        margin-left: auto;
        margin-right: auto;
      }
      .answer-v {
        width: 100%;
        padding: 1rem;
        background-color: rgb(100%, 80%, 80%);
        font-size: 20pt;
        margin-top: auto;
        margin-bottom: auto;
      }
    </style>
    <title>AR-Cam</title>
  </head>
  <body>
    <div id="app">
      <div class="top"><div class="answer-h">{{ top }}</div></div>
      <div class="bottom"><div class="answer-h">{{ bottom }}</div></div>
      <div class="left"><div class="answer-v">{{ left }}</div></div>
      <div class="right"><div class="answer-v">{{ right }}</div></div>
      <div class="content">
        <noscript>
          <h1>JavaScript</h1>
          <p>
            Diese Webseite benötigt JavaScript. Bitte verwende einen modernen Browser oder aktiviere JavaScript.
          </p>
          <p>
            JavaScript wird ausschliesslich für die Funktionalität der Webseite verwendet. Es wird kein
            JavaScript-Code von anderen Quellen verwendet, es findet kein Tracking statt.
          </p>
        </noscript>
        <v-cam width="320" height="240" @input="detection($event)"></v-cam>
        <v-votes :votes="votes"></v-votes>
        <button class="button" @click="reset">Reset</button>
      </div>
    </div>
    <script src="src/browsercheck.js"></script>
    <script src="lib/aruco/aruco.min.js"></script>
    <script src="lib/vue/vue.min.js"></script>
    <script src="src/cam.js" type="module"></script>
  </body>
</html>
