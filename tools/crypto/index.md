# Kryptologie

- [Skytale](?page=skytale/)
- [Polybios](?page=polybios/)
- [Caesar-Chiffre](?page=caesar/)
- [Substitutions-Chiffre](?page=substitution/)
- [Häufigkeitsanalyse](?page=frequency-analysis/)
- [XOR-Chiffre](?page=xor/)
- [Schlüsseltausch](?page=diffie-hellman/)
- [Faktorisierung](?page=factorise/)
- [Hash](?page=hash/)
