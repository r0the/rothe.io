# Information und Daten

- [Binär-Challenge](?page=binary-challenge/)
- [Binäre Karten](?page=binary-cards/)
- [Binär-Umrechner](?page=binary-converter/)
- [Farbwahl](?page=colour-picker/)
- [Pentacode](?page=pentacode/)
- [QR-Code-Generator](?page=qr-code/)
- [Redundanz](?page=redundancy/)
