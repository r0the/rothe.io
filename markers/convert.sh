#!/bin/sh
for FILE in markers/*.svg; do
  inkscape -D -z --file="${FILE}" --export-pdf=tmp/$(basename "${FILE}" .svg).pdf --export-latex
done
