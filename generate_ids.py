#!/usr/bin/env python3
import rio

ids: set = set()

optional_root_items = [
    "cover",
    "subtitle",
    "abstract"
]

optional_items = [
    "base",
    "course",
    "icon",
    "page",
    "lock"
]

def process_entry(entry, root):
    if "include" in entry:
        return entry
    new_entry = {}
    if not "title" in entry:
        print("Error: entry without title found")
        exit(1)
    title = entry["title"]
    if "id" in entry:
        id = entry["id"]
    else:
        id = hash(title) % 1000000
        while id in ids:
            id += 1;
        print("Creating id " + str(id) + " for entry " + title)
        ids.add(id)
    new_entry["id"] = id
    new_entry["title"] = title
    if root:
        for key in optional_root_items:
            if key in entry:
                new_entry[key] = entry[key]
    for key in optional_items:
        if key in entry:
            new_entry[key] = entry[key]
    if "items" in entry:
        new_items: list = []
        new_entry["items"] = new_items
        for item in entry["items"]:
            new_items.append(process_entry(item, False))
    return new_entry


def process_book(path):
    print("Bearbeite Skript " + path)
    book = rio.load_json(path)
    book = process_entry(book, True)
    rio.save_json(path, book)


def main():
    paths = rio.load_books()
    for path in paths:
        process_book(path)


main()
