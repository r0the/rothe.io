/* global require */
const vueComponents = [
  'src/components/book.vue',
  'src/components/tab.vue',
  'src/components/tabs.vue',
  'src/components/video.vue',
  'src/core/account-dialog.vue',
  'src/core/app.vue',
  'src/core/code.vue',
  'src/core/collapsible.vue',
  'src/core/exercise.vue',
  'src/core/icon.vue',
  'src/core/image-tag.vue',
  'src/core/markdown-view.vue',
  'src/core/math.vue',
  'src/core/menu.vue',
  'src/core/menu-item.vue',
  'src/core/message.vue',
  'src/core/scratch.vue',
];

import sfcCompiler from '@vue/compiler-sfc';
import fs from 'fs';

const componentStart = 'export default {';

function convertSFC(filePath) {
  fs.readFile(filePath, 'utf8', function (err, data) {
    if (err) {
      console.log(`Fehler in der Datei ${filePath}.`);
      console.log(err);
      return;
    }
    let content = '';
    const parsed = sfcCompiler.parse(data);
    if (!parsed.descriptor) return;
    let template = parsed.descriptor.template.content;
    template = template.replaceAll('`', '``');
    const script = parsed.descriptor.script.content.trim();
    const startPos = script.indexOf(componentStart) + componentStart.length;
    content = script.substring(0, startPos);
    content += '\n  template: `' + template + '  `,';
    content += script.substring(startPos) + '\n';
    const targetFilePath = filePath
      .replace('.vue', '.js')
      .replace('/core/', '/generated/')
      .replace('/components/', '/generated/')
    fs.writeFile(targetFilePath, content, (err) => {
      if (err) throw err;
      console.log(
        `Die Datei ${targetFilePath} ist erfolgreich erstellt worden.`,
      );
    });
  });
}

for (let i = 0; i < vueComponents.length; ++i) {
  convertSFC(vueComponents[i]);
}
// glob('**/*.vue', {}, (err, files) => {
//   console.log(`Convert ${files.length} SFCs...`);
//   files.forEach((filePath) => {
//     convertSFC(filePath);
//   });
// });
