# rothe.io Content Management System

## Voraussetzungen

Um dieses CMS verwenden zu können, muss eine lokale Installation von Node.js und yarn vorhanden sein

## Installation

Mit dem folgenden Befehl werden alle notwendigen Pakete heruntergeladen:
```
yarn install
```

## Lokaler Server

Mit dem folgenden Befehl wird ein lokaler Server gestartet:

```
yarn dev
```

