# Komponenten

## Tabs

Mit `<v-tabs>` und `<v-tab>` können Tabs erzeugt werden:

```html
<v-tabs>
  <v-tab label="Erstens"> Inhalt 1 </v-tab>
  <v-tab label="Zweitens"> Inhalt 2 </v-tab>
</v-tabs>
```

<v-tabs>
  <v-tab label="Erstens"> Inhalt 1 </v-tab>
  <v-tab label="Zweitens"> Inhalt 2 </v-tab>
</v-tabs>

## Videos

Videos können mit der Komponente `v-video` eingebettet werden:

```html
<v-video id="xxxx" source="youtube"> Beschriftung </v-video>
```

Dabei ist `source` die Quelle. Der Standard ist `youtube`, ausserdem sind die Werte `ted` und `vimeo` möglich. Mit `id` wird die ID des Videos angegeben, welche in die URL eingesetzt wird.

## Digitale Schaltungen

Mit der Komponente `v-circuit` wird eine digitale Schaltung von [CircuitVerse](https://circuitverse.org) eingebettet:

```html
<v-circuit id="xxxx" />
```

## IP-Adressen

Die Komponente `remote-ip` zeigt die IP-Adresse an, unter welcher der Client für den Webserver sichtbar ist. Dazu wird das Skript `ip.php` auf dieser Seite aufgerufen.

```html
<remote-ip />
```
