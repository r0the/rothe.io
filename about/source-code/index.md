# Quelltext

- [:link: Download-Konfiguration Prism.js][1]

## Inline

Inline-Quelltext wird mit einfachen _Grave Accents_ (U+0060) `` ` `` umschlossen. Der Quelltext wird rot und in nichtproportionaler Schriftart dargestellt. Es findet keine Syntax-Hervorhebung statt.

:::example

```markdown
Rufe die Funktion `print()` auf.
```

ergibt folgendes:

Rufe die Funktion `print()` auf.
:::

## Fence

Block-Quelltext wird mit drei oder mehr _Grave Accents_ (U+0060) ` ``` ` umschlossen.

::: example

````markdown
```
def foo():
    print('bar')
```
````

ergibt folgendes:

```
def foo():
    print('bar')
```

:::

Nach den einleitenden _Grave Accents_ kann eine Sprache angegeben werden, um die entsprechende Syntax-Hevorhebung zu aktivieren:

::: example

````markdown
```python
def foo():
    print('bar')
```
````

ergibt folgendes:

```python
def foo():
    print('bar')
```

:::

## Definition

In Markdown können auch drei oder mehr Tilden verwendet werden, um einen Quelltext-Block zu umschliessen. Ein solcher Block wird speziell formatiert, um als Titel in einer Referenz zu dienen:

::: example

````markdown
~~~python
microbit.running_time()
~~~
liefert die Anzahl Millisekunden zurück, welche seit dem Einschalten
oder dem letzten Neustart verstrichen sind.
````

ergibt folgendes:

~~~python
microbit.running_time()
~~~

liefert die Anzahl Millisekunden zurück, welche seit dem Einschalten
oder dem letzten Neustart verstrichen sind.
:::

## Sprachen

| Identifier      | Bibliothek    | Modul                              |
| :-------------- | :------------ | :--------------------------------- |
| `aseba`         | Prism         | (selbst definiert)                 |
| `bash`          | Prism         | Bash                               |
| `c`             | Prism         | C                                  |
| `cpp`           | Prism         | C++                                |
| `css`           | Prism         | CSS                                |
| `html`          | Prism         | Markup + HTML + XML + SVG + MathML |
| `javascript`    | Prism         | JavaScript                         |
| `json`          | Prism         | JSON + JSONP                       |
| `latex`         | Prism         | LaTeX                              |
| `markdown`      | Prism         | Markdown                           |
| `prolog`        | Prism         | Prolog                             |
| `python`        | Prism         | Python                             |
| `scratch`       | scratchblocks |                                    |
| `shell-session` | Prism         | Shell session                      |
| `sql`           | Prism         | SQL                                |

#### `aseba`

```aseba
var count = 0

onevent buttons
    when button.forward == 1 do
        count = count + 1
    end
```

#### `bash`

```bash
echo "Hello World"
```

#### `css`

```css
div.test {
  font-size: 5pt;
}
```

#### `html`

```html
<div class="test">Hello World</div>
```

#### `javascript`

```javascript
alert('Hello World');
```

#### `json`

```json
{
  "json": "Text",
  "items": [
    {
      "number": 5
    }
  ]
}
```

#### `latex`

```latex
\documentclass{scrartcl}
\begin{document}
Text
\end{document}
```

#### `markdown`

```markdown
#### Markdown

[Google](https://google.ch)
```

#### `prolog`

```prolog
father(F, C) :- child(C, F), male(F).
```

#### `python`

```python
def square(x):
    return x * x
r = square(5)
print("Result:", r)
```

#### `scratch`

```scratch
Wenn die grüne Flagge angeklickt
gehe (10) er Schritt
```

#### `shell-session`

```shell-session
/home/user$ echo "Hello World"
Hello World
/home/user$ exit
```

#### `sql`

```sql
INSERT INTO TABLE students (name) values ("Bobby"); DROP TABLE students; --")
```

[1]: https://prismjs.com/download.html#themes=prism&languages=markup+css+clike+javascript+bash+c+cpp+http+json+latex+markdown+prolog+python+shell-session+sql&plugins=line-numbers+custom-class+keep-markup
