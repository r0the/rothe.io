# Links

## Query-Parameter

Kurse, Bücher und Seiten können über Query-Parameter referenziert werden.

| Parameter kurz | Parameter lang         | Bedeutung                             |
| :------------- | :--------------------- | :------------------------------------ |
| `c=g23a`       | `?course=g23a`         | Kurs-ID gemäss Eintrag in Kursliste   |
| `b=thymio`     | `?book=thymio`         | Buch-ID gemäss Eintrag in Bücherliste |
| `p=2335635`    | `?page=thymio.sensors` | Seiten-ID oder Seitenreferenz         |

Wird nur `p` oder `page` angegeben, werden für `course` und `book` die im _local storage_ des Browsers gespeicherten Werte übernommen.

Mit dem Wert `none` kann ein Kurs oder ein Buch geschlossen werden:

```
?book=none
```

## Seitenverweis

Verweise auf andere Seiten werden mit dem Suchparameter `page` angegeben. Dabei wird die Dateiendung weggelassen. Mit

```markdown
Siehe [hier](?page=otherpage)
```

wird auf die Datei `otherpage.md` verwiesen.

Achtung: Das Pfadtrennzeichen `/` muss in URL-Parametern als Escape-Sequenz `%2f` dargestellt werden:

```markdown
Siehe [hier](?page=dir%2fotherpage)
```

verweist auf die Datei `dir/otherpage.md`.

Wenn der Seitenpfad mit einem Pfadtrennzeichen `/` endet, so wird automatisch der Dateiname `index.md` angehängt:

```markdown
Siehe [hier](?page=dir/)
```

verweist auf die Datei `dir/index.md`.

Es kann auch ein absoluter Pfad angegeben werden, indem ein Schrägstrich `/` vorangestellt wird. Der absolute Pfad bezieht sich auf den Ort der `index.html`-Datei. Mit

```markdown
Siehe [hier](?page=%2fdir%2fotherpage)
```

wird auf die Datei `/dir/otherpage.md` verwiesen.
