# Bibliotheken

Für diese Website werden die folgenden Bibliotheken verwendet:

| Bibliothek                          | Version | Quellcode           | Lizenz     | statisch | dynamisch |
| :---------------------------------- | :------ | :------------------ | :--------- | -------: | --------: |
| [ACE][101]                          | 1.4.13  | [:mdi-github:][102] | [BSD][103] |   380 kB |         - |
| [appwrite][111]                     | 0.11.0  | [:mdi-github:][112] | [BSD][113] |    27 kB |         - |
| [Brython][121]                      | 3.10.3  | [:mdi-github:][122] | [BSD][123] |   786 kB |         - |
| [Bulma][211]                        | 0.9.3   | [:mdi-github:][212] | [MIT][213] |   378 kB |         - |
| [Bulma Slider][221]                 | 2.0.4   | [:mdi-github:][222] | [MIT][223] |        - |         - |
| [Bulma Switch][231]                 | 1.0.2   | [:mdi-github:][232] | [MIT][233] |        - |         - |
| [Chart.js][291]                     | 3.7.0   | [:mdi-github:][292] | [MIT][293] |   195 kB |         - |
| [How Secure Is My Password][301]    | 0.2.0   | [:mdi-github:][302] | MIT        |        - |    108 kB |
| [KaTeX][321]                        | 0.15.0  | [:mdi-github:][322] | [MIT][323] |    23 kB |    313 kB |
| [markdown-it][401]                  | 12.3.2  | [:mdi-github:][402] | [MIT][403] |   101 kB |         - |
| [markdown-it-container][411]        | 3.0.0   | [:mdi-github:][412] | [MIT][413] |     2 kB |         - |
| [markdown-it-footnote][421]         | 3.0.3   | [:mdi-github:][422] | [MIT][423] |     7 kB |         - |
| [markdown-it-implicit-figures][431] | 0.10.0  | [:mdi-github:][432] | [MIT][433] |     4 kB |         - |
| [Material Design Icons][451]        | 6.5.95  | [:mdi-github:][452] | [OFL][453] |    47 kB |         - |
| [Prism][511]                        | 1.26.0  | [:mdi-github:][512] | [MIT][513] |     5 kB |     46 kB |
| [QRious][521]                       | 4.0.2   | [:mdi-github:][522] | [GPL][522] |        - |     17 kB |
| [scrachblocks][531]                 | 3.5.2   | [:mdi-github:][532] | [MIT][533] |        - |    124 kB |
| [Vue.js][601]                       | 3.2.29  | [:mdi-github:][602] | [MIT][603] |   127 kB |         - |
| [Vue Router][611]                   | 4.0.12  | [:mdi-github:][612] | [MIT][613] |    24 kB |         - |

[101]: https://ace.c9.io
[102]: https://github.com/ajaxorg/ace
[103]: https://github.com/ajaxorg/ace/blob/master/LICENSE
[111]: https://appwrite.io/
[112]: https://github.com/appwrite/appwrite
[113]: https://github.com/appwrite/appwrite/blob/master/LICENSE
[121]: https://brython.info/
[122]: https://github.com/brython-dev/brython
[123]: https://github.com/brython-dev/brython/blob/master/LICENCE.txt
[211]: https://bulma.io/
[212]: https://github.com/jgthms/bulma
[213]: https://github.com/jgthms/bulma/blob/master/LICENSE
[221]: https://wikiki.github.io/form/slider/
[222]: https://github.com/Wikiki/bulma-slider
[223]: https://github.com/Wikiki/bulma-slider/blob/master/LICENSE
[231]: https://wikiki.github.io/form/switch/
[232]: https://github.com/Wikiki/bulma-switch
[233]: https://github.com/Wikiki/bulma-switch/blob/master/LICENSE
[291]: https://www.chartjs.org
[292]: https://github.com/chartjs/Chart.js
[293]: https://github.com/chartjs/Chart.js/blob/master/LICENSE.md
[301]: https://howsecureismypassword.net/
[302]: https://github.com/howsecureismypassword/hsimp
[321]: https://khan.github.io/KaTeX/
[322]: https://github.com/Khan/KaTeX
[323]: https://github.com/Khan/KaTeX/blob/master/LICENSE.txt
[401]: https://markdown-it.github.io/
[402]: https://github.com/markdown-it/markdown-it
[403]: https://github.com/markdown-it/markdown-it/blob/master/LICENSE
[411]: https://github.com/markdown-it/markdown-it-container
[412]: https://github.com/markdown-it/markdown-it-container
[413]: https://github.com/markdown-it/markdown-it-container/blob/master/LICENSE
[421]: https://github.com/markdown-it/markdown-it-footnote
[422]: https://github.com/markdown-it/markdown-it-footnote
[423]: https://github.com/markdown-it/markdown-it-footnote/blob/master/LICENSE
[431]: https://github.com/arve0/markdown-it-implicit-figures
[432]: https://github.com/arve0/markdown-it-implicit-figures
[433]: https://github.com/arve0/markdown-it-implicit-figures/blob/master/LICENSE
[451]: https://materialdesignicons.com/
[452]: https://github.com/Templarian/MaterialDesign-SVG
[453]: https://github.com/Templarian/MaterialDesign-SVG/blob/master/LICENSE
[511]: https://prismjs.com/
[512]: https://github.com/PrismJS/prism
[513]: https://github.com/PrismJS/prism/blob/master/LICENSE
[521]: https://neocotic.com/qrious
[522]: https://github.com/neocotic/qrious
[523]: https://github.com/neocotic/qrious/blob/master/LICENSE.md
[531]: https://scratchblocks.github.io/
[532]: https://github.com/scratchblocks/scratchblocks
[533]: https://github.com/scratchblocks/scratchblocks/blob/master/LICENSE
[601]: https://v3.vuejs.org
[602]: https://github.com/vuejs/vue-next
[603]: https://github.com/vuejs/vue-next/blob/master/LICENSE
[611]: https://next.router.vuejs.org
[612]: https://github.com/vuejs/vue-router-next
[613]: https://github.com/vuejs/vue-router-next/blob/master/LICENSE
