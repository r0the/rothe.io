# Konfiguration

Die Konfiguration der Website findet über JSON-Dateien statt.

## Hauptkonfiguration 

Die Hauptkonfiguration befindet sich in der Datei `config/config.json` relativ zu `index.html`. Sie muss folgende Einträge enthalten:

| Eintrag           | Typ    | Bedeutung                                               |
| :---------------- | :----- | :------------------------------------------------------ |
| `author`          | String | Autor(en), wird in Fusszeile angezeigt                  |
| `books`           | List   | JSON-Dateien mit Bücherlisten                           |
| `courses`         | List   | JSON-Dateien mit Kurslisten                             |
| `logo`            | String | Pfad zur Logo-Datei der Webseite                        |
| `mainLink`        | String | Link auf Hauptseite, welche standardmässig geladen wird |
| `namespace`       | String | Präfix für Einträge im Local Storage des Browsers       |
| `navbarMenuEnd`   | List   | Menüeinträge für die Navbar (rechtbündig)               |
| `navbarMenuStart` | List   | Menüeinträge für die Navbar (linksbündig)               |
| `title`           | String | Titel der Website                                       |

Alle Pfade werden relativ zu `index.html` angegeben.

```json
{
  "title": "rothe.io",
  "author": "Stefan Rothe",
  "mainLink": "?page=course/&book=none",
  "namespace": "rio",
  "books": ["books.json", "book/books.json"],
  "courses": ["course/courses.json"],
  "navbarMenuStart": [],
  "navbarMenuEnd": []
}
```

::: warning
TODO: Wie sieht ein Menüeintrag aus?
:::

## Kurslisten und Bücherlisten

Kurslisten und Bücherlisten sind JSON-Dateien, welche Namen für Kurse und Bücher auf Pfade für die JSON-Dateien der Kurse bzw. Bücher abbilden.

#### books.json

```json
{
  "data": "data/book.json",
  "network": "network/book.json"
}
```

#### courses.json

```json
{
  "g23a": "course/in-g23a/course.json",
  "g23b": "course/in-g23b/course.json"
}
```

## Kurse und Bücher

Ein Kurse oder Buch wird durch eine JSON-Datei definiert. Das Hauptobjekt kann folgende Eigenschaften haben:

| Eigenschaft | Typ     | Bedeutung                                        |
| :---------- | :------ | :----------------------------------------------- |
| `title`     | String  | Titel des Kurses/Buchs                           |
| `base`      | String  | Basisverzeichnis des Kurses/Buchs                |
| `page`      | String  | Seitenverweis auf Hauptseite                     |
| `icon`      | String  | Name eines Icons                                 |
| `subtitle`  | String  | Kurze Beschreibung des Themas                    |
| `items`     | List    | Liste von Menüeinträgen                          |
| `teacher`   | Boolean | Anzeige nur für Lehrer:innen                     |
| `book`      | String  | Buch, welches für einen Kurs geladen werden soll |

Das Basisverzeichnis wird relativ zu `index.html` angegeben, alle anderen Verweise werden relativ zum Basisverzeichnis angegeben.

**Beispiel:**

```json
{
  "title": "Information und Daten",
  "subtitle": "Darstellung von Information im Computer",
  "base": "code",
  "icon": "mdi-translate"
}
```

### Seitenverweise

Seitenverweise werden als Pfad relativ zum Basisverzeichnis angegeben. Wenn der Seitenverweis mit einem Schrägstrich `/` beginnt, wird er als absoluter Pfad bezogen auf die Basis-URL betrachtet. Der Seitenverweis

```
?page=/tools/binary-converter/
```

bezieht sich auf die Seite `/tools/binary-converter/index.md`. Wenn der Seitenverweis **nicht** mit einem Schrägstrich beginnt, wird er als relativer Pfad zur Basisverzeichnis betrachtet. So kann mit

```
?page=../2-next-chapter/
```

auf eine andere Seite im gleichen Verzeichnis verwiesen werden.

## Seiteneintrag

Ein Seiteneintrag kann folgende Eigenschaften haben:

| Eigenschaft | Bedeutung                                            |
| :---------- | :--------------------------------------------------- |
| `id`        | eindeutige ID der Seite                              |
| `base`      | Basisverzeichnis der Seite und aller Unterseiten     |
| `title`     | Titel der Seite                                      |
| `page`      | Pfad zur Markdown-Datei relativ zum Basisverzeichnis |
| `src`       | externer Link                                        |
| `icon`      | Name eines Icons                                     |
| `items`     | Liste von Untereinträgen                             |
| `teacher`   | Anzeige nur für Lehrer:innen                         |

Wenn ein Eintrag eine `page`-Eigenschaft hat, wird diese der `src`-Eigenschaft bevorzugt. Bei Seiten (`page`) wird die Endung `.md` automatisch angehängt. Die Seitenpfade sind relativ zum Basisverzeichnis des Kurses/Buches.

```json
{
  "id": 406123,
  "title": "Algorithmen",
  "page": "algo/index",
  "items": [
    {
      "id": 940675,
      "title": "Algorithmus",
      "page": "algo/1-1-algorithm"
    },
    {
      "title": "Anleitung",
      "icon": "link",
      "src": "https://ict.mygymer.ch/"
    }
  ]
}
```

Seiten mit der Eigenschaft `teacher` werden nur angezeigt, wenn kein Kurs aktiv ist.
