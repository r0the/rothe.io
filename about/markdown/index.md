# Markdown-Syntax

## Inline-Formatierung

Betonung wird mit `*` umschlossen und kursiv dargestellt. Starke Betonung wird mit `**` umschlossen und fett dargestellt.

::: info

```markdown
_Betonung_ und **starke Betonung**
```

_Betonung_ und **starke Betonung**
:::

Eigennamen von GUI-Elementen werden mit `_` markiert und mit `__` stark betont. Im Gegensatz zu Standard-Markdown gibt es eine spezielle Darstellung:

::: example

```
__File Open Dialog__: Wähle den Menüeintrag _File ‣ Open_, um diesen Dialog zu öffnen.
```

**File Open Dialog**: Wähle den Menüeintrag _File ‣ Open_, um diesen Dialog zu öffnen.
:::

## Bilder

Wenn ein Paragraph nur aus einem Bild besteht, wird der Paragraph als `figure` gerendert. Dazu wird das Plugin _markdown-it-implicit-figures_ verwendet.

Wenn in der Bildbeschreibung ein ©-Zeichen vorhanden ist, werden die Urheberrechtsinformationen aus der JSON-Datei mit dem gleichen Namen die die Bilddatei geladen.

```markdown
![Bildbeschreibung ©](./wave.jpg)
```

![Bildbeschreibung ©](./wave.jpg)

## Blockquote

```markdown
> blockquote
```

> blockquote

## Tabellen

| Eine    | einfache |
| ------- | -------- |
| Tabelle |          |

## Listen

Listen werden mit einem vorangestellten Minuszeichen dargestellt:

```markdown
- Erstens
- Zweitens
```

- Erstens
- Zweitens

Wenn das erste Listenelement mit einem Link beginnt, so wird die Liste als Menü dargestellt:

```markdown
- [:download: Erstens]()
- [:link: Zweitens]()
```

- [:download: Erstens]()
- [:link: Zweitens]()

Nummerierte Listen:

1. Erstens
2. Zweitens

## Spalten

Mit `::: columns` wird ein mehrspaltiger Bereich eröffnet, mit `:::` wird er wieder geschlossen. Die Spalten werden durch `***` getrennt

```markdown
::: columns
erste Spalte

---

zweite Spalte
:::
```

::: columns
erste Spalte

---

zweite Spalte
:::

Mit `::: cards` wird ebenfalls ein mehrspaltiger Bereich eröffnet, wobei jede Spalte als Card dargestellt wird:

```markdown
::: cards
Erste Karte

---

Zweite Karte
:::
```

::: cards
Erste Karte

---

Zweite Karte
:::

Ausserdem kann mit `1`, `2`, `3` oder `4` die maximale Anzahl Spalten festgelegt werden:

```markdown
::: cards 3
Erste Karte

---

Zweite Karte

---

Dritte Karte

---

Vierte Karte
:::
```

::: cards 3
Erste Karte

---

Zweite Karte

---

Dritte Karte

---

Vierte Karte
:::

## Information

Mit `::: info` wird eine Informationsbox geöffnet.

```markdown
::: info
Eine Information
:::
```

::: info
Eine Information
:::

## Warnung

Mit `::: warning` wird eine Warnungsbox geöffnet.

```
::: warning
#### Eine Warnung
Markdown ist gefährlich einfach.
:::
```

::: warning

#### Eine Warnung

Markdown ist gefährlich einfach.
:::

## Details

Mit `::: details` wird ein aufklappbarer Hinweis geöffnet.

```
::: details Ein aufklappbarer Hinweis
Hier stehen die Details.
:::
```

::: details Ein aufklappbarer Hinweis
Hier stehen die Details.
:::

## Übung

Mit `::: exercise` wird eine Übung geöffnet. Mit `***` können mehrere Teile abgetrennt werden. Es können bis zu drei zusätzliche Teile angegeben werden:

- Checks
- Hinweis
- Lösung

Dabei können die Teile von oben her weggelassen werden. Zuerst werden also die Checks weggelassen, dann der Hinweis, dann die Lösung.

```
::: exercise
#### :exercise: Eine Übung
Hier steht die Aufgabe.
:::
```

::: exercise

#### :exercise: Eine Übung

Hier steht die Aufgabe.
:::

```
::: exercise
#### :exercise: Eine Übung mit Lösung
Hier steht die Aufgabe.
***
Versteckte Lösung
:::
```

::: exercise

#### :exercise: Eine Übung mit Lösung

Hier steht die Aufgabe.

---

Versteckte Lösung
:::

```
::: exercise
#### :exercise: Eine Übung mit Hinweis und Lösung
Hier steht die Aufgabe.
***
Versteckter Hinweis
***
Versteckte Lösung
:::
```

::: exercise

#### :exercise: Eine Übung mit Hinweis und Lösung

Hier steht die Aufgabe.

---

Versteckter Hinweis

---

Versteckte Lösung
:::
