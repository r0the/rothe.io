# Zeichentabelle

Zeichentabelle zur Verwendung in Markdown:

| Zeichen | Unicode | Bezeichnung                                | Verwendung             |
| :------ | :------ | :----------------------------------------- | :--------------------- |
| `·`     | U+007B  | Middle Dot                                 |                        |
| `©`     | U+00A9  | Copyright Sign                             |                        |
| `«`     | U+00AB  | Left-pointing Double Angle Quotation Mark  | Anführungszeichen      |
| `±`     | U+00B1  | Plus-minus Sign                            | Toleranz               |
| `»`     | U+00BB  | Right-pointing Double Angle Quotation Mark | Anführungszeichen      |
| `×`     | U+00D7  | Multiplication Sign                        | Dimensionsangaben      |
| `ß`     | U+00DF  | Latin Small Letter Sharp S                 |                        |
| `÷`     | U+00F7  | Division Sign                              |                        |
| `Δ`     | U+0394  | Greek Capital Letter Delta                 |                        |
| `‒`     | U+2012  | Figure Dash                                | Aufzählungen           |
| `–`     | U+2013  | En Dash                                    | Trennung               |
| `‣`     | U+2023  | Triangular Bullet                          | Menüpunkt              |
| `…`     | U+2026  | Horizontal Elliipsis                       | Auslassungspunkte      |
| `←`     | U+2190  | Leftwards Arrow                            |                        |
| `↑`     | U+2191  | Upwards Arrow                              |                        |
| `→`     | U+2192  | Rightwards Arrow                           |                        |
| `↓`     | U+2193  | Downwards Arrow                            |                        |
| `≠`     | U+2260  | Not Equal To                               |                        |
| `≤`     | U+2264  | Less-than Or Equal To                      |                        |
| `≥`     | U+2265  | Greater-than Or Equal To                   |                        |
| `⌀`     | U+2300  | Diameter Sign                              |                        |
| `␣`     | U+2423  | Open Box                                   | Symbol für Leerzeichen |
| `★`     | U+2605  | Black Star                                 |                        |
| `☆`     | U+2606  | White Star                                 |                        |
| `♡`     | U+2661  | White Heart Suit                           |                        |
| `♥`     | U+2665  | Bleack Heart Suit                          |                        |
| `♩`     | U+2669  | Quarter Note                               |                        |
| `⬤`     | U+2B24  | Black Large Circle                         |                        |
| `𝄆`     | U+1D106 | Musical Symbol Left Repeat Sign            |                        |
| `𝄇`     | U+1D107 | Musical Symbol Right Repeat Sign           |                        |

## Griechisches Alphabet

| Name    | Klein | Unicode | Gross | Unicode |
| :------ | :---- | :------ | :---- | :------ |
| Alpha   | `α`   | U+03B1  | `Α`   | U+0391  |
| Beta    | `β`   | U+03B2  | `Β`   | U+0392  |
| Gamma   | `γ`   | U+03B3  | `Γ`   | U+0393  |
| Delta   | `δ`   | U+03B4  | `Δ`   | U+0394  |
| Epsilon | `ε`   | U+03B5  | `Ε`   | U+0395  |
| Zeta    | `ζ`   | U+03B6  | `Ζ`   | U+0396  |
| Eta     | `η`   | U+03B7  | `Η`   | U+0397  |
| Theta   | `ϑ`   | U+03D1  | `Θ`   | U+0398  |
| Iota    | `ι`   | U+03B9  | `Ι`   | U+0399  |
| Kappa   | `ϰ`   | U+03F0  | `Κ`   | U+039A  |
| Lambda  | `λ`   | U+03BB  | `Λ`   | U+039B  |
| My      | `μ`   | U+03BC  | `Μ`   | U+039C  |
| Ny      | `ν`   | U+03BD  | `Ν`   | U+039D  |
| Xi      | `ξ`   | U+03BE  | `Ξ`   | U+039E  |
| Omikron | `ο`   | U+03BF  | `Ο`   | U+039F  |
| Pi      | `π`   | U+03C0  | `Π`   | U+03A0  |
| Rho     | `ϱ`   | U+03F1  | `Ρ`   | U+03A1  |
| Sigma   | `σ`   | U+03C2  | `Σ`   | U+03A3  |
| Tau     | `τ`   | U+03C4  | `Τ`   | U+03A4  |
| Ypsilon | `υ`   | U+03C5  | `Υ`   | U+03A5  |
| Phi     | `φ`   | U+03C6  | `Φ`   | U+03A6  |
| Chi     | `χ`   | U+03C7  | `Χ`   | U+03A7  |
| Psi     | `ψ`   | U+03C8  | `Ψ`   | U+03A8  |
| Omega   | `ω`   | U+03C9  | `Ω`   | U+03A9  |

## Superscript

| Zeichen | Unicode | Bezeichnung                      |
| :------ | :------ | :------------------------------- |
| `⁰`     | U+2070  | Superscript Zero                 |
| `¹`     | U+00B9  | Superscript One                  |
| `²`     | U+00B2  | Superscript Two                  |
| `³`     | U+00B3  | Superscript Three                |
| `⁴`     | U+2074  | Superscript Four                 |
| `⁵`     | U+2075  | Superscript Five                 |
| `⁶`     | U+2076  | Superscript Six                  |
| `⁷`     | U+2077  | Superscript Seven                |
| `⁸`     | U+2078  | Superscript Eight                |
| `⁹`     | U+2079  | Superscript Nine                 |
| `⁺`     | U+207A  | Superscript Plus Sign            |
| `⁻`     | U+207B  | Superscript Minus                |
| `⁼`     | U+207C  | Superscript Equals Sign          |
| `⁽`     | U+207D  | Superscript Left Paranthesis     |
| `⁾`     | U+207E  | Superscript Right Paranthesis    |
| `ⁱ`     | U+2071  | Superscript Latin Small Letter I |
| `ⁿ`     | U+207F  | Superscript Latin Small Letter N |
