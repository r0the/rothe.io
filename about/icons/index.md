# Icons

Im Markdown wird ein Icon dargestellt, indem dessen Name mit Doppelpunkten umschlossen wird: `:mdi-heart-pulse:` ergibt :mdi-heart-pulse:. Es steht eine Auswahl der [Material Design Icons][1] zu Verfügung.

Für einige Icons gibt es Aliase. Diese beginnen **nicht** mit `mdi-`.

<v-tabs>
<v-tab label="Kategorien">

### Allgemeine Icons

| Icon           | Alias            | Markdown                 | Verwendung            |
| :------------- | :--------------- | :----------------------- | :-------------------- |
| :construction: | `:construction:` | `:mdi-sign-caution:`     | in Arbeit             |
| :exam:         | `:exam:`         | `:mdi-alert-decagram:`   | Prüfungen/Noten       |
| :goal:         | `:goal:`         | `:mdi-bullseye-arrow:`   | Lernziele             |
| :group:        | `:group:`        | `:mdi-account-multiple:` | Gruppenarbeit         |
| :holiday:      | `:holiday:`      | `:mdi-beach:`            | Ferien/Ausfall        |
| :timeline:     | `:timeline:`     | `:mdi-timeline-outline:` | Semesterplan, Verlauf |

### Links und Downloads

| Icon       | Alias        | Markdown                    | Verwendung                |
| :--------- | :----------- | :-------------------------- | :------------------------ |
| :link:     | `:link:`     | `:mdi-open-in-new:`         | Link auf externe Seite    |
| :download: | `:download:` | `:mdi-download:`            | Download (z.B. Anwendung) |
| :docx:     | `:docx:`     | `:mdi-file-word-box:`       | Link auf Word-Datei       |
| :odt:      | `:odt:`      | `:mdi-text-box:`            | Link auf ODT-Datei        |
| :pdf:      | `:pdf:`      | `:mdi-file-pdf-box:`        | Link auf PDF-Datei        |
| :pptx:     | `:pptx:`     | `:mdi-file-powerpoint-box:` | Link auf PowerPoint-Datei |
| :xlsx:     | `:xlxs:`     | `:mdi-file-excel-box:`      | Link auf Excel-Datei      |
| :zip:      | `:zip:`      | `:mdi-zip-box:`             | Link auf Zip-Datei        |

## Boxes

Die folgenden Icons können in der entsprechenden Box (`::: box`) verwendet werden:

| Icon       | Alias        | Icon-Name               | Verwendung       |
| :--------- | :----------- | :---------------------- | :--------------- |
| :exercise: | `:exercise:` | `:pencil-box-outline:`  | Aufgaben/Übungen |
| :info:     | `:info:`     | `:information-outline:` | information      |
| :warning:  | `:warning:`  | `:alert-outline:`       | Warnung          |

### Pfeile

| Icon                                  | Markdown                                |
| :------------------------------------ | :-------------------------------------- |
| :mdi-arrow-down:                      | `:mdi-arrow-down:`                      |
| :mdi-arrow-down-bold:                 | `:mdi-arrow-down-bold:`                 |
| :mdi-arrow-down-drop-circle-outline:  | `:mdi-arrow-down-drop-circle-outline:`  |
| :mdi-arrow-down-thick:                | `:mdi-arrow-down-thick:`                |
| :mdi-arrow-left:                      | `:mdi-arrow-left:`                      |
| :mdi-arrow-left-bold:                 | `:mdi-arrow-left-bold:`                 |
| :mdi-arrow-left-drop-circle-outline:  | `:mdi-arrow-left-drop-circle-outline:`  |
| :mdi-arrow-left-thick:                | `:mdi-arrow-left-thick:`                |
| :mdi-arrow-right:                     | `:mdi-arrow-right:`                     |
| :mdi-arrow-right-bold:                | `:mdi-arrow-right-bold:`                |
| :mdi-arrow-right-drop-circle-outline: | `:mdi-arrow-right-drop-circle-outline:` |
| :mdi-arrow-right-thick:               | `:mdi-arrow-right-thick:`               |
| :mdi-arrow-up:                        | `:mdi-arrow-up:`                        |
| :mdi-arrow-up-bold:                   | `:mdi-arrow-up-bold:`                   |
| :mdi-arrow-up-drop-circle-outline:    | `:mdi-arrow-up-drop-circle-outline:`    |
| :mdi-arrow-up-thick:                  | `:mdi-arrow-up-thick:`                  |
| :mdi-chevron-down:                    | `:mdi-chevron-down:`                    |
| :mdi-chevron-left:                    | `:mdi-chevron-left:`                    |
| :mdi-chevron-right:                   | `:mdi-chevron-right:`                   |
| :mdi-chevron-up:                      | `:mdi-chevron-up:`                      |

### Marken und Produkte

| Icon                      | Markdown                    |
| :------------------------ | :-------------------------- |
| :mdi-android:             | `:mdi-android:`             |
| :mdi-apple:               | `:mdi-apple:`               |
| :mdi-apple-safari:        | `:mdi-apple-safari:`        |
| :mdi-blender-software:    | `:mdi-blender-software:`    |
| :mdi-microsoft-edge:      | `:mdi-microsoft-edge:`      |
| :mdi-email:               | `:mdi-email:`               |
| :mdi-firefox:             | `:mdi-firefox:`             |
| :mdi-git:                 | `:mdi-git:`                 |
| :mdi-github:              | `:mdi-github:`              |
| :mdi-google:              | `:mdi-google:`              |
| :mdi-google-chrome:       | `:mdi-google-chrome:`       |
| :mdi-language-javascript: | `:mdi-language-javascript:` |
| :mdi-language-markdown:   | `:mdi-language-markdown:`   |
| :mdi-language-python:     | `:mdi-language-python:`     |
| :mdi-microsoft:           | `:mdi-microsoft:`           |
| :mdi-microsoft-edge:      | `:mdi-microsoft-edge:`      |
| :mdi-microsoft-office:    | `:mdi-microsoft-office:`    |
| :mdi-microsoft-onedrive:  | `:mdi-microsoft-onedrive:`  |
| :mdi-microsoft-windows:   | `:mdi-microsoft-windows:`   |
| :mdi-snapchat:            | `:mdi-snapchat:`            |
| :mdi-vuejs:               | `:mdi-vuejs:`               |
| :mdi-whatsapp:            | `:mdi-whatsapp:`            |
| :mdi-wifi:                | `:mdi-wifi:`                |

### Allgemeine Icons

| Icon                       | Markdown                     |
| :------------------------- | :--------------------------- |
| :mdi-check-circle:         | `:mdi-check-circle:`         |
| :mdi-check-circle-outline: | `:mdi-check-circle-outline:` |
| :mdi-cloud-outline:        | `:mdi-cloud-outline:`        |
| :mdi-sync:                 | `:mdi-sync:`                 |

### Tasten

Die folgenden Icons werden für die Darstellung von [Tasten](?page=../keyboard/) verwendet. Sie müssen normalerweise nicht direkt als Icon eingesetzt werden.

| Icon                         | Markdown                       | Verwendung            |
| :--------------------------- | :----------------------------- | :-------------------- |
| :mdi-apple-keyboard-caps:    | `:mdi-apple-keyboard-caps:`    | Feststelltaste        |
| :mdi-apple-keyboard-command: | `:mdi-apple-keyboard-command:` | Command-Taste (Apple) |
| :mdi-apple-keyboard-control: | `:mdi-apple-keyboard-control:` | Control-Taste (Apple) |
| :mdi-apple-keyboard-option:  | `:mdi-apple-keyboard-option:`  | Option-Taste (Apple)  |
| :mdi-apple-keyboard-shift:   | `:mdi-apple-keyboard-shift:`   | Shift-Taste           |
| :mdi-arrow-down-bold:        | `:mdi-arrow-down-bold:`        | Pfeiltaste            |
| :mdi-arrow-left-bold:        | `:mdi-arrow-left-bold:`        | Pfeiltaste            |
| :mdi-arrow-right-bold:       | `:mdi-arrow-right-bold:`       | Pfeiltaste            |
| :mdi-arrow-up-bold:          | `:mdi-arrow-up-bold:`          | Pfeiltaste            |
| :mdi-backspace-outline:      | `:mdi-backspace-outline:`      | Backspace-Taste       |
| :mdi-keyboard-return:        | `:mdi-keyboard-return:`        | Return-Taste          |
| :mdi-keyboard-tab:           | `:mdi-keyboard-tab:`           | Tabulatortaste        |
| :mdi-menu:                   | `:mdi-menu:`                   | Menütaste             |
| :mdi-microsoft-windows:      | `:mdi-microsoft-windows:`      | Windows-Taste         |

</v-tab>
<v-tab label="Alle">

### Alle verfügbaren Icons

| Icon                                  | Markdown                                | Alias             |
| :------------------------------------ | :-------------------------------------- | :---------------- |
| :mdi-account:                         | `:mdi-account:`                         |
| :mdi-account-multiple:                | `:mdi-account-multiple:`                | `:group:`         |
| :mdi-alert-decagram:                  | `:mdi-alert-decagram:`                  | `:exam:`          |
| :mdi-alert-outline:                   | `:mdi-alert-outline:`                   | `:warning:`       |
| :mdi-android:                         | `:mdi-android:`                         |
| :mdi-apple:                           | `:mdi-apple:`                           |
| :mdi-apple-keyboard-caps:             | `:mdi-apple-keyboard-caps:`             |
| :mdi-apple-keyboard-command:          | `:mdi-apple-keyboard-command:`          |
| :mdi-apple-keyboard-control:          | `:mdi-apple-keyboard-control:`          |
| :mdi-apple-keyboard-option:           | `:mdi-apple-keyboard-option:`           |
| :mdi-apple-keyboard-shift:            | `:mdi-apple-keyboard-shift:`            |
| :mdi-apple-safari:                    | `:mdi-apple-safari:`                    |
| :mdi-application-edit:                | `:mdi-application-edit:`                |
| :mdi-application-edit-outline:        | `:mdi-application-edit-outline:`        |
| :mdi-arrow-down:                      | `:mdi-arrow-down:`                      |
| :mdi-arrow-down-bold:                 | `:mdi-arrow-down-bold:`                 |
| :mdi-arrow-down-drop-circle-outline:  | `:mdi-arrow-down-drop-circle-outline:`  | `:thymio-down:`   |
| :mdi-arrow-down-thick:                | `:mdi-arrow-down-thick:`                |
| :mdi-arrow-left:                      | `:mdi-arrow-left:`                      |
| :mdi-arrow-left-bold:                 | `:mdi-arrow-left-bold:`                 |
| :mdi-arrow-left-drop-circle-outline:  | `:mdi-arrow-left-drop-circle-outline:`  | `:thymio-left:`   |
| :mdi-arrow-left-thick:                | `:mdi-arrow-left-thick:`                |
| :mdi-arrow-right:                     | `:mdi-arrow-right:`                     |
| :mdi-arrow-right-bold:                | `:mdi-arrow-right-bold:`                |
| :mdi-arrow-right-drop-circle-outline: | `:mdi-arrow-right-drop-circle-outline:` | `:thymio-right:`  |
| :mdi-arrow-right-thick:               | `:mdi-arrow-right-thick:`               |
| :mdi-arrow-up:                        | `:mdi-arrow-up:`                        |
| :mdi-arrow-up-bold:                   | `:mdi-arrow-up-bold:`                   |
| :mdi-arrow-up-drop-circle-outline:    | `:mdi-arrow-up-drop-circle-outline:`    | `:thymio-up:`     |
| :mdi-arrow-up-thick:                  | `:mdi-arrow-up-thick:`                  |
| :mdi-backspace-outline:               | `:mdi-backspace-outline:`               |
| :mdi-beach:                           | `:mdi-beach:`                           | `:holiday:`       |
| :mdi-blender-software:                | `:mdi-blender-software:`                |
| :mdi-book-open-variant:               | `:mdi-book-open-variant:`               | `:book:`          |
| :mdi-bookshelf:                       | `:mdi-bookshelf:`                       |
| :mdi-bullseye-arrow:                  | `:mdi-bullseye-arrow:`                  | `:goal:`          |
| :mdi-calendar:                        | `:mdi-calendar:`                        |
| :mdi-camera:                          | `:mdi-camera:`                          |
| :mdi-cellphone-link:                  | `:mdi-cellphone-link:`                  |
| :mdi-chart-bar:                       | `:mdi-chart-bar:`                       |
| :mdi-chart-bell-curve:                | `:mdi-chart-bell-curve:`                |
| :mdi-chevron-down:                    | `:mdi-chevron-down:`                    |
| :mdi-chevron-left:                    | `:mdi-chevron-left:`                    |
| :mdi-chevron-right:                   | `:mdi-chevron-right:`                   |
| :mdi-chevron-up:                      | `:mdi-chevron-up:`                      |
| :mdi-check-circle:                    | `:mdi-check-circle:`                    |
| :mdi-check-circle-outline:            | `:mdi-check-circle-outline:`            |
| :mdi-chip:                            | `:mdi-chip:`                            |
| :mdi-circle-outline:                  | `:mdi-circle-outline:`                  | `:thymio-middle:` |
| :mdi-close:                           | `:mdi-close:`                           |
| :mdi-cloud-outline:                   | `:mdi-cloud-outline:`                   |
| :mdi-cog:                             | `:mdi-cog:`                             | `:interactive:`   |
| :mdi-controller-classic:              | `:mdi-controller-classic:`              |
| :mdi-cube-outline:                    | `:mdi-cube-outline:`                    |
| :mdi-cube-send:                       | `:mdi-cube-send:`                       |
| :mdi-cursor-default-click:            | `:mdi-cursor-default-click:`            |
| :mdi-dice-1:                          | `:mdi-dice-1:`                          |
| :mdi-dice-2:                          | `:mdi-dice-2:`                          |
| :mdi-dice-3:                          | `:mdi-dice-3:`                          |
| :mdi-dice-4:                          | `:mdi-dice-4:`                          |
| :mdi-dice-5:                          | `:mdi-dice-5:`                          |
| :mdi-dice-6:                          | `:mdi-dice-6:`                          |
| :mdi-download:                        | `:mdi-download`                         | `:download:`      |
| :mdi-email:                           | `:mdi-email`                            |
| :mdi-export-variant:                  | `:mdi-export-variant:`                  |
| :mdi-eye:                             | `:mdi-eye`                              |
| :mdi-face-recognition:                | `:mdi-face-recognition:`                |
| :mdi-file-excel-box:                  | `:mdi-file-excel-box:`                  | `:xlsx:`          |
| :mdi-file-pdf-box:                    | `:mdi-file-pdf-box:`                    | `:pdf:`           |
| :mdi-file-powerpoint-box:             | `:mdi-file-powerpoint-box:`             | `:pptx:`          |
| :mdi-file-word-box:                   | `:mdi-file-word-box:`                   | `:docx:`          |
| :mdi-firefox:                         | `:mdi-firefox:`                         |
| :mdi-format-text:                     | `:mdi-format-text:`                     |
| :mdi-forum-outline:                   | `:mdi-forum-outline:`                   |
| :mdi-gate-and:                        | `:mdi-gate-and:`                        |
| :mdi-gate-nand:                       | `:mdi-gate-nand:`                       |
| :mdi-gate-not:                        | `:mdi-gate-not:`                        |
| :mdi-gate-or:                         | `:mdi-gate-or:`                         |
| :mdi-gate-xor:                        | `:mdi-gate-xor:`                        |
| :mdi-git:                             | `:mdi-git:`                             |
| :mdi-github:                          | `:mdi-github:`                          |
| :mdi-google:                          | `:mdi-google:`                          |
| :mdi-google-chrome:                   | `:mdi-google-chrome:`                   |
| :mdi-graph:                           | `:mdi-graph:`                           |
| :mdi-hammer-wrench:                   | `:mdi-hammer-wrench:`                   |
| :mdi-heart-pulse:                     | `:mdi-heart-pulse:`                     |
| :mdi-home:                            | `:mdi-home:`                            |
| :mdi-human-male-board:                | `:mdi-human-male-board:`                |
| :mdi-information-outline:             | `:mdi-information-outline:`             |
| :mdi-information-variant:             | `:mdi-information-variant:`             |
| :mdi-instagram:                       | `:mdi-instagram:`                       |
| :mdi-key:                             | `:mdi-key:`                             |
| :mdi-keyboard-return:                 | `:mdi-keyboard-return:`                 |
| :mdi-keyboard-tab:                    | `:mdi-keyboard-tab:`                    |
| :mdi-lan:                             | `:mdi-lan:`                             |
| :mdi-language-javascript:             | `:mdi-language-javascript:`             |
| :mdi-language-markdown:               | `:mdi-language-markdown:`               |
| :mdi-language-python:                 | `:mdi-language-python:`                 |
| :mdi-lightbulb-on:                    | `:mdi-lightbulb-on:`                    |
| :mdi-lock:                            | `:mdi-lock:`                            |
| :mdi-lock-open:                       | `:mdi-lock-open:`                       |
| :mdi-menu:                            | `:mdi-menu:`                            |
| :mdi-microphone:                      | `:mdi-microphone:`                      |
| :mdi-microsoft:                       | `:mdi-microsoft:`                       |
| :mdi-microsoft-edge:                  | `:mdi-microsoft-edge:`                  |
| :mdi-microsoft-office:                | `:mdi-microsoft-office:`                |
| :mdi-microsoft-onedrive:              | `:mdi-microsoft-onedrive:`              |
| :mdi-microsoft-windows:               | `:mdi-microsoft-windows:`               |
| :mdi-music:                           | `:mdi-music:`                           |
| :mdi-music-note-outline:              | `:mdi-music-note-outline:`              |
| :mdi-open-in-new:                     | `:mdi-open-in-new:`                     | `:link:`          |
| :mdi-pause:                           | `:mdi-pause:`                           |
| :mdi-pencil:                          | `:mdi-pencil:`                          | `:task:`          |
| :mdi-play:                            | `:mdi-play:`                            |
| :mdi-power:                           | `:mdi-power:`                           |
| :mdi-presentation:                    | `:mdi-presentation:`                    |
| :mdi-printer:                         | `:mdi-printer:`                         |
| :mdi-qrcode:                          | `:mdi-qrcode:`                          |
| :mdi-raspberry-pi:                    | `:mdi-raspberry-pi:`                    |
| :mdi-refresh:                         | `:mdi-refresh:`                         |
| :mdi-robot:                           | `:mdi-robot:`                           |
| :mdi-robot-mower:                     | `:mdi-robot-mower:`                     |
| :mdi-school:                          | `:mdi-school:`                          |
| :mdi-shape:                           | `:mdi-shape:`                           |
| :mdi-share:                           | `:mdi-share:`                           |
| :mdi-sigma:                           | `:mdi-sigma:`                           |
| :mdi-sign-caution:                    | `:mdi-sign-caution:`                    | `:construction:`  |
| :mdi-sitemap:                         | `:mdi-sitemap:`                         |
| :mdi-snapchat:                        | `:mdi-snapchat:`                        |
| :mdi-snowflake:                       | `:mdi-snowflake:`                       |
| :mdi-soldering-iron:                  | `:mdi-soldering-iron:`                  |
| :mdi-star:                            | `:mdi-star:`                            | `:extra:`         |
| :mdi-sync:                            | `:mdi-sync:`                            |
| :mdi-text-box:                        | `:mdi-text-box:`                        | `:odt:`           |
| :mdi-text-box-multiple-outline:       | `:mdi-text-box-multiple-outline:`       |
| :mdi-timeline-outline:                | `:mdi-timeline-outline:`                | `:timeline:`      |
| :mdi-translate:                       | `:mdi-translate:`                       |
| :mdi-trophy-award:                    | `:mdi-trophy-award:`                    |
| :mdi-upload:                          | `:mdi-upload:`                          |
| :mdi-view-module:                     | `:mdi-view-module:`                     |
| :mdi-vuejs:                           | `:mdi-vuejs:`                           |
| :mdi-web:                             | `:mdi-web:`                             |
| :mdi-whatsapp:                        | `:mdi-whatsapp:`                        |
| :mdi-wifi:                            | `:mdi-wifi:`                            |
| :mdi-youtube:                         | `:mdi-youtube:`                         |
| :mdi-zip-box:                         | `:mdi-zip-box:`                         | `:zip:`           |

</v-tab>
</v-tabs>

[1]: https://materialdesignicons.com/
