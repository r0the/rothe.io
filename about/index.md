# Über diese Webseite

## Kontakt

[Stefan Rothe](https://stefan-rothe.ch/)<br>
[Gymnasium Kirchenfeld](https://www.gymkirchenfeld.ch/)<br>
Kirchenfeldstrasse 25<br>
3005 Bern<br>
Schweiz

## Technologien

Für diese Webseite werden folgende Technologien verwendet:

::: cards 3
<v-card-link icon="mdi-vuejs" title="Vue.js" subtitle="JavaScript-Framework" to="https://vuejs.org/"/>

---

<v-card-link icon="mdi-language-markdown" title="Markdown" subtitle="Auszeichnungs&shy;sprache" to="https://commonmark.org/"/>
:::

## Programmierung

Der Quellcode der Webseite ist von Stefan Rothe geschrieben worden. Er steht unter einer MIT-Lizenz. Die [verwendeten Bibliotheken](?page=libraries/) stehen unter einer MIT- oder BSD-Lizenz.

## Inhalte

Die Inhalte stammen von folgenden Personen:

- Stefan Rothe, Gymnasium Kirchenfeld
- Tom Jampen, Gymnasium Kirchenfeld
- Sebastian Forster, Gymnasium Kirchenfeld
- Martin Lehmann, Gymnasium Kirchenfeld
- Andreas Bürge, Gymnasium Kirchenfeld
- Anna Payer, Gymnasium Kirchenfeld
- Uwe Gleiß, Franz-Ludwig-Gymnasium Bamberg

Sämtliche Inhalte können unter den Bedingungen der [Creative Commons Attribution-NonCommercial-ShareAlike 4.0][4]-Lizenz verwendet werden.

Die Illustrationen stammen vorwiegend von den Autoren, [Wikimedia Commons][5] und [Pixabay][6] oder manchmal aus anderen Quellen. Sämtliche Illustrationen stehen unter einer [Creative Commons][7]-Lizenz.

[4]: https://creativecommons.org/licenses/by-nc-sa/4.0/
[5]: https://commons.wikimedia.org/wiki/
[6]: https://pixabay.com/
[7]: https://creativecommons.org/
