# Mathematik

Mathematik wird mit der [KaTeX][1]-Bibliothek gerendert.

Es gibt zwei Mathematik-Modi: inline und Block. Inline-Formeln werden mit dem Dollarzeichen `$` begrenzt:

::: example

```
Der Satz von Pythagoras lautet: $a^2 + b^2 = c^2$
```

Der Satz von Pythagoras lautet: $a^2 + b^2 = c^2$
:::

Block-Formeln werden mit einem doppelten Dollarzeichen `$$` begrenzt. Die Formel muss in einem eigenen Absatz stehen:

::: example

```
Der Satz von Pythagoras lautet:

$$a^2 + b^2 = c^2$$
```

Der Satz von Pythagoras lautet:

$$a^2 + b^2 = c^2$$
:::

## Funktionen

|    TeX    | Darstellung |  TeX   | Darstellung |
| :-------: | :---------: | :----: | :---------: |
| `\arcsin` |  $\arcsin$  | `\sin` |   $\sin$    |
| `\arccos` |  $\arccos$  | `\tan` |   $\tan$    |
| `\arctan` |  $\arctan$  | `\det` |   $\det$    |
|  `\cos`   |   $\cos$    | `\lim` |   $\lim$    |
|   `\ln`   |    $\ln$    | `\max` |   $\max$    |
|  `\log`   |   $\log$    | `\min` |   $\min$    |

## Operatoren

| TeX | Darstellung | TeX | Darstellung |
| :-: | :---------: | :-: | :---------: |
| `+` |     $+$     | `-` |     $-$     |
| `⋅` |     $⋅$     |     |             |

## Relationen

| TeX | Darstellung |    TeX    | Darstellung |
| :-: | :---------: | :-------: | :---------: |
| `=` |     $=$     |   `\in`   |    $\in$    |
| `≠` |     $≠$     | `\notin`  |  $\notin$   |
| `<` |     $<$     | `\subset` |  $\subset$  |
| `>` |     $>$     | `\supset` |  $\supset$  |
| `≤` |     $≤$     |           |             |
| `≥` |     $≥$     |           |             |

[1]: https://www.katex.org/
