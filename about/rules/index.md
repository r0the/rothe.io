# Hinweise für Autoren

## Typografie

#### Anführungszeichen

Im normalen Text werden die Schweizer Anführungszeichen `«` und `»` verwendet.

#### GUI-Elemente

**GUI-Elemente** sind Texte, die genau so auf der Benutzeroberfläche vorkommen. Diese werden mit `_` oder `__` markiert und somit rot hervorgehoben. Beispiel: Wähle den Menüpunkt _File ‣ Save_, um die Datei zu speichern.

Haupt- und Untermenü werden mit dem Unicodezeichen U+2023 (Triangular Bullet) `‣`, getrennt.

#### Auslassungspunkte

Als Auslassungspunkte wird das Unicodezeichen U+2026 (Horizontal Elliipsis) `…` verwendet, nicht drei aufeinanderfolgende Punkte. Vor und nach den Auslassungspunkten stehen je ein Leerzeichen.

#### Dimensionsangaben

Für mehrfache Dimensionsangaben wird das Unicodezeichen U+00D7 (Multiplication Sign) `×` verwendet, nicht ein kleines `x`.

## Bilder

#### Format

Bilder sollten im Normalfall das Format 16:9 haben und in folgenden Auflösungen abgespeichert werden:

- 640×360: normal, für Verwendung in Webseite
- 1280×720: mit Endung **.1280**, für Verwendung in PDF
- 1920×1080: mit Endung **.1920**

JPEG sollten mit einer Qualität von 80% gespeichert werden.

Bildschirmfotos sollten in Originalgrösse gespeichert werden.

#### Diagramme

Diagramme werden idealerweise mit **Dia** erstellt. Sie sollten auch das Format 16:9 haben, damit sie einfach in Präsentationen eingebunden werden können. In Dia sollten die Diagramme so formatiert werden:

- Grösse: 32×18 cm
- Liniendicke: 0.01 cm
- Schriftart: Droid Sans Fallback
- Schriftgrösse: 30 pt (klein: 20 pt)

In Diagrammen sollten nach Möglichkeit [Material Design Colours][1] verwendet werden:

| Hex Code  | Farbe           | Bedeutung                   |
| :-------- | :-------------- | :-------------------------- |
| `#FFE082` | Amber 200       | Orange hell                 |
| `#FF6F00` | Amber 900       | Orange dunkel               |
| `#90CAF9` | Blue 200        | Blau hell                   |
| `#0D47A1` | Blue 900        | Blau dunkel                 |
| `#80DEEA` | Cyan 200        |                             |
| `#EEEEEE` | Gray 200        | Grau hell                   |
| `#BDBDBD` | Gray 400        | Grau mittel                 |
| `#9E9E9E` | Gray 500        | Grau dunkel                 |
| `#A5D6A7` | Green 200       | Grün hell                   |
| `#1B5E20` | Green 900       | Grün dunkel                 |
| `#C5E1A5` | Light Green 200 |                             |
| `#CE93D8` | Purple 200      | Violett hell                |
| `#EF9A9A` | Red 200         | Rot hell                    |
| `#B71C1C` | Red 900         | Rot dunkel                  |
| `#FFEB3B` | Yellow 500      | Markierung von UI-Elementen |

[1]: https://material.io/design/color/
